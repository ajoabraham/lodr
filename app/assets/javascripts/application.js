// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require tether

// require bootstrap
//= require toolkit.min	
//= require jquery_ujs
//= require turbolinks
//= require handlebars
//= require jquery-ui.min
//= require z.jquery.fileupload
//= require jquery.tablesorter.min
//= require jquery.stickytableheaders.min
//= require lodr
//= require highcharts
//= require codemirror
//= require sql-mode
//= require handsontable.full.min
//= require jsonpath.min
//= require_tree .

Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};

$(document).on('click','.check-all',function(e){
	e.preventDefault();
	e.stopPropagation();
	$el = $(e.currentTarget);
	var targets = $el.data("check-all-target");
	
	if($el.data("checked") === true){
		$el.data("checked", "false")
			.find("i")
			.addClass("fa-square-o")
			.removeClass("fa-check-square-o");
			
		
		$("input[type=checkbox]").prop("checked",false);
		
		$(targets).addClass("hidden");
	}else{
		$el.data("checked", true)
			.find("i")
			.removeClass("fa-square-o")
		.addClass("fa-check-square-o");
			
		$("input[type=checkbox]").prop("checked",true);
		$(targets).removeClass("hidden");
	}
});

$(document).on('click','input[type=checkbox]',function(e){
	var targets = $(".check-all").data("check-all-target");
	if(targets && $(":checked").length){
		$(targets).removeClass("hidden");
	}else if(targets){
		$(targets).addClass("hidden");
	}
});

// Submenus
$(document).on('click', '.dropdown-submenu a.submenu', function(e){
	$('li.dropdown-submenu .dropdown-menu:visible').hide();
  
  $(this).next('ul').toggle();  
    
  e.stopPropagation();
  e.preventDefault();
  
  $u = $(this).next('ul');
  if(($u.offset().left + $u.width()) > $(window).width()){
	  $u.css('left','-50%');
  }
});

$(document).on('click','.check-enable',function(e){
	e.preventDefault();
	e.stopPropagation();
	
	if($(e.currentTarget).hasClass("disabled")){
		return ;
	}
	
	var $f = $($(e.currentTarget).data("check-all-form"));
	if($f.length){
		$mf = $("#main-check-all-form");
		$mf.attr('action',$f.attr("action"));
		$mf.attr('method',$f.attr("method"));
		$mf.find("input[name=authenticity_token]").val($f.find("input[name=authenticity_token]").val());
		
		var _meth = $f.find("input[name=_method]");
		if(_meth.length){
			var _mmeth = $mf.find("input[name=_method]");
			if(_mmeth.length){
				_mmeth.val(_meth.val());
			}else{
				$mf.append('<input type="hidden" value='+ _meth.val() +' name="_method" />');
			}
		}
		$(e.currentTarget).addClass("disabled");
		$mf.submit();
	}	
});

$(document).on('turbolinks:load',function(e){
	$("#main-ok-alert").fadeTo(8000, 500).slideUp(500, function(){
	    $("#main-ok-alert").slideUp(500);
	});
});

$(document).on('show.bs.modal','#errorModal', function (event) {
	var button = $(event.relatedTarget);
	var modal = $(this);
	
	var bodyTransformer = button.data('body-transformer');
	var body = button.data('body');
	if(bodyTransformer){
		if(bodyTransformer == "fieldError" && typeof body == "object"){
			modal.find("#errorModalLabel").text("Number of Rows with Bad Data per Column")
			var new_body = '<table class="table table-condensed"><tr><th>Column Name</th><th># of Rows with Errors</th></tr>';
			for(var i=0;i<body.length;i++){
				new_body += "<tr><td>" + body[i]["columnName"] + "</td><td>" + body[i]["failedRows"] + "</td></tr>";
			}
			body = new_body + "</table>";
		}
	}else{
		modal.find("#errorModalLabel").text("Execution Error")
	}
	modal.find('.modal-body pre').html(body);
});

// auto setup current page polling
$(document).on('turbolinks:load',function(e){
	var poll = $('*[data-poll-page]:first');
	var condition = poll.data("poll-page-condition");
	var timeout = poll.data("poll-page");
	var path = poll.data("poll-page-path");
	if($(condition).length > 0 && timeout){
		setTimeout(function(e){ Turbolinks.visit(path,{action: 'replace'}); }, timeout);
	}
});