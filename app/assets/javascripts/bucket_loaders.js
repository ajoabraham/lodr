
lodr.bucket_loader = {
	pollers: function(){
		var pollers = $(".loader-status .queued, .loader-status .running_loads, .loader-status .prepping_files").map(function(i,el){
			return $(el).data("loader-id");
		}).get();
		
		return pollers;
	},
	
	index_poll: function(){
		var pollers = lodr.bucket_loader.pollers();
	
		if(pollers.length >0){
			$.getScript('bucket_loaders?loaders=' + JSON.stringify(pollers));
			
			if($(".bucket_loaders.index").length){
				setTimeout(lodr.bucket_loader.index_poll, 2000);
			}
		}		
	},
	
	show_poll: function(){
		var pollers = lodr.bucket_loader.pollers();
		var REFRESH_INTERVAL_IN_MILLIS = 5000;
	     if (pollers.length > 0) {
	       setTimeout(function(){
			   	lodr.bucket_loader.scroll = $(window).scrollTop();
	        	Turbolinks.visit(location.toString());
	        }, REFRESH_INTERVAL_IN_MILLIS);
	    }
	},
	
	getPreview: function(){
		$(".bucket_loader_preview_section").html(
			'<div class="alert alert-info text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Loading...</div>'
		);
		if($("select#bucket_loader_file_mapping_type").val().trim() == ""){
			$(".bucket_loader_preview_section").html(
				'<a class="btn btn-primary-outline m-b" onclick="lodr.bucket_loader.getPreview();return false;"><i class="fa fa-refresh"></i> Refresh Preview</a>' +
				'<div class="alert alert-warning">Please choose a valid file mapping type to see a preview of tables created</div>'
			);
		}else{
			var s = $(".bucket_loader_preview_section").data("preview-path");
			var o = $("form.bucket_loader_form").serializeObject();
			o._method = "get";
			var p = s+"?"+$.param(o);
			$.getScript(p);
		}
	}
}

$(document).on("turbolinks:load",function(){
	// index page poller
	if($(".bucket_loaders.index").length){
		lodr.bucket_loader.index_poll();
	}

	if($(".bucket_loader_preview_section").length>0){
		lodr.bucket_loader.getPreview();
	}
});
$(document).on("change", "select#bucket_loader_s3_bucket_id, select#bucket_loader_file_mapping_type, #bucket_loader_prefix, #bucket_loader_target_table_prefix, #bucket_loader_target_table_suffix, #bucket_loader_file_filter", function(e){
	lodr.bucket_loader.getPreview();
});