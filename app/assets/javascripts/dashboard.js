// # Place all the behaviors and hooks related to the matching controller here.
// # All this logic will automatically be available in application.js.
// # You can use CoffeeScript in this file: http://coffeescript.org/
$(function(){
	Handlebars.registerHelper('numberToLocale', function(num) {
		if(!isNaN(num) && num.toLocaleString){
			return num.toLocaleString();
		}
	});
	Handlebars.registerHelper('sizeToHuman', function(num) {
		if(!isNaN(num)){
			return (num / (1024)).toLocaleString();
		}
	});
});
lodr.dash = {
	_job_stats: function(context){
		this.__job_stats = this.__job_stats ? this.__job_stats : Handlebars.compile($("#_job_stats").html());
		return this.__job_stats(context);
	},
	
	pollStatus: function(){
		var pollers = $(".running, .queued").map(function(i,el){
			return $(el).data("job-id");
		}).get();
		
		if(pollers.length >0){
			$.getScript('dashboard/stats?job_ids=' + JSON.stringify(pollers));
			setTimeout(lodr.dash.pollStatus, 3000);
		}
	}
}

$(document).on("turbolinks:load", function(e){
	if($(".dashboard.index").length){
		lodr.dash.pollStatus();
	}		
});

$(document).on('click', '.job-stats',function(e){
	var ctx = $(e.currentTarget).data("stats");
	if(ctx.started_at){
		var d = new Date(ctx.started_at);
		ctx.started_at = d.toLocaleString();
	}
	if(ctx.finished_at){
		var d = new Date(ctx.finished_at);
		ctx.finished_at = d.toLocaleString();
	}
	ctx.percent_loaded = ((ctx.total_rows_loaded/ctx.input_rows)*100) + '%';
	ctx.percent_failed = ((ctx.total_failed_rows/ctx.input_rows)*100) + '%';
	ctx.percent_inserted = ((ctx.inserted_rows/ctx.input_rows)*100) + '%';
	ctx.percent_updated = ((ctx.updated_rows/ctx.input_rows)*100) + '%';
	$("#stats-modal .modal-content").html(lodr.dash._job_stats(ctx));
});

(function() {
	
	$(document).on('click', 'a',function(e){
		if($(e.currentTarget).parent().data("scroll")){
			lodr.scrollTop = $("body").scrollTop();
		}else{
			lodr.scrollTop = 0;
		}			
	});
	

  addEventListener("turbolinks:render", function() {
	  $("body").scrollTop(lodr.scrollTop);
  })
})();