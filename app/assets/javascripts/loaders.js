$(function(){
	$.fn.serializeObject = function()
	{
	    var o = {};
	    var a = this.serializeArray();
	    $.each(a, function() {
	        if (o[this.name] !== undefined) {
	            if (!o[this.name].push) {
	                o[this.name] = [o[this.name]];
	            }
	            o[this.name].push(this.value || '');
	        } else {
	            o[this.name] = this.value || '';
	        }
	    });
	    return o;
	};
	String.prototype.replaceAt=function(index, character) {
	    return this.substr(0, index) + character + this.substr(index+character.length);
	}
	
	Handlebars.registerHelper('typeHtml', function(type) {
		return lodr.utils.getTypeHtml(type);	
	});
	Handlebars.registerHelper('safeId', function(id) {
		return lodr.utils.getSafeId(id);
	});
	Handlebars.registerHelper('if_eq', function(a, b) {
	    return a == b;
	});
});

$(document).on('click','#wizard-next-btn',function(e){
	e.preventDefault();
	lodr.track('loaders', 'wizard-next', '');
	$("form.next-submit-form").submit();
});
$(document).on('click','#save-exit',function(e){
	e.preventDefault();
	lodr.track('loaders', 'save-and-exit', '');
	
	$f = $("form#wrangler-form");
	if($f.length){
		$.ajax({
		  	type: "PATCH",
		  	url: $("#save-exit").attr("href"),
			data: $f.serialize()   
		});
	}else{
		Turbolinks.visit($("#save-exit").attr("href"));
	}	
	
});

$(document).on('click','.command-script button.close',function(e){
	if(confirm('All following commands will be deleted as well. Continue?')){
		lodr.track('loaders', 'delete-command', '');
		lodr.delete_command($(e.currentTarget).data("command-index"));
	}else{
		
	}	
});

// setup the transform column drop down menu interactions
$(document).on('shown.bs.dropdown','.transforms .col-dropdown', function (e) {	
	var $e = $(e.currentTarget);
	
	var menu = "tranforms";
	if($e.hasClass("data-type-btn")) menu = "change-data-type";	
	lodr.track('loaders', 'header-menu', menu);
	
	var $d = $e.find('.dropdown-menu:first');
	var $w = $(window);
	var left = $e.offset().left;

	if( (left + 300) > ($w.width()-10)){
		left = $w.width() - 300 - 10;
	}
	$d.clone().appendTo("body").css({top: $e.offset().top+28, left: left, display: 'block'});
});
$(document).on('hide.bs.dropdown','.transforms .col-dropdown', function (e) {
	$('body > .dropdown-menu').remove();
});

// sort setup
// $(document).on('click','.transforms .sort-btn', function (e) {
// 	var $e = $(e.currentTarget);
// 	var n = $e.parent(".col-header").find(".col-title").text();
// 	var dir = 'desc';
//
// 	if($e.hasClass('desc')){
// 		dir = 'asc';
// 	}
//
// 	lodr.controller.sort(n,dir);
// 	lodr.utils.sortedIndex = lodr.utils.colIndex(n);
//
// });

var lodr = lodr || {};
$.extend(lodr, {
	offset: { row: 0, col: 0	},
	_colHdr: function(ctx){
		if(lodr.__colHdr) return lodr.__colHdr(ctx);
		
		lodr.__colHdr = Handlebars.compile($("#_colHdr").html());
		return lodr.__colHdr(ctx);
	},
	transformations: [],	
	updateForm: function(){
		$f = $("form:first");
		$f.find("#loader_column_settings").val(JSON.stringify(lodr.headers));
		$f.find("#loader_transformations").val(JSON.stringify(lodr.transformations));
	},
	renderWranglerTable: function(){
		
		if(lodr.t_table){
			lodr.offset.col = lodr.t_table.colOffset();
			lodr.offset.row = lodr.t_table.rowOffset();
		}
		
		lodr.resize();
		lodr.timing_start = performance.now();
		lodr.render_time = 0;
				
		lodr.wait.show("Rendering...",{dialogSize: 'sm',progressType: 'warning'});
		lodr.updateForm();
				
		var r0=performance.now();
		
		if(lodr.t_table){
			lodr.t_table.destroy();
		}
		
		lodr.t_table = new Handsontable($("#table-container").get(0), {
			  data: lodr.rows,
			  rowHeaders: true,
			  colHeaders: function(col){ 
				  				return lodr._colHdr(lodr.headers[col]) ; 
			  			  },			 
			  dropdownMenu: false,
			  stretchH: 'all',
			  manualColumnResize: true,
			  readOnly: true,
			  filters: true,
			  columnSorting: false,
			  columnHeaderHeight: 35,
			  copyPaste: true,
			  search: true,
			  afterRender: function(){
				
				// if(lodr.utils.sortedIndex){
// 				  	var $b = $("#table-container thead th:nth-child(" + (lodr.utils.sortedIndex+2) + ") .sort-btn");
// 				  	var dir = "desc";
// 					if($b.hasClass("asc")) dir = 'asc';
// 				  	if(dir == "asc"){
// 				  		$b.addClass("asc");
// 				  		$b.find("i").removeClass("fa-sort-amount-desc").addClass("fa-sort-amount-asc");
// 				  	}else{
// 				  		$b.addClass("desc");
// 				  		$b.find("i").removeClass("fa-sort-amount-asc").addClass("fa-sort-amount-desc");
// 				  	}
// 				}
			  	
			  }
		});
		lodr.wait.update({width: "100%"});	
		
		setTimeout(function(){
			lodr.reset();			
		},150);
		
		var r0=performance.now();
		lodr.$g = $("#table-container");
		lodr.controller.validateData();
		// console.log("validation time: " + (performance.now()-r0));
		lodr.wait.hide();
		lodr.render_time += (performance.now() - r0);
		
		var offsetplus = (lodr.rows.length - lodr.offset.row) < 10 ? 0 : 10;
		lodr.t_table.selectCell(lodr.offset.row+offsetplus, lodr.offset.col);
		lodr.t_table.deselectCell();		
	},
	
	reset: function(){	
		$('th .dropdown-toggle').dropdown();		
			
		$('.col-dropdown').on('show.bs.dropdown',function(e){
			$(e.currentTarget).parents('th').css("overflow","visible");
		});
		$('.col-dropdown').on('hide.bs.dropdown',function(e){
			$(e.currentTarget).parents('th').css("overflow","hidden");
		});
	},
	
	delete_command: function(index){
		lodr.transformations.splice(index);
		lodr.reapply_commands();
		lodr.renderWranglerTable();
	},
	
	reapply_commands: function(){
		try{
			$("#transforms .command-script").remove();
			lodr.reset_data();
			var do_commands = ["delete", "upper_case", "lower_case", "titleize", "capitalize", "snake_case", "trim", "merge_all_rows", "insert_file_name"];
			$.each(lodr.transformations, function(i, cm){
				if(do_commands.indexOf(cm.command) > -1){
					var sc = Handlebars.compile($("#_script_do").html());
					eval("lodr.commands."+cm.command+"(cm.columnName)");	
					if(cm.command != "delete" && cm.command != "rename"){
						
						if(cm.command == "merge_all_rows" || cm.command == "insert_file_name"){
							var cn = cm.command == "merge_all_rows" ? "all" : cm.columnName;
							$("#transforms").append(sc({prefix: '', command: cm.command, 
								columnName: cn, order: i}));
						}else{
							$("#transforms").append(sc({prefix: 'text to', command: cm.command, 
								columnName: cm.columnName, order: i}));
						}						
						
					}else{
						$("#transforms").append(sc({command: cm.command, 
							columnName: cm.columnName, order: i}));
					}
				}else{
					var opts = cm.opts;
					opts.col_name = cm.columnName;
					eval("lodr.commands."+cm.command+"(opts)");
					var sc = Handlebars.compile($("#_script_show").html());
					$("#transforms").append(sc({command: cm.command, columnName: opts.col_name, opts: cm.opts, order: i}));
				}
			});
			
			// need to ensure column data type changes are
			// applied. Was causing issues. this maybe redundant
			// with some command calls but need to ensure.
			$.each(lodr.headers, function(i,hd){
				cs = lodr.utils.lookupColumnSettings(hd.columnName);
				if(cs){
					hd.dataType = cs.dataType;
				}
			});
			
			if(lodr.transformations.length==0){ 
				$(".no-transforms").slideDown(); 
			}else{
				$(".no-transforms").slideUp();
			}
		}
		catch(err){
			$("#transforms").prepend('<div class="alert alert-danger"><button type="button" ' +
				'class="close" data-dismiss="alert" aria-label="Close">' +
  		  		'<span aria-hidden="true">&times;</span>' +
				'</button>'+err+'</div>');
			$("#transforms .command-script").remove();			
			lodr.reset_data();
			lodr.transformations.length = 0;
			lodr.renderWranglerTable();
			
			if(console){ console.log(err); }
			lodr.wait.hide();
			$(".modal-backdrop").remove();
		}
	},
	
	reset_data: function(){		
		lodr.headers = [];
		$.each(lodr.original_headers, function(i,hd){
			var nhd = {original_index: i};
			$.extend(nhd, hd);
			lodr.headers.push(nhd);
		});
		
		lodr.rows = [];		
		$.each(lodr.original_rows,function(i,row){
			lodr.rows.push(row.slice());
		});
	},
	
	resize: function(){
		if($(".transforms .transform-table-style").length == 0) return false;
		
		var $t = $("#table-container");		
		var w = $(window).height();
		var fh = $(".wizard-path").height();
		$t.height(w-$t.offset().top-fh);
		
		$tr = $("#transforms");
		$tr.height(w-$tr.offset().top-fh);
	}
});
$(document).on("turbolinks:load", function(){
	if($(".transforms .transform-table-style").length)
		$(window).on('resize', lodr.resize);
});

lodr.track = function(cat,action,label){
	if(typeof ga != "undefined"){
		ga('send','event', cat, action, label);
	}	
};

lodr.utils = {
	processJsonPath: function(opts, row){
		var path = opts.path.split(",");
	
		var jp = jsonpath;
		var expandArray = opts.expand_array == "on" ? true : false;
	
		var sample = { headers: [], rows: []};		
		$.each(path, function(ip, p){		
			sample.headers.push(lodr.utils.pathToColumnName(p));
			if(ip == 0){
				var res = jp.query(JSON.parse(row[lodr.utils.colIndex(opts.col_name)]), p.trim());
				if($.isArray(res) && expandArray && res.length){
					$.each(res,function(i,v){
						var val = v;
						if(typeof v == "object") val = JSON.stringify(v);
						sample.rows.push([val]);
					});
				}else{
					var val = res.length ? res : "";
					if(typeof val == "object") val = JSON.stringify(res);
					sample.rows.push([val]);
				}
			}else{
				var res = jp.query(JSON.parse(row[lodr.utils.colIndex(opts.col_name)]), p.trim());
				var level = lodr.utils.comparePaths(p, path[0]);
				if($.isArray(res) && level != "diff" && level != "lower" && expandArray && res.length){
					if(level == "higher" && res.length == 1){
						$.each(sample.rows,function(ir,r){
							var val = res[0];
							if(typeof val == "object") val = JSON.stringify(val);
							r.splice(ip,0,val);
						});	
					}else{
						$.each(res,function(ir,r){
							var val = r;
							if(typeof r == "object") val = JSON.stringify(r);
				
							if(sample.rows.length-1 < ir){
								var nr = [];
								for(var addcoli=0;addcoli<ip;addcoli++){
									nr.push("");
								}
								sample.rows.push(nr);
							}
							sample.rows[ir].splice(ip,0,val);
						});	
						
						if(res.length < sample.rows.length && res.length){
							for(var ar=res.length-1;ar<sample.rows.length;ar++){
								sample.rows[ar].splice(ip,0,"");
							}
						}
					}				
				}else{
					var val = res.length ? res : "";
					if(typeof val == "object") val = JSON.stringify(res);
					$.each(sample.rows,function(ir,r){					
						r.splice(ip,0,val);
					});
				}			
			}
		});
		
		return sample;
	},
	
	comparePaths: function(p1,p2){
		p1 = p1.trim().split(".");
		p2 = p2.trim().split(".");
		
		var level = "same";
		
	  	if(p1.length > p2.length){
	  		level = "lower"
			for(var i=0;i<p2.length-1;i++){
	  			if(p1[i] != p2[i]){
	  				level = "diff";
					break;
	  			}
	  		}
	  	}else if(p1.length < p2.length){
	  		level = "higher"
			for(var i=0;i<p1.length-1;i++){
	  			if(p1[i] != p2[i]){
	  				level = "diff";
					break;
	  			}
	  		}
	  	}else{
			for(var i=0;i<p1.length-1;i++){
	  			if(p1[i] != p2[i]){
	  				level = "diff";
					break;
	  			}
	  		}
	  	}
		return level;
	},
	
	pathToColumnName: function(path){
		var p = path.trim();
		return p.replace(/(\.|\s)+/g, "_").replace(/[^a-zA-Z0-9_]+/g,"").replace(/^_/,"").substring(0,127);
	},
	
	colIndex: function(col_name){
		var index = -1;
		for(var i=0;i<lodr.headers.length;i++){
			if(lodr.headers[i].columnName.trim() === col_name.trim()){
				index = i;
				break;
			}
		}
		return index;
	},
	getHeader: function(col_name){
		var index = -1;
		var hd;
		for(var i=0;i<lodr.headers.length;i++){
			if(lodr.headers[i].columnName.trim() === col_name.trim()){
				index = i;
				hd = lodr.headers[i];
				break;
			}
		}
		return hd;
	},
	getHeaderCells: function(col_name){
		var i = lodr.utils.colIndex(col_name) +1;
		return lodr.$g.find("thead tr th:nth-child(" + i +")");
	},
	getRowCells: function(col_name){
		var i = lodr.utils.colIndex(col_name) +2;
		return lodr.$g.find("tbody tr td:nth-child(" + i + ")");
	},
	removeColumn: function(col_name){
		var i = lodr.utils.colIndex(col_name);
		lodr.headers.splice(i,1);
		$.each(lodr.rows,function(ii,row){
			row.splice(i,1);
		});
		// lodr.renderWranglerTable();
	},
	uniqueColName: function(col_name){
		col_name = col_name.trim() == "" ? "COLUMN" : col_name;
		var i = 0;
		var nc = col_name;
		while(lodr.utils.colIndex(nc)>=0){
			nc = col_name+"_"+i;
			i++;
		}
		return nc;
	},
	getSafeId: function(id){
		return id.replace(/\s/g, "_");
	},
	getTypeHtml: function(type){
		
		var type_html;
		if(type=="INTEGER"){
			type_html='<i class="fa fa-hashtag" aria-hidden="true"></i>';
		}else if(type=="DECIMAL"){
			type_html='<i class="fa text-icon" aria-hidden="true">#.#</i>';
		}else if(type=="DATE"){
			type_html = '<i class="fa fa-calendar" aria-hidden="true"></i>';
		}else if(type=="DATETIME" || type=="TIMESTAMP"){
			type_html = '<i class="fa fa-clock-o " aria-hidden="true"></i>';				
		}else if(type=="BOOLEAN"){
			type_html = '<i class="fa fa-toggle-on" aria-hidden="true"></i>';
		}else{
			type_html = '<i class="fa text-icon text-icon-string" aria-hidden="true">abc</i>';
		}
		return type_html;
	},
	lookupColumnSettings: function(col_name){
		var hd;
		for(var i=0;i<lodr.column_settings.length;i++){
			var cs = lodr.column_settings[i];
			if(col_name.toLowerCase().trim()==cs.columnName.toLowerCase().trim()){
				hd = cs;
				break;
			}
		}
		return hd;
	}
}

// JSON Viewer wiring
$(document).on('keyup', '#transform_extract_json input.json-search', function(e){
	if(lodr.json_search_timeout){
		clearTimeout(lodr.json_search_timeout);
	}
	
	var val = $(e.currentTarget).val();
	if(val.trim() == "") {
		$("#transform_extract_json .json-view .pathNode.highlight").removeClass("highlight");
		return false;
	}
	
	lodr.json_search_timeout = setTimeout(function(){
		$("#transform_extract_json .json-view .pathNode").each(function(i,el){
			$el = $(this);
			$el.removeClass("highlight");
			
			if($el.text().indexOf(val) > -1){
				$el.addClass("highlight");
				var btn = $el.parents(".json_item").find("> .btn.json-tree-node");
				if(btn.attr("aria-expanded") == "false"){
					btn.trigger('click');
				}				
			}
		});
	}, 250);	
});
$(document).on('keydown', '#transform_extract_json input.jsonPathDefinition', function(e){
	if(event.keyCode == 13) {
	      event.preventDefault();
		  $('#transform_extract_json input.jsonPathDefinition').trigger('change');
	      return false;
	 }
});
$(document).on('click','#transform_extract_json .clear-input',function(e){
	$(e.currentTarget).parents(".input-group").find("input").val("");
	$('#transform_extract_json input.jsonPathDefinition').trigger('change');
});
$(document).on('click', '#transform_extract_json .btn.json-tree-node',function(e){
	var $e = $(e.currentTarget);
	if($e.hasClass("collapsed")){
		$(e.currentTarget).find("i.fa").removeClass("fa-minus-square-o").addClass("fa-plus-square-o");
	}else{
		$(e.currentTarget).find("i.fa").removeClass("fa-plus-square-o").addClass("fa-minus-square-o");
	}	
});
$(document).on('click','#transform_extract_json .pathNode',function(e){
	$e = $('#transform_extract_json input.jsonPathDefinition');
	var p = $(e.currentTarget).data('json-path');
	
	if($e.val().trim() != ""){
		$e.val($e.val()+", "+p);
	}else{
		$e.val(p);
	}
	$e.trigger('change');
});
$(document).on('click','#transform_extract_json form input[name=expand_array]',function(e){
	$('#transform_extract_json input.jsonPathDefinition').trigger('change');
});
$(document).on('click','#transform_extract_json .btn.row-increment',function(e){
	if(lodr.controller.rowNumber+1 >= lodr.rows.length) return false;
	lodr.controller.rowNumber++;
	$('#transform_extract_json .json-view .scroll-wrapper').remove();
	lodr.controller.showJSONViewer($('#transform_extract_json input[name=col_name]').val());
	$('#transform_extract_json input.jsonPathDefinition').trigger('change');	
});
$(document).on('click','#transform_extract_json .btn.row-decrement',function(e){
	if(lodr.controller.rowNumber == 0) return false;
	lodr.controller.rowNumber--;
	$('#transform_extract_json .json-view .scroll-wrapper').remove();
	lodr.controller.showJSONViewer($('#transform_extract_json input[name=col_name]').val());	
	$('#transform_extract_json input.jsonPathDefinition').trigger('change');
});
$(document).on('change','#transform_extract_json input.jsonPathDefinition',function(e){
	e.stopPropagation();
	e.preventDefault();
	
	var path = $(e.currentTarget).val();
	if(path.trim() == "") {
		$('#transform_extract_json .table-view .proccessed-table')
		.html("");
		return false;
	}
	
	var opts = $("#transform_extract_json form").serializeObject();
	opts.path = path;
	
	try{
		var sample = lodr.utils.processJsonPath(opts, lodr.rows[lodr.controller.rowNumber]);
	
		var source   = $("#_json_sampler").html();
		var template = Handlebars.compile(source);
		$('#transform_extract_json .table-view .proccessed-table')
			.html(template(sample));
	}catch(err){
		$('#transform_extract_json .table-view .proccessed-table')
			.html('<div class="alert alert-danger">' + err + '</div>');
		
		if(console){ console.log(err); }
	}	
});
lodr.controller ={
	rowNumber: 0,
	jsonNodeId: 0,
	
	buildNode: function(node, label, path){
		var ndType = "scalar";
		var ndDisplay = "";
		var nextPath = "";
		var thisPath = path;
				
		if($.isArray(node)){ 
			ndType = "array";
			ndDisplay = "[ ]";
			
			nextPath = path + '.' + label + '[*]';
			thisPath = nextPath;
		}else if(typeof node == "object"){
			ndType = "object";
			ndDisplay = "{ }";
			
			if($.isNumeric(label)){
				if(path == "$"){
					thisPath = path+"[*]";
				}else if(path.endsWith("[*]")){
					var l = path.lastIndexOf("[*]");
					thisPath = path.replaceAt(l,("[" +label+"][*]"));
				}else{
					thisPath = path + '.' + label + '[*]';
				}				
				nextPath = path ;
			}else{
				nextPath = path + '.' + label;
				thisPath = nextPath;
			}			
			
		}else{
			if($.isNumeric(label) && path == "$"){
				thisPath = path+"[*]";
			}else{
				thisPath = path + '.' + label ;	
			}							
		}
		
		if(label == 'JSON'){
			nextPath = path;
		}
				
		if(ndType == "scalar"){
			return '<span class="json-node-type ' + ndType + '"><i class="fa fa-circle-o fa-fw" aria-hidden="true"></i> ' + 
						'<span class="pathNode" data-json-path="' + thisPath + '"> ' + label + ':</span> <span class="scalar-val">' +node + '</span></span>';
		}
		
		var labelid = 'json-id-' + (lodr.controller.jsonNodeId++);
		var n = '<button class="btn json-tree-node" type="button" data-toggle="collapse" data-target="#' + labelid + '"' +					
					'aria-expanded="false" aria-controls="collapseExample">' +
				'<i class="fa fa-plus-square-o fa-fw" aria-hidden="true"></i>' +
		'<span class="json-node-type ' + ndType + '" >' + ndDisplay + '</span></button> ' + '<span class="pathNode" data-json-path="' + thisPath + '"> ' + label + '</span>' +		
		'<ul class="table-columns collapse fa-ul" id="' + labelid + '">';
		
		$.each(node,function(lb,val){
			n += '<li class="json_item" >' + lodr.controller.buildNode(val,lb,nextPath) +'</li>';
		});
		
		n += '</ul>';
		return n;
	},
	
	showJSONViewer: function(col){
		var j;
		var failedToParse = false;
		try{
			if(lodr.rows.length-1 < lodr.controller.rowNumber){
				lodr.controller.rowNumber = 0;
			}
			j = JSON.parse(lodr.rows[lodr.controller.rowNumber][lodr.utils.colIndex(col)]);
		}catch(err){
			failedToParse = true;
		}
		
		$v = $("body #transform_extract_json");
		if(typeof j != "object" || failedToParse){
			$v.find(".modal-body .row .json-view .alert").remove();
			$v.find(".modal-body .row .json-view").append('<div class="alert alert-danger">Not a valid JSON object</div>');
			return ;
		}
		var ht = $(window).height()*0.7;		
		$v.find(".modal-body .table-view, .modal-body .json-view").height(ht);
		$v.find(".json-view").append('<div class="scroll-wrapper">' + lodr.controller.buildNode(j,"JSON",'$') + '</div>');	
		$v.find(".json-tree-node:first").trigger('click');
		$('#transform_extract_json .btn.current-row').text("Row " + (lodr.controller.rowNumber+1) + " of " + lodr.rows.length );
	},
	
	show: function(view,e){
		e.preventDefault();
		e.stopPropagation();
		lodr.track('loaders', 'show-transform', view);
		
		$("th .open .dropdown-toggle").dropdown('toggle');
		
		var $p = $($(e.currentTarget).parents("ul").data('parent'));
		var col_name = $p.find(".col-title").text().trim();
		
		var ctx = {columnName: col_name, headers: lodr.headers};
		var v = Handlebars.compile($("#_"+view).html());
		$("body").append(v(ctx));
		
		var $v = $("body #transform_"+view);
		$v.modal();	
		$v.on('hide.bs.modal',function(e){ $v.remove(); });
		$v.on('shown.bs.modal',function(e){ 
			if(view == "extract_json"){
				lodr.controller.showJSONViewer(col_name);
				
			}
			$v.find("input[type=text],textarea").first().focus(); 
		});
		
		var handler = function(e){
			e.preventDefault();
			e.stopPropagation();
			
			// clear any preview timeouts
			if(lodr.lodrTransformTimeout){
					clearTimeout(lodr.lodrTransformTimeout);
			}
			
			if(!lodr.controller.validate(view)) return;
						
			var args = $("#transform_"+view).find("form").serializeObject();
			eval("lodr.commands."+view+"(args)");
			
			var sc = Handlebars.compile($("#_script_show").html());
			delete args.col_name;
			
			lodr.transformations.push({command: view, columnName: col_name, opts: args});
			lodr.renderWranglerTable();
			lodr.controller.hideTransform();
			
			$(".no-transforms").slideUp();
			$("#transforms").append(sc({command: view, columnName: col_name, opts: args, order: lodr.transformations.length-1}));
		}
		
		$v.find("form").on('submit',handler);
		$v.find("button#apply").on('click',handler);
		
		$v.modal('show');
	},
	
	validate: function(view){
		$v = $("body #transform_"+view);
		var isValid = true;
		$v.find("input[required=true]").each(function(i,req){
			$r = $(req);
			var val = $r.val();
			if($r.attr("type") == "text"){
				var blanks = $r.data("allow-blanks");
				if(val == undefined || val.length <= 0 ||
					(blanks == "false" && val.trim().length <=0) ){
						
					$r.parents(".form-group").addClass("has-error");
					isValid = false;
				}
			}else if($r.attr("type") == "number"){
				if(!$.isNumeric(val)){
					$r.parents(".form-group").addClass("has-error");
					isValid = false;
				}
			}
		});
		
		$v.find("textarea[required=true]").each(function(i,req){
			$r = $(req);
			var val = $r.val();
			var blanks = $r.data("allow-blanks");
			if(val == undefined || val.length <= 0 ||
				(blanks == "false" && val.trim().length <=0) ){
					
				$r.parents(".form-group").addClass("has-error");
				isValid = false;
			}
		});
		
		if(view == "rename"){
			var new_name = $v.find("#column-name").val();
			var $c = $v.find("#column-name");
			var orig_name = $v.find('input[name="col_name"]').val();
			if(new_name == orig_name){
				return isValid;
			}
			
			for(var ii=0;ii<lodr.headers.length;ii++){
				if(lodr.headers[ii].columnName == new_name){
					$v.find("#column-name").parents(".form-group").addClass("has-error");
					
					if($c.parents(".form-group").find(".help-block").length == 0){
						$c.parents(".form-group")
							.append('<span class="help-block">Column name has to be unique</span>');
					}
					
					isValid = false;
					break;
				}
			}						
		}
		
		if(view == "extract_pattern" && isValid){
			try{
				var $p = $v.find("#pattern");
				var x = new RegExp($p.val());
				$p.parents(".form-group").removeClass("has-error").find(".help-block").text("Enter your regex pattern");
			}catch(err){
				isValid=false;
				$p.parents(".form-group").addClass("has-error").find(".help-block").text(err);
			}			
		}
		
		if(isValid){ $v.find(".has-error").removeClass("has-error"); }
		return isValid;
	},
	
	do: function(command,e){
		e.preventDefault();
		e.stopPropagation();
		lodr.track('loaders', 'do-command', command);
		
		$("th .open .dropdown-toggle").dropdown('toggle');
		
		var $p = $($(e.currentTarget).parents("ul").data('parent'));
		var col_name = $p.find(".col-title").text().trim();
		eval("lodr.commands."+command+"(col_name)");
		
		var sc = Handlebars.compile($("#_script_do").html());
		
	    lodr.transformations.push({command: command, columnName: col_name});
		$(".no-transforms").slideUp();
		if(command != "rename"){
			lodr.renderWranglerTable();
			if(command == "merge_all_rows" || command == "insert_file_name"){
				var cn = command == "merge_all_rows" ? "all" : col_name;				
				$("#transforms").append(sc({prefix: '', command: command, 
					columnName: cn, order: lodr.transformations.length-1}));
			}else if(command == "delete"){
				$("#transforms").append(sc({command: command, 
					columnName: col_name, order: lodr.transformations.length-1}));
			}else{
				$("#transforms").append(sc({prefix: 'text to', command: command, 
					columnName: col_name, order: lodr.transformations.length-1}));
			}			
		}else{
			$("#transforms").append(sc({command: command, 
				columName: col_name, order: lodr.transformations.length-1}));
		}
	},
	
	hideTransform: function(){
		$(".transform-modal").modal('hide');
	},
	
	BOOLEANS: [1,0, true, false, "true", "false",
			   "t", "f", "1", "0", "y", "n", "yes", "no"],
	validateColumn: function(col_name){
		var hd = lodr.headers[lodr.utils.colIndex(col_name)];
		if(hd == undefined) return ;
		lodr.utils.getRowCells(hd.columnName).each(function(r,field){
			var $f = $(field);
			var hasError = false;
			if(hd.dataType == "INTEGER"){
				var v = $f.text();
				if(v != undefined && v.trim().length>0
					&& isNaN(parseInt($f.text()))){
					hasError = true;
				}else if(v != undefined && v.trim().length>0 
					&& !$.isNumeric(v)){
					hasError = true;
				}
			}else if(hd.dataType=="DECIMAL"){
				if(v != undefined && v.trim().length>0 && !$.isNumeric($f.text())){
					hasError = true;
				}
			}else if(hd.dataType == "BOOLEAN"){
				var v = $f.text();
				if(v != undefined && v.trim().length > 0 && lodr.controller.BOOLEANS.indexOf(v.trim().toLowerCase()) == -1 ){
					hasError = true;
				}
			}else if(hd.dataType == "DATE" || hd.dataType == "DATETIME"){
				var v = Date.parse($f.text());
				if(v != undefined && isNaN(v)){
					hasError = true;
				}
			}
			
			if(hasError){
				$f.addClass("danger");
			}else{
				$f.removeClass("danger");
			}		
		});		
	},
		
	validateData: function(){
		// crap is not working 
		return ;
		if(lodr.$g == undefined) return;
		for(var i=0;i<lodr.headers.length;i++){
			var hd = lodr.headers[i];
			lodr.controller.validateColumn(hd.columnName);
		}
	},
	
	changeType: function(col_name,new_type,e){
		e.preventDefault();
		e.stopPropagation();
		
		
		lodr.track('loaders', 'change-type', new_type);
		
		var oi = $(e.currentTarget).parents("th").data("original-index");
		if($.isNumeric(oi))
			lodr.original_headers[oi].dataType = new_type;
		
		$("th .open .dropdown-toggle").dropdown('toggle');
		var hd = lodr.utils.getHeader(col_name);
		lodr.utils.getRowCells(hd.columnName).each(function(i,cell){
			$(cell).removeClass().addClass("type_"+new_type);
		});
		
		hd.dataType = new_type;
		$("#col_"+lodr.utils.getSafeId(col_name) + " .data-type-btn button").html(lodr.utils.getTypeHtml(new_type));
		
		lodr.controller.validateColumn(col_name);
		lodr.updateForm();
	},
	
	sort: function(col_name, dir){
		if(lodr.t_table == undefined) return;
		
		var i = lodr.utils.colIndex(col_name);
		
		var comp;
		if(dir == 'asc'){
			comp = function(a, b) {
			   if (a[i] < b[i]) return -1;
			   if (a[i] > b[i]) return 1;
			   return 0;
			}
		}else{
			comp = function(a, b) {
			   if (a[i] < b[i]) return 1;
			   if (a[i] > b[i]) return -1;
			   return 0;
			}
		}		
		
		lodr.rows.sort(comp);
		lodr.t_table.render(); 
	}	
}

//execute commands
lodr.commands = {
	delete: function(col_name){
		lodr.utils.removeColumn(col_name);
	},
	rename: function(opts){
		var i = lodr.utils.colIndex(opts.col_name);
		// lodr.utils.getHeaderCells(opts.col_name).find(".col-title").text(opts.new_name);
		lodr.headers[i].columnName = opts.new_column_name;
		var cs = lodr.utils.lookupColumnSettings(opts.new_column_name);
		if(cs) lodr.headers[i].dataType = cs.dataType;		
	},
	move: function(opts){
		var index = lodr.utils.colIndex(opts.col_name);
		lodr.headers.move(index, opts.position-1);
		$.each(lodr.rows, function(i,row){
			row.move(index, opts.position-1);
		});
	},
	merge: function(opts){
		var index = lodr.utils.colIndex(opts.col_name);
		var targetIndex = lodr.utils.colIndex(opts.target_column_name);
		var del = opts.delimiter || "";
		
		// merge with self
		if(index == targetIndex){
			$.each(lodr.rows,function(i,row){
				var v = row[index];
				var d = (v && v.trim().length>-1) ? del : "";
				row[index] = v + "" + d + "" + v;
			});
		}else{
			// remove target
			lodr.headers.splice(targetIndex,1);
			$.each(lodr.rows,function(i,row){
				var d = (row[index] && row[index].length>-1 && row[targetIndex] && row[targetIndex].length>-1) ? del : "";
				var v = (row[index] + "") + d + (row[targetIndex] + "");
				row[index] = v;
				row.splice(targetIndex,1);
			});
		}
		
		var cs = lodr.utils.lookupColumnSettings(opts.col_name);
		if(cs) lodr.headers[index].dataType = cs.dataType;
	},	
	upper_case: function(col_name){
		var index = lodr.utils.colIndex(col_name);
		$.each(lodr.rows, function(i,row){
			if(typeof row[index] == "string"){
				row[index] = row[index].toUpperCase();
			}
		});
	},
	lower_case: function(col_name){
		var index = lodr.utils.colIndex(col_name);
		$.each(lodr.rows, function(i,row){
			if(typeof row[index] == "string"){
				row[index] = row[index].toLowerCase();
			}
		});
	},
	capitalize: function(col_name){
		var index = lodr.utils.colIndex(col_name);
		$.each(lodr.rows, function(i,row){
			if(typeof row[index] == "string"){
				var val = row[index].trim().toLowerCase();
				val = val.replaceAt(0,val.charAt(0).toUpperCase());
				row[index] = val;
			}
		});
	},
	titleize: function(col_name){
		var index = lodr.utils.colIndex(col_name);
		$.each(lodr.rows, function(i,row){
			if(typeof row[index] == "string"){
				var val = row[index].trim().toLowerCase().split(/\s/);
				for(var i=0;i<val.length;i++){
					val[i] = val[i].replaceAt(0,val[i].charAt(0).toUpperCase());
				}	
				row[index] = val.join(" ");
			}
		});
	},
	snake_case: function(col_name){
		var index = lodr.utils.colIndex(col_name);
		$.each(lodr.rows, function(i,row){
			if(typeof row[index] == "string"){
				var val = row[index].trim().toLowerCase().replace(/\s/g,"_").replace(/_+/g,"_");	
				row[index] = val
			}
		});
	},
	trim: function(col_name){
		var index = lodr.utils.colIndex(col_name);
		$.each(lodr.rows, function(i,row){
			if(typeof row[index] == "string"){
				row[index] = row[index].trim();
			}
		});
		
	},
	
	merge_all_rows: function(col_name){
		lodr.headers = [lodr.headers[0]];
		lodr.rows = [ [ lodr.rows.join(" ") ] ];
	},
	
	insert_file_name: function(col_name){
		var index = lodr.utils.colIndex(col_name);
		var header = {
			columnName: lodr.utils.uniqueColName("source_file_name"),
			dataType: "VARCHAR"
		}		
		lodr.headers.splice(index+1,0,header);
		
		$.each(lodr.rows,function(i,row){
			row.splice(index+1,0,lodr.source_file_name);
		});
	},
	
	split_delimiter: function(opts){		
		var original_index = lodr.utils.colIndex(opts.col_name);
		var index = original_index;			
		
		if(opts.limit == undefined || opts.limit<=0){
			opts.limit = 2;
			$.each(lodr.rows,function(i,row){
				if(row[original_index])
					opts.limit = Math.max(opts.limit , row[original_index].trim().split(opts.delimiter).length );
			});
		}
		
		var original = lodr.headers[index];
		var headers = [];
		
		for(var i=0;i<opts.limit;i++){
			var header = {
				columnName: lodr.utils.uniqueColName(original.columnName.trim()),
				dataType: "VARCHAR"
			}
			var cs = lodr.utils.lookupColumnSettings(header.columnName);
			if(cs) header.dataType = cs.dataType;
			
			headers.push(header);
			lodr.headers.splice(index+1,0,header);
							
			index++;		
		}		
		var flags="";
		if(opts.case_insensitive == "on"){
			flags = "i";
		}
		var re;
		if(opts.regex == "on"){
			re = new RegExp(opts.delimiter,flags);
		}else{
			re = opts.delimiter;
		}
		
		$.each(lodr.rows, function(i,row){
			var new_cells=row[original_index].split(re);
			index = original_index;
			for(var j=0;j<opts.limit;j++){
				if(j<new_cells.length){
					row.splice(index+1,0,new_cells[j]);
				}else{
					row.splice(index+1,0,"");
				}
				index++;				
			}
		});
		
		// check for retain original column
		// clear original if requested
		if(opts.retain_column == undefined || opts.retain_column=="off"){
			lodr.utils.removeColumn(opts.col_name);
		}
	},
	
	split_position: function(opts){		
		var original_index = lodr.utils.colIndex(opts.col_name);
		var index = original_index;			
				
		var original = lodr.headers[index];
		var headers = [];
		
		for(var i=0;i<2;i++){
			var header = {
				columnName: lodr.utils.uniqueColName(original.columnName.trim()),
				dataType: "VARCHAR"
			}
			var cs = lodr.utils.lookupColumnSettings(header.columnName);
			if(cs) header.dataType = cs.dataType;
			
			headers.push(header);
			lodr.headers.splice(index+1,0,header);
							
			index++;		
		}
		
		
		$.each(lodr.rows, function(i,row){
			var val = row[original_index].trim();
			var new_cells = [val.substring(0,opts.position), val.substring(opts.position)];
			index = original_index;
			for(var j=0;j<2;j++){
				if(j<new_cells.length){
					row.splice(index+1,0,new_cells[j]);
				}else{
					row.splice(index+1,0,"");
				}
				index++;				
			}
		});
		
		// check for retain original column
		// clear original if requested
		if(opts.retain_column == undefined || opts.retain_column=="off"){
			lodr.utils.removeColumn(opts.col_name);
		}
	},
	
	extract_between: function(opts){
		var index = lodr.utils.colIndex(opts.col_name);				
		var original = lodr.headers[index];
		var header = {
			columnName: lodr.utils.uniqueColName(original.columnName.trim()),
			dataType: "VARCHAR"
		}
		var cs = lodr.utils.lookupColumnSettings(header.columnName);
		if(cs) header.dataType = cs.dataType;
		
		lodr.headers.splice(index+1,0,header);
		if(opts.occurrence == undefined || opts.occurrence=="first"){			
			$.each(lodr.rows,function(i,row){
				var v = row[index];
				var s = v.indexOf(opts.begin) + opts.begin.length;
				var l = v.indexOf(opts.end, s);
				if(s<0||l<0){
					v = "";
				}else{
					v= v.substring(s,l);
				}
				row.splice(index+1,0,v);
			});
		}else{
			$.each(lodr.rows,function(i,row){
				var v = row[index];
				var s = v.lastIndexOf(opts.end);
				var l = v.lastIndexOf(opts.begin,s-1) + opts.begin.length;
				if(s<0||l<0){
					v = "";
				}else{
					v=v.substring(l,s);
				}
				row.splice(index+1,0,v);
			});
		}
	},
	
	extract_between_position: function(opts){
		// console.log(opts);
	},
	
	extract_pattern: function(opts){
		var original_index = lodr.utils.colIndex(opts.col_name);
		var index = original_index;
		var original = lodr.headers[index];
					
		var re = "";
		if(opts.pattern.indexOf("(")>-1){
			re = new RegExp(opts.pattern);
		}else{
			re = new RegExp("("+opts.pattern+")");
		}
				
		for(var i=0;i<opts.limit;i++){
			var header = {
				columnName: lodr.utils.uniqueColName(original.columnName.trim()),
				dataType: "VARCHAR"
			}
			var cs = lodr.utils.lookupColumnSettings(header.columnName);
			if(cs) header.dataType = cs.dataType;
						
			lodr.headers.splice(index+1,0,header);
			index++;
		}
		
		$.each(lodr.rows, function(i,row){
			index = original_index;
			var val = row[index];
			var limit = parseInt(opts.limit);
			var new_cells = Array(limit).fill("");
			
			match = re.exec(val);
			if(match != null){
				for(var j=1;j<match.length;j++){
					if(j > limit) break;
					var v = match[j] == undefined ? "" : match[j];
					new_cells[j-1] = v;
				}				
			}
			for(var i=0;i<new_cells.length;i++){
				row.splice(index+1,0,new_cells[i]);
				index++;
			}				
			
		});
	},
	
	find_replace: function(opts){
		var index = lodr.utils.colIndex(opts.col_name);
		var flags = "";
		var regex = opts.regex== "on" ? true : false;
		var ci = false;
		var ra = false;
		opts.replace = opts.replace ? opts.replace : '';
		
		if(opts.replace_all != undefined || opts.replace_all == "on"){
			flags += "g";
			ra = true;
		}
		if(opts.case_insensitive != undefined || opts.case_insensitive == "on"){
			flags += "i";
			ci = true;
		}
		
		if(regex){
			var re = new RegExp(opts.find, flags);
			$.each(lodr.rows, function(i,row){
				row[index] = row[index].replace(re, opts.replace);
			});
		}else{
			$.each(lodr.rows, function(i,row){
				var field = "";
				
				if(row[index]){
					field = row[index];
				}
				
				var f = ci ? opts.find.toLowerCase() : opts.find;
				if(ra){
					f = f.replace(/([\[\^\$\|\(\)\?\*\+\}\{\.])/gi,'\\$1');
					field = field.replace(new RegExp(f,flags), opts.replace);
				}else{
					field = field.replace(f, opts.replace);
				}
				row[index] = field;
			});
		}
	},
	
	extract_json: function(opts){
		if(opts.path.trim() == "") return false;
		var original_index = lodr.utils.colIndex(opts.col_name);
		var index = original_index;
		var original = lodr.headers[index];
		
		var path = opts.path.split(",");		
		var headers = [];
		$.each(path,function(ih, h){
			var header = {
				columnName: lodr.utils.uniqueColName(lodr.utils.pathToColumnName(h)),
				dataType: "VARCHAR"
			}
			var cs = lodr.utils.lookupColumnSettings(header.columnName);
			if(cs) header.dataType = cs.dataType;
			
			headers.push(header);
			lodr.headers.splice(index+1,0,header);
							
			index++;	
		});
		index = original_index;	
		var expandArray = opts.expand_array == "on" ? true : false;	
		
		var new_rows = [];
		$.each(lodr.rows, function(i,row){
			var data = lodr.utils.processJsonPath(opts, row);
			if(expandArray){				
				$.each(data.rows,function(ii,res){
					var new_row = row.slice(0);
					new_row.splice.apply(new_row, [index+1,0].concat(res) );
					new_rows.push(new_row);
				});
			}else{
				row.splice.apply(row, [index+1,0].concat(data.rows[0]));
				new_rows.push(row);		
			}
		});
		lodr.rows = new_rows;
		// check for retain original column
		// clear original if requested
		if(opts.retain_column == undefined || opts.retain_column=="off"){
			lodr.utils.removeColumn(opts.col_name);
		}
	},
	
	delete_rows: function(opts){
		lodr.rows.splice(opts.start-1, opts.end - opts.start +1);
	}
}

// wait page poller
$(document).on("turbolinks:load",function(e){
	if($(".loaders.wait").length){
		setTimeout(function(){
			Turbolinks.visit($(".wait-content").data("poll-path"));
		}, 2000);
		$(".wizard-path a").parent()
			.addClass("disabled")
			.on('click',function(e){
				e.preventDefault();
				e.stopPropagation();
		});
	}
	
	// index page poller
	if($(".loaders.index").length){
		function poll(){
			var pollers = $(".loader-status .queued, .loader-status .running").map(function(i,el){
				return $(el).data("loader-id");
			}).get();
			
			if(pollers.length >0){
				$.getScript('loaders/status/all?loaders=' + JSON.stringify(pollers));
				setTimeout(poll, 2000);
			}	
		}
		poll();
	}
	
	if($(".loaders.target_table").length){
		var $cs = $("#col-search");
		var $t = $(".table:first");
		$cs.keyup(function(e){
			var v = $cs.val();
			$t.find("tbody tr td:nth-child(2)").each(function(i,el){
				$e = $(el);
				if($e.text().toLowerCase().indexOf(v) == -1){
					$e.parent().addClass("hidden");
				}else{
					$e.parent().removeClass("hidden");
				}
			});
		});
	}
});

// Render transform preview on keyup
$(document).on('keyup click change',".transform-modal[data-previewable] .preview-trigger", function(e){
	var ctypes = ["radio", "checkbox"];
	var $d = $(e.currentTarget).parents(".transform-modal[data-previewable]");
	
	if(e.type == 'click' && ctypes.indexOf($(e.currentTarget).attr("type")) == -1){
		return;
	}
	
	if(lodr.lodrTransformTimeout){
		clearTimeout(lodr.lodrTransformTimeout);
	}
	
	var delay = 500;
	try{ delay =  parseInt($d.data('previewable')) }catch(err){ };		
	
	if(e.which == 13) delay = 1;
	
	lodr.lodrTransformTimeout = setTimeout(function(){
		var rows_last = lodr.rows.map(function(arr){ return arr.slice(0); });
		var hd_last = lodr.headers.map(function(obj){ return $.extend({},obj); });
		
		var args = $d.find("form").serializeObject();
		var preview_index = 0;
		try{ preview_index = parseInt($d.data('preview-index')); }catch(err){};	
		var col = lodr.utils.colIndex(args.col_name) + preview_index;
		// var $d = $(e.currentTarget).parents(".transform-modal[data-previewable]");
				
		var view = $d.attr("id").split("transform_")[1];
		if(!lodr.controller.validate(view)) return;
		if(view == "merge"){
			var target_col = lodr.utils.colIndex(args.target_column_name);
			if(target_col < col) col--;
		}		
		eval("lodr.commands."+view+"(args)");		
		
		// var preview = '<table class="table table-condensed"><thead><tr><th>Row</th>';
		
		if(args.limit){
			args.limit = parseInt(args.limit);			
		}else{
			args.limit = 1;
		}
		
		var hd = [],rows = [];
		for(var j=0;j<args.limit;j++){
			hd.push(lodr.headers[col+j]);
		}
		
		$.each(lodr.rows,function(i,row){
			rowly = [];
			for(var j=0;j<args.limit;j++){
				rowly.push(row[col+j]);
			}
			rows.push(rowly);
		});	
		
		$d.find(".preview-content").html("");
		if(lodr.p_table){
			lodr.p_table.destroy();
		}		
		lodr.p_table = new Handsontable($d.find(".preview-content").get(0), {
			  data: rows,
			  rowHeaders: true,
			  colHeaders: function(col){ return hd[col].columnName; },			 
			  dropdownMenu: false,
			  manualColumnResize: true,
			  stretchH: 'all',
			  readOnly: true,
			  columnSorting: true,
			  copyPaste: true
		});
		
		// for(var j=0;j<args.limit;j++){
		// 	preview += "<th>"+lodr.headers[col+j].columnName +"</th>";
		// }
		// preview += "</tr></thead><tbody>";
		//
		// $.each(lodr.rows,function(i,row){
		// 	preview += '<tr><td class="row-num">' +i + "</td>";
		// 	for(var j=0;j<args.limit;j++){
		// 		preview += "<td>" + row[col+j] +"</td>";
		// 	}
		// 	preview += "</tr>";
		// });
		// preview += "</tbody></table>";
		//
		// $d.find(".preview-content").html(preview);
		lodr.rows = rows_last;
		lodr.headers = hd_last;
	}, delay);
	
});
















