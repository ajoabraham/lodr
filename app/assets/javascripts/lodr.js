var lodr = lodr || {};
lodr.wait = (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header">' +
				'<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
				'<h3 style="margin:0;"></h3></div>' +
			'<div class="modal-body">' +
				'<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 1%"></div></div>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Rendering...';
			}
			var settings = $.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null  // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {
				$dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
			}
			$dialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);					
				});
			}
			
			// check if its in the body still
			if($("body .modal").length==0){
				$dialog.appendTo("body");
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
			setTimeout(function(){ if(lodr.wait.isVisible()) lodr.wait.forceHide(); }, 1500);
		},
		
		update: function(opts){
			var settings = $.extend({
				message: "Rendering...",
				width: "100%"
			}, opts);
			$dialog.find('h3').text(settings.message);
			$dialog.find(".progress-bar").width(settings.width);			
		}, 
		
		isVisible: function(){
			return $dialog.is(":visible");
		},
		
		forceHide: function(){
			$dialog.modal();
			$dialog.modal('hide');
		}
	};

})(jQuery);

lodr.waitSpin = (function ($) {
    'use strict';

	// Creating modal dialog's DOM
	var $dialog = $(
		'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
		'<div class="modal-dialog modal-m">' +
		'<div class="modal-content">' +
			'<div class="modal-header"><h5 style="margin:0;"></h5></div>' +
			'<div class="modal-body">' +
				'<div class="text-center"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div>' +
				'<p class="help-block"></p>' +
			'</div>' +
		'</div></div></div>');

	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {
				options = {};
			}
			if (typeof message === 'undefined') {
				message = 'Waiting...';
			}
			var settings = $.extend({
				subtitle: '',
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);

			// Configuring dialog
			$dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			$dialog.find('.modal-body').attr('class', 'modal-body'); // reset types
			if (settings.progressType) {
				$dialog.find('.modal-body').addClass('text-' + settings.progressType);
			}
			$dialog.find('h5').text(message);
			$dialog.find('.help-block').text(settings.subtitle);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {
				$dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
					settings.onHide.call($dialog);
				});
			}
			
			// check if its in the body still
			if($("body .modal").length==0){
				$dialog.appendTo("body");
			}
			// Opening dialog
			$dialog.modal();
		},
		/**
		 * Closes dialog
		 */
		hide: function () {
			$dialog.modal('hide');
		},
		
		update: function(opts){
			var settings = $.extend({
				message: "Waiting...",
				subtitle: ""
			}, opts);
			$dialog.find('h5').text(settings.message);
			$dialog.find('.help-block').text(settings.subtitle);			
		}
	};

})(jQuery);
