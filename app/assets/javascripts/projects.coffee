# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on "show.bs.modal", "#invite-users, #project-settings", (e) ->
	wait = 	"""
				<div class="modal-content">
			      
				  <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="exampleModalLabel">
			        	<i class="fa fa-cog fa-spin"></i>	Loading UI...		
			        </h4>
			      </div>
			      
				  <div class="modal-body p-a-md text-center" style="max-height: 400px;overflow-y:auto;color:#c0cdd8;">
						<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
						<span class="sr-only">Loading...</span>
				  </div>
			      
				  <div class="modal-footer">
			        <button type="button" class="btn btn-default-outline" data-dismiss="modal">Cancel</button>
			      </div>
				  
				 </div>
			"""
	$(e.currentTarget).find(".modal-dialog").html wait

$(document).on "shown.bs.modal", "#invite-users, #project-settings", (e) ->
	$(e.currentTarget).find(".modal-dialog").load $(e.currentTarget).data("remote")