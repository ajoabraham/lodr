lodr.schedule = {
	init: false,
	build: function(){
		var fd = $(".schedulable").parents("form").serializeObject();
		var k = Object.keys(fd);
		var schedulable = "";
		for(var i=0; i< k.length; i++){
			if(k[i].indexOf("[schedule_type]") > 0){
				schedulable = k[i].split("[")[0];
				break;
			}
		}
	
		lodr.schedule.schedulable = schedulable;
		
		lodr.schedule.data = {
			schedule_type: fd[schedulable+"[schedule_type]"][1],
			start_date: fd[schedulable+"[start_date]"],
			start_time: fd[schedulable+"[start_time]"],
			time_zone: fd[schedulable+"[time_zone]"],
			schedule_period: fd[schedulable+"[schedule_period]"],
			day_of_month: fd[schedulable+"[day_of_month]"],
			days_of_week: fd[schedulable+"[days_of_week][]"],
			months_of_year: fd[schedulable+"[months_of_year][]"],
			weeks_of_month: fd[schedulable+"[weeks_of_months][]"]
		}
		
		return schedulable;
	}
};

$(document).on('shown.bs.modal', '#scheduleModal', function (evt) {
	var schedulable = lodr.schedule.build();
	if(schedulable.trim() == ""){
		if(console){
			console.log("Could not find schedulable object.")
		}
		return false;
	}
	
	
	var st = lodr.schedule.data.schedule_type;
	if(st == "schedule_none"){
		lodr.schedule.none();
	}else if(st == "schedule_once"){
		lodr.schedule.once();
	}else if(st == "schedule_hourly"){
		lodr.schedule.hourly();
	}else if(st == "schedule_weekly"){
		lodr.schedule.weekly();
	}else if(st == "schedule_daily"){
		lodr.schedule.daily();
	}else if(st == "schedule_monthly"){
		lodr.schedule.monthly();
	}else if(st == "schedule_yearly"){
		lodr.schedule.yearly();
	}


	$(document).on('click', "#" + schedulable + "_schedule_type_schedule_none", function(){ lodr.schedule.none(); });
	$(document).on('click', "#" + schedulable + "_schedule_type_schedule_once", function(){ lodr.schedule.once(); });
	$(document).on('click', "#" + schedulable + "_schedule_type_schedule_hourly", function(){ lodr.schedule.hourly(); });
	$(document).on('click', "#" + schedulable + "_schedule_type_schedule_weekly", function(){ lodr.schedule.weekly(); });
	$(document).on('click', "#" + schedulable + "_schedule_type_schedule_daily", function(){ lodr.schedule.daily(); });
	$(document).on('click', "#" + schedulable + "_schedule_type_schedule_monthly", function(){ lodr.schedule.monthly(); });
	$(document).on('click', "#" + schedulable + "_schedule_type_schedule_yearly", function(){ lodr.schedule.yearly(); });
});

$(document).on('hide.bs.modal', '#scheduleModal', function (evt) {
	lodr.schedule.build();
	if(lodr.schedule.data.schedule_type == "schedule_none" ){
		$("a.schedule-edit-btn").html(
			"<i class=\"fa fa-calendar-plus-o \" aria-hidden=\"true\"></i> Add Schedule"
		);
	}else{
		$("a.schedule-edit-btn").html(
			"<i class=\"fa fa-calendar-check-o \" aria-hidden=\"true\"></i> Configured " + lodr.schedule.data.schedule_type.split("_")[1] + " schedule"
		);
	}
	
});

lodr.schedule.none = function(){
	var $o = $("#start-time-options, #periodic-option, #month-of-year-option, #week-of-month-option, #day-of-week-option, #day-of-month-option");
	$o.fadeOut();
}

lodr.schedule.once = function(){
	var $o = $("#periodic-option, #month-of-year-option, #week-of-month-option, #day-of-week-option, #day-of-month-option");
	$o.fadeOut();
	$("#start-time-options").fadeIn();
}

lodr.schedule.hourly = function(){
	$(".period-type").text(" Hour(s) ");
	var $o = $("#month-of-year-option, #day-of-month-option, #week-of-month-option");
	$o.fadeOut();
	var $i = $("#start-time-options, #periodic-option, #day-of-week-option");
	$i.fadeIn();
}

lodr.schedule.weekly = function(){
	$(".period-type").text(" Week(s) ");
	var $o = $("#month-of-year-option, #day-of-month-option, #week-of-month-option");
	$o.fadeOut();
	var $i = $("#start-time-options, #periodic-option, #day-of-week-option");
	$i.fadeIn();
}

lodr.schedule.daily = function(){
	$(".period-type").text(" Day(s) ");
	var $o = $("#month-of-year-option, #day-of-month-option, #week-of-month-option, #day-of-week-option");
	$o.fadeOut();
	var $i = $("#start-time-options, #periodic-option");
	$i.fadeIn();
}

lodr.schedule.monthly = function(){
	$(".period-type").text(" Month(s) ");
	var $o = $("#month-of-year-option");
	$o.fadeOut();
	var $i = $("#start-time-options, #periodic-option, #week-of-month-option, #day-of-week-option, #day-of-month-option");
	$i.fadeIn();
}

lodr.schedule.yearly = function(){
	$(".period-type").text(" Year(s) ");
	var $i = $("#start-time-options, #month-of-year-option, #periodic-option, #week-of-month-option, #day-of-week-option, #day-of-month-option");
	$i.fadeIn();
}