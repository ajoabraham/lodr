Handlebars.registerHelper('normalizeSize', function(size) {
  if(size < 1024){
		return size + " Bytes";
	}
	
	var mb = (1024 * 1024);
  if(size < mb){
		return Math.round(size/1024) + " KB";
	}
	
	var gb = (1024 * 1024 * 1024);
  if(size < gb){
		return Math.round(size/mb) + " MB";
	}
	
	var tb = (1024 * 1024 * 1024 * 1024);
  return Math.round(size/gb) + " GB";
	
});

var lodr = lodr || {};

$.extend(lodr, {
	getAcceptTypes: function(){
		if(lodr._accept_types == undefined){
			lodr._accept_types = $("#drop-zone").data("file-types");
		}
		return lodr._accept_types;
	},
	initFileUpload: function(){
		try{
			$('#js-upload-files').fileupload('destroy');
		}catch(e){ 
			// ignore 
		}
		$("#drop-zone").data("drop-ready",true);
		this._fileItem = Handlebars.compile($("#_fileItem").html());
		
		$(document).on('dragover',function(){
			$("#drop-zone").addClass("active-drop");
		});
		
		$(document).on('dragend drop dragexit',function(){
			$("#drop-zone").removeClass("active-drop");
		});	
		
    var form         = $('.direct-upload');	    
    var overrallProgressBar  = $(".overall .progress-bar");
	var fileInput = $('#js-upload-files');
				
    fileInput.fileupload({
		acceptFileTypes: /(\.|\/)(csv|tsv|psv)$/i, 
	    fileInput:       fileInput,
	    url:             form.data('url'),
	    type:            'POST',
	    autoUpload:       true,
	    formData:         form.data('form-data'),
	    paramName:        'file', // S3 does not like nested name fields i.e. name="user[avatar_url]"
	    dataType:         'XML',  // S3 returns XML if success_action_status is set to 201
	    replaceFileInput: false,
		 
		add: function(e, data){
			var fl = data.files[0];
			var parts = fl.name.split(".");
			var ext = parts.length > 1 ? parts[parts.length-1] : "";
			
			if(ext == "" || lodr.getAcceptTypes().indexOf(ext.toLowerCase())>-1){   			 	
   			 	data.context = $(lodr._fileItem(data.files[0]));					 
   			 	$(".file-status tbody").append(data.context);					 					 
   			 	data.submit();
			}else{
				fl.error = "Unsupported file type: " + ext ;
   			 	data.context = $(lodr._fileItem(fl));					 
   			 	$(".file-status tbody").append(data.context);	
			}
			$(".file-upload-status").fadeIn();
		},		 
		progress: function(e,data){
			var progress = parseInt(data.loaded / data.total * 100, 10);
			data.context.find(".progress-bar").css('width', progress + '%')
							.text((progress + '%'));
		},
		progressall: function (e, data) {
			 var progress = parseInt(data.loaded / data.total * 100, 10);
			 overrallProgressBar.css('width', progress + '%')
			 						.text("loading files " + (progress + '%'));
		},     
		
		start: function (e) {
	        overrallProgressBar.
	          css('display', 'block').
	          css('width', '0%').
	          text("Upload Loading files...");
      	},
     
		done: function(e, data) {
	        overrallProgressBar.text("All Files Uploaded");

	        // extract key and generate URL from response
	        var key   = $(data.jqXHR.responseXML).find("Key").text();
	        var url   = '//' + form.data('host') + '/' + key;

	        // create hidden field
	        var input = $("<input />", { type:'hidden', name: fileInput.attr('name'), value: url })
	        form.append(input);
			 
			lodr.fileUploaded(data.files[0], data.context);
			 
      	},
     
		fail: function(e, data) {
	        overrallProgressBar.
	          css("background", "red").
	          text("Failed");
				 
				 data.context.find(".badge")
				 	.removeClass("alert-info")
				 	.addClass("alert-danger")
				 	.text("Failed");	 
      	  }
    	});
	},
	
	fileUploaded: function(file, $el){
		var d = {
				name: file.name, 
				size: file.size,
				file_type: file.type };
		
		$el.find(".badge")
				.text("Pre Processing...");
				
		$.post( '/sources', d, 'json')
			  .done(function(data) {
				$el.find(".badge")
		 			.removeClass("alert-info")
		 			.addClass("alert-success")
		 			.text("Success");
				$el.find(".progress .progress-bar").addClass("progress-bar-success");
			  })
			  .fail(function(data) {
					$el.find(".badge")
			 			.removeClass("alert-info")
			 			.addClass("alert-danger")
				  		.text("Failed");
					$el.find(".progress .progress-bar").addClass("progress-bar-danger");
					$el.find(".badge").tooltip({title: data.responseText });
			  }); 
	},
	
	pollFileStatus: function(){
		var pollers = $(".index .pre_processing, .index .profiling").map(function(i,el){
			return $(el).data("file-id");
		}).get();
		
		if(pollers.length >0){			
			$.getScript('sources/status/all?files=' + JSON.stringify(pollers));
			setTimeout(lodr.pollFileStatus, 3000);
		}
	},
	
	pollUnzipStatus: function(){
		var pollers = $(".unzipping");
		
		if(pollers.length >0){
			$.getScript('sources/status/zips');
			setTimeout(lodr.pollUnzipStatus, 5000);
		}
	}
		
	
}); // end lodr object

$(document).on("turbolinks:load", function(e){
	$('[data-toggle="tooltip"]').tooltip();
	
	if($(".sources.upload").length && $("#drop-zone").data("drop-ready") == undefined){
		lodr.initFileUpload();
	}
	
	if($(".sources.index").length){
		lodr.pollFileStatus();
		lodr.pollUnzipStatus();
	}		
});


// handle file delete
$(document).on("ajax:beforeSend", "a.delete-input-file", function(e) {
  var cell = $(e.currentTarget).parent().parent().parent();
  cell.addClass("disabled");
  $(e.currentTarget).addClass("disabled").html('<i class="fa fa-spinner fa-spin"></i>&nbsp; Deleting...');
});

