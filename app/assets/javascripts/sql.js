var lodr = lodr || {};

lodr.sql = {
	searchDB: function(kw){
		$("#database .tree-node.table-item")
			.each(function(i,el){ 
				var $e = $(el);
				var hasMatchingCol = false;
				var $colN = $($e.data('target')).find(".table-column .db-table-col-name");
				$colN.each(function(ii,cel){
					if($(cel).text().indexOf(kw) > -1){
						hasMatchingCol = true;
					}
				});
				
				if( $e.find(".db-table-name").text().indexOf(kw) == -1 && !hasMatchingCol ){
					$e.collapse('hide').hide();
				}else if(hasMatchingCol){
					$colN.each(function(ii,cel){
						var $cel = $(cel);
						var ct = $cel.text();
						if(ct.indexOf(kw) == -1){
							$cel.parent().hide();
						}else{
							$cel.html(ct.replace(kw, '<span class="highlight">'+kw+'</span>'));
						}
					});
				}
				
				var tn = $e.find(".db-table-name").text();
				if(tn.indexOf(kw) > -1){
					$e.find(".db-table-name").html(tn.replace(kw, '<span class="highlight">'+kw+'</span>'));
				}				
			});
	},
	warn: function(msg){
		$("#main-warning-alert .message").text(msg).parent().slideDown('fast');
		setTimeout(function(){ lodr.sql.resize(); }, 400);
	},
	grids: [],
	config: {
		mode: 'text/x-pgsql',
		lineNumbers: true,
		autofocus: true,
		lineWrapping: true
	},
	init: function(){
		lodr.sql.queries = [];
		lodr.sql.cm = CodeMirror.fromTextArea($("#script_content").get(0), lodr.sql.config);
		lodr.sql.cm.on("change", function(c,o) {
			if(lodr.sql.saveTimer) clearTimeout(lodr.sql.saveTimer);
		    lodr.sql.saveTimer = setTimeout(function(){ $(".sql form.auto-forms").submit(); }, 700);
		 });

		 lodr.sql.cm.setOption("extraKeys", {
		   'Cmd-S': function(cm) {
			   $(".sql form.auto-forms").submit();
		   },
		   'Ctrl-S': function(cm) {
			   $(".sql form.auto-forms").submit();
		   },
		   'Cmd-R': function(cm) {
			   $("#run").trigger('click');
		   },
		   'Ctrl-R': function(cm) {
			   $("#run").trigger('click');
		   }
		 });
		
		lodr.sql.resize();
		lodr.sql.getQueries();	
	},
	resize: function(){
		var $t = $(".sql .browser #database .browser-panel-body");
		if($t.length == 0) return;
		
		var w = $(window).height();
		$t.height(w-$t.offset().top -20);
		
		var $h = $("#history #queries");
		$h.height(w-$h.offset().top -20);
		
		var h = w - $("#editor").offset().top -25;
		lodr.sql.cm.setSize(null, h);
		
		$(".main").width($(window).width()-300);
		var mw = $(".main-content").width();
		var ww = $(".main").width() - 61 - mw;
		$("#history").width(ww ).find(".query").width(ww-20);
	},
	getQueries: function(){
		$("#history #queries").html('<i id="queries-wait" class="fa fa-spinner fa-pulse fa-5x text-info"></i>');
		
		var script_id = $("#history").data("script-id");
		window.setTimeout(function(){
			$.getScript('/sql/scripts/' + script_id + '/queries');
		}, 300);
	},
	_query: function(context){
		var q = Handlebars.compile($("#_query").html());
		return q(context);
	},
	setTimeAgo: function(q){
		var units = "";
		q.ago = ((new Date()) - (new Date(q.updated_at ))) / 1000;
		if(q.ago > 60 && q.ago < 3601){
			q.ago = q.ago / 60;
			units += " mins ago";
		}else if(q.ago > 3600 && q.ago < (3600*24) ){
			q.ago = q.ago / 3600;
			units += " hrs ago";
		}else if(q.ago > (3600*24) ){
			q.ago = q.ago / (3600*24) ;
			units += " days ago";
		}else{
			units += " secs ago"
		}
		q.ago = q.ago.toFixed(2) + units ;	
	},
	setDuration: function(q){
		q.dur = ((new Date(q.finished_at)) - (new Date(q.started_at || q.created_at))) / 1000;
		if(q.dur > 60 && q.dur < 3601){
			q.dur = q.dur / 60;
			q.units = "mins";
		}else if(q.dur > 3600){
			q.dur = q.dur / 3600;
			q.units = "hrs";
		}else{
			q.units = "secs"
		}
		q.dur = q.dur.toFixed(2);	
	},
	completed_query: function(q,polled){
		lodr.sql.setDuration(q);
		lodr.sql.setTimeAgo(q);	
		q.completed = true;
		
		var d = q.result_data;
		if(polled && $("#query_"+q.id).length){
			var $qry = $("#query_"+q.id).replaceWith(lodr.sql._query(q));
		}else{
			var $qry = $q.append(lodr.sql._query(q));
		}
		
		if(d.columns == undefined || d.columns.length == 0){
			$("#query_"+q.id + " .query-body")
				.hide()
				.parent()
			.find(".query-body-sql").removeClass("hidden");
		}else{
			var hdrs = d.columns.map(function(c) { return c.name ;});	
			var hot = new Handsontable($("#query_"+q.id + " .query-body").get(0), {
				  data: d.data,
				  rowHeaders: true,
				  colHeaders: hdrs,
				  dropdownMenu: ['filter_by_condition', 'filter_action_bar', 'filter_by_value'],
				  height: function(){
					  var ht = (d.data.length * 30) + 30;
					  return ht < 250 ? ht : 250;
				  },
				  stretchH: 'all',
				  manualColumnResize: true,
				  readOnly: true,
				  filters: true,
				  wordWrap: false,
				  columnSorting: true
			});
			lodr.sql.grids.push(hot);
			
			var qry_export = "#query_"+q.id + " .query-export";
			$(document).on('click', qry_export, function(e){
				e.preventDefault();
				e.stopPropagation();
				
				var exportPlugin = hot.getPlugin('exportFile');
				exportPlugin.downloadFile('csv', {
					rowHeaders: false,
					columnHeaders: true,
					filename: $('input#script_name').val() });
			});
		}
		
		lodr.sql.resize();
	},

	failed_query: function(q,polled){
		lodr.sql.setDuration(q);
		lodr.sql.setTimeAgo(q);
		
		if(polled && $("#query_"+q.id).length){
			var $qry = $("#query_"+q.id).replaceWith(lodr.sql._query(q));
		}else{
			var $qry = $q.append(lodr.sql._query(q));
		}		
		
		$("#query_"+q.id + " .query-body")
			.hide()
			.parent()
		.find(".query-body-error").removeClass("hidden");
		
		lodr.sql.resize();
	},
	running_query: function(q, polled){
		var $q = $("#queries");
		lodr.sql.setTimeAgo(q);
		
		if(polled && $("#query_"+q.id).length){
			var $qry = $("#query_"+q.id).replaceWith(lodr.sql._query(q));
		}else{
			var $qry = $(lodr.sql._query(q)).prependTo($q).slideDown();
		}
		
		$("#query_"+q.id + " .query-body")
			.hide()
			.parent()
			.find(".query-body-sql").removeClass("hidden");
			
		setTimeout(function(){
			$.getScript('/sql/scripts/'+q.script_id+'/queries/'+q.id);
		}, 1000+(Math.random()*1200));
		
		lodr.sql.resize();
	},
	request_upgrade: function(){
		if(typeof ga != "undefined"){
			ga('send','event', 'subscriptions', 'no_queries_left', '<%= current_user.email %>');
		}
		$("#history #upgrade").slideDown();
	}
}

$(document).on("dblclick",".db-table-col-name",function(e){
	var t = $(e.currentTarget).text();
	if(t == undefined || t.trim() == "") return;
	lodr.sql.editor.insertNewLine(t);
});
lodr.sql.editor = {
	insertNewLine: function(txt){
		if(lodr.sql.cm == undefined) return ;
		var doc = lodr.sql.cm.getDoc();
		var cursor = doc.getCursor();
		var ch = doc.getLine(cursor.line).length;
		
		ch = ch < 0 ? 0 : ch;
		if(ch > 0){
			txt = "\n" + txt + "\n";
		}else{
			txt = txt + "\n";
		}
		doc.replaceRange(txt, {line: cursor.line, ch: ch });
	}
}

lodr.sql.cx = {
	init: function(){
		lodr.sql.cx.$e = $("#table-context-menu");
		$(".sql-context-menu").hide();
		lodr.sql.cx.data = lodr.sql.cx.$e.data("table-def");
	},
	select: function(){
		lodr.sql.cx.init();
		var d = lodr.sql.cx.data;
		var t = 'SELECT ';
		$.each(d.columns,function(i,c){
			t = t + '\n\t' + c.name;
			if(i<d.columns.length-1) t += ','
		});
		t = t + '\nFROM\n\t' + d.name + ';';
		
		lodr.sql.editor.insertNewLine(t);		
	},
	alter: function(){
		lodr.sql.cx.init();
		var d = lodr.sql.cx.data;
		var t = 'ALTER TABLE ' + d.name + '\n{\n\tADD table_constraint |\n\tDROP CONSTRAINT constraint_name [ RESTRICT | CASCADE ] |\n\tOWNER TO new_owner |\n\tRENAME TO new_name |\n\tRENAME COLUMN column_name TO new_name |\n\tADD [ COLUMN ] column_name column_type\n\t[ DEFAULT default_expr ]\n\t[ ENCODE encoding ]\n\t[ NOT NULL | NULL ] |\n\tDROP [ COLUMN ] column_name [ RESTRICT | CASCADE ] }\n\nwhere table_constraint is:\n\n\t[ CONSTRAINT constraint_name ]\n\t{ UNIQUE ( column_name [, ... ] )  |\n\tPRIMARY KEY ( column_name [, ... ] ) |\n\tFOREIGN KEY (column_name [, ... ] )\n\tREFERENCES  reftable [ ( refcolumn ) ]}\n';
		
		lodr.sql.editor.insertNewLine(t);	
	},
	drop: function(){
		lodr.sql.cx.init();
		var d = lodr.sql.cx.data;
		var t = 'DROP TABLE [ IF EXISTS ] ' + d.name +' [, ...] [ CASCADE | RESTRICT ]';
		
		lodr.sql.editor.insertNewLine(t);		
	},
	create: function(){
		lodr.sql.cx.init();
		var d = lodr.sql.cx.data;
		if(d.table_type == "table"){
			var t = 'CREATE TABLE ' + d.name + '(';
			$.each(d.columns,function(i,c){
				t = t + '\n\t' + c.name + ' ';
			
				if(c.data_type.indexOf('varying') > -1){
					t += 'varchar(' + c.length + ') '; 
				}else if(c.data_type.indexOf('integer') > -1){
						t += 'int '; 
				}else{
					t += c.data_type;
					if(c.precision){
						t += '(' + c.precision + ', ' + c.scale + ')';
					}else if(c.length){
						t += '(' + c.length + ')';
					}				
				}
				t += ' ';
			
				if(c.is_distribution_key){
					t += 'DISTKEY '; 
				}			
			
				if(i<d.columns.length-1) t += ','
			});
			t += ' );';
		}else{
			var t = 'CREATE OR REPLACE VIEW ' + d.name + ' AS \n\t' + d.view_def + '; ';
		}
		
		
		lodr.sql.editor.insertNewLine(t);		
	},
	def: function(){
		lodr.sql.cx.init();
		var $m = $("#table-def-modal");		
		var d = lodr.sql.cx.data;
		if(lodr.sql.cx.table){
			$m.find(".modal-body-table").empty()
			lodr.sql.cx.table.destroy();
		}		
		
		$m.find(".modal-title .actual-title").text(d.name);
		$m.find(".modal-footer").html('<h6>Table Size: <span class="label label-warning">' + d.table_size + ' MB</span></h6>');
		if(d.table_type=="view"){
			$m.find(".modal-title .label")
				.text("view").removeClass("label-primary")
				.addClass("label-info");
		}else{
			$m.find(".modal-title .label")
				.text("table").removeClass("label-info")
				.addClass("label-primary");
		}
		
		var hdrs = ["Name", "Type", "Length", "Precision", "Scale", "Distribution Key", "Primary Key", "Compression"];
		var data = [];
		$.each(d.columns,function(i,c){
			data.push([c.name, c.data_type, 
						c.length, c.precision, c.scale, 
						c.is_distribution_key, c.is_primary_key, c.compression])
		});	
		var hot = new Handsontable($m.find(".modal-body-table").get(0), {
			  data: data,
			  rowHeaders: true,
			  colHeaders: hdrs,
			  dropdownMenu: ['filter_by_condition', 'filter_action_bar', 'filter_by_value'],
			  height: function(){
				  var l = data.length * 40;
				  var mx = $(window).height() *0.5;
				  var s = l > mx ? mx : l;
	   			  return s;
	   		  },
			  width: function(){
				  return $m.find(".modal-dialog").width()-10;
			  },
			  stretchH: 'all',
			  manualColumnResize: true,
			  readOnly: true,
			  filters: true,
			  wordWrap: true,
			  columnSorting: true,
			  renderAllRows: false
		});
		lodr.sql.cx.table = hot;
		$m.modal('show');
	},
	
	moveScript: function(){
		$(".sql-context-menu").hide();
		
		var s = lodr.sql.cx.scriptID;
		if(s == undefined || s == "") return;
		
		var $f = $("#moveFolderModal").find("form");
		var action = $f.attr("action").split("/");
		action[action.length-1] = s;
	
		$f.attr("action", action.join("/"));
	
		$("#moveFolderModal").modal({show: true});
	}
}
$(document).on('shown.bs.modal', '.sql #table-def-modal', function(e){
	lodr.sql.cx.table.render()
});
$(document).on("turbolinks:load", function(){
	if($(".CodeMirror").length){
		location.reload();
	}else if($(".sql .main").length){		
		$(window).on('resize', lodr.sql.resize);
		lodr.sql.init();
				
		try { $(".main-content").resizable('destroy'); } catch(err) {}
		$(".main-content").resizable({
			handles: 'e',
			minWidth: 300,
			resize: function(){
				lodr.sql.resize();		
			},
			stop: function(){
				$.each(lodr.sql.grids,function(i,g){ g.render(); });
			}
		});
	}
});

$(document).on('click', ".editable-label", function(e){
	var $e = $(e.currentTarget);
	$e.hide();	
	var $t = $($e.data("target")).removeClass("hidden").focus();
});
$(document).on('blur', ".editable-label-target",function(e){
	var $e = $(e.currentTarget);
	$e.addClass("hidden");
	var $t = $e.parent().find(".editable-label");
	var txt = $t.text();
	
	if($e.val() == undefined || $e.val().trim() == ""){
		$e.val('Untitled Script');
	}
	
	if($e.val() != txt){
		$t.text($e.val());	
		$e.parents("form").submit();
	}
	$t.show();
});
$(document).on('ajax:beforeSend',"#run", function(e, xhr,settings){
	if(lodr.sql.cm.getDoc().somethingSelected()){
		settings.url = settings.url+ '?sql=' + encodeURIComponent(lodr.sql.cm.getDoc().getSelection());
	}
});
$(document).on('ajax:beforeSend',".sql form.auto-forms", function(e, xhr,settings){
	lodr.sql.cm.save();
	$("#save-state")
		.removeClass("hidden")
		.find("i").addClass("fa-spinner fa-spin")
		.parent().find("span").text("saving changes...");
		
	$("#save").prop("disabled",true);		
	
	var $e = $(e.currentTarget).find(".editable-label-target");
	$e.addClass("hidden");
	var $t = $e.parent().find(".editable-label");
	var txt = $t.text();
	
	if($e.val() == undefined || $e.val().trim() == ""){
		$e.val('Untitled Script');
	}
	
	if($e.val() != txt){
		$t.text($e.val());			
	}
	$t.show();	
});
$(document).on('ajax:complete',".sql form.auto-forms", function(e, xhr,settings){
	try{
		var s = $(".main *[data-script-id]").data("script-id");
		var $f = $("#script_"+s+"_link");
		var ic = $f.find("i").detach();
		$f.text(" " + $("#script_name").val()).prepend(ic);
	}catch(err){}	
	
	$("#save").prop("disabled",false);
	setTimeout(function(){
		$("#save-state")
			.find("i").removeClass("fa-spinner fa-spin")
			.parent().find("span").text("All Changes Saved");
	},1000);	
});
$(document).on('ajax:error',".sql form.auto-forms", function(e, xhr,error){
	if(xhr.responseJSON){
		var body = "";
		$.each(xhr.responseJSON,function(i,msg){
			body += "<li>" + msg + "</li>";
		});
		$("#main-error-alert").removeClass("hidden")
			.slideDown()
			.append(
				'<h5 class="text-danger">Error while saving script</h5>' +
				'<ul>' + body +'</ul>'
			);
	}
});

$(document).on('click', '.sql #save', function(e){ 
	e.preventDefault();
	e.stopPropagation();
	
	$(".sql form.auto-forms").submit();
});

$(document).on('click','.sql .query-sql-toggle', function(e){
	e.preventDefault();
	e.stopPropagation();
	
	var $q = $(e.currentTarget).parents(".query");
	
	if($q.find(".query-body-sql").hasClass("hidden")){
		$q.find(".query-body-sql").removeClass("hidden")
			.parent().find(".query-body").addClass("hidden");
		
		$(e.currentTarget).html('<i class="fa fa-table"> </i>')	
	}else{
		$q.find(".query-body").removeClass("hidden")
			.parent().find(".query-body-sql").addClass("hidden");
		
		$(e.currentTarget).html("SQL");
	}
	
	$(e.currentTarget).blur();		
});

$(document).on('click', '.sql .query-expand', function(e){
	e.preventDefault();
	e.stopPropagation();
	
	var qid = $(e.currentTarget).parents(".query").data("query-id");
	
	var $m = $("#gridFullScreenModal").data("query-id",qid);
	$m.find(".modal-body").empty();
	// $m.find(".modal-body").height($(window).height()*.6);
	
	$m.modal({show: true});
});
$(document).on('shown.bs.modal', '.sql #gridFullScreenModal', function(e){
	var $m = $("#gridFullScreenModal");
	var qid =  $("#gridFullScreenModal").data("query-id");
	
	var qq;
	$.each(lodr.sql.queries,function(i,q){
		if(qid == q.id){
			qq = q;
		}
	})
	
	var d = qq.result_data;
	var hdrs = d.columns.map(function(c) { return c.name ;});	
	var hot = new Handsontable($m.find(".modal-body").get(0), {
		  data: d.data,
		  rowHeaders: true,
		  colHeaders: hdrs,
		  dropdownMenu: ['filter_by_condition', 'filter_action_bar', 'filter_by_value'],
		  height: function(){
   			  return $(window).height()*0.80;
   		  },
		  stretchH: 'all',
		  manualColumnResize: true,
		  readOnly: true,
		  filters: true,
		  wordWrap: false,
		  columnSorting: true,
		  renderAllRows: false
	});
	lodr.sql.zoomed = hot;
});
$(document).on('hidden.bs.modal', '.sql #gridFullScreenModal', function(e){
	if(lodr.sql.zoomed) lodr.sql.zoomed.destroy();
	$("#gridFullScreenModal .modal-body").empty();
});	

$(document).on('hidden.bs.collapse','#scripts .browser-panel-body',function(e){
	lodr.sql.resize();
});

$(document).on('mouseenter', '.has-browse-tools',function(e){
	$(e.currentTarget).find(".browse-tools").fadeIn('fast');
});
$(document).on('mouseleave', '.has-browse-tools',function(e){
	$(e.currentTarget).find(".browse-tools").fadeOut('fast');
});

$(document).on('click', '.btn-folder',function(e){
	var ic = $(e.currentTarget).find(".fa");
	if(ic.length == 0)
		ic = $(e.currentTarget).siblings(".fa-li");
	
	if(ic.hasClass("fa-folder-open-o")){
		ic.removeClass("fa-folder-open-o")
			.addClass("fa-folder-o");
	}else{
		ic.removeClass("fa-folder-o")
			.addClass("fa-folder-open-o");
	}
});

$(document).on('click', '#database .dropdown-menu a', function(e){
	if($("#database .dropdown-menu").is(":visible") && !$(e.currentTarget).hasClass("submenu")){
		$('#database .browser-menu').trigger('click');
	}	
});
$(document).click(function(){ $(".sql-context-menu").hide(); })
$(document).on('contextmenu', '.table-item',function(e){
	e.preventDefault();
	e.stopPropagation();
	var $e = $(e.currentTarget);
	$cx = $("#table-context-menu");
	var t = e.pageY - 52;
	var wh = $(window).height();
	
	if((wh-t) < $cx.height()){
		t = wh - $cx.height() -100;
	}
	
	$cx.data("table-def", $e.data('table-def') )
		.show().css({
			left: e.pageX, top: t
		});
});
$(document).on('contextmenu', '.script-item',function(e){
	e.preventDefault();
	e.stopPropagation();
	var $e = $(e.currentTarget);
	$cx = $("#script-context-menu");
	var t = e.pageY - 52;
	var wh = $(window).height();
	
	if((wh-t) < $cx.height()){
		t = wh - $cx.height() -100;
	}
	
	lodr.sql.cx.scriptID = $e.data("script");
	$cx.find(".script-delete-btn").attr("href", "/sql/scripts/"+$e.data("script"));
	$cx.show().css({
			left: e.pageX, top: t
		});    
	
});
$(document).on('click', '.sql-context-menu li a', function(e){ e.preventDefault(); });
$(document).on('keyup', '#database #js-db-search', function(e){
	if(lodr.sql.dbSearchTimeout){
		clearTimeout(lodr.sql.dbSearchTimeout);
	}
	
	$(".sql #database .highlight").each(function(x,xel){
		$e = $(xel);
		$e.replaceWith($e.text());
	});
	
	var t = $("#database #js-db-search").val();
	$(".sql #database .tree-node.table-item").show();
	$(".sql #database .table-column").show();
	if(t.trim() != ""){
		lodr.sql.dbSearchTimeout = setTimeout(function(){ lodr.sql.searchDB(t); }, 250);
	}
});