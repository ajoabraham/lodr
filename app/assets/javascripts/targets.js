$(document).on('click','#add-prop',function(e){
	e.preventDefault();
	e.stopPropagation();
	var index = $("#properties").find("tr").length +1;	
   var row = $('<tr class="keypair">'+
						'<td><input class="form-control" type="text" placeholder="key" name="props[key_' + index + ']" ></td>'+
						'<td><input class="form-control" type="text" placeholder="value" name="props[val_' + index + ']" ></td>' +
	'<td><a href="#" class="delete-prop btn btn-xs btn-danger"><span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> </a></td></tr>');
	
	$("#properties").append(row);
});

$(document).on('click','.delete-prop',function(e){
	e.preventDefault();
	e.stopPropagation();
    $(e.currentTarget).parents("tr").remove();
});


lodr.pollDBConnectionStatus = function(){
	var pollers = $(".testing").map(function(i,el){
		return $(el).data("target-id");
	}).get();
	
	if(pollers.length >0){
		$.getScript('databases/status?targets=' + JSON.stringify(pollers));
		setTimeout(lodr.pollDBConnectionStatus, 1500);
	}
}
lodr._target_row = function(context){
	this.__target_row = this.__target_row ? this.__target_row: Handlebars.compile($("#_database_row").html());
	return this.__target_row(context);
}

$(document).on('turbolinks:load',function(){
	lodr.pollDBConnectionStatus();
});