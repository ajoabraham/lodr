class AccountController < ApplicationController
  layout 'devise'
  
  def show
  end
  
  def new
    @buckets = current_project.s3_buckets
  end
  
  def choose_plan
    @plan = Plan.where(stripe_id: params[:id], active: true).first
    if @plan && @plan.stripe_id == current_user.subscription.plan.stripe_id
      redirect_to page_path("upgrade"), alert: "You are already in the #{@plan.name.capitalize} plan. Please choose a different plan to upgrade."
    elsif @plan.nil?
      redirect_to page_path("upgrade"), alert: "Choose a valid plan. #{params[:id]} is not valid"  
    end
     
  end
  
  def cancel
    plan = current_user.subscription.plan
    if plan.is_stripe_paid_plan?
      current_user.subscription.cancel
      redirect_to account_path, notice: "Your current plan, #{plan.name}, will be terminated at the end of the period and you will be placed back in the bootstrap plan."
    else
      redirect_to account_path, notice: "This plan cannot be cancelled from here. Plan: #{plan.name}. Please contact support@lodr.io"
    end
  end
  
  def subscribe
    @plan = Plan.where(stripe_id: params[:stripe_id], active: true).first
    
    stripe = {
      :email => params[:stripeEmail],
      :source  => params[:stripeToken]
    }
    
    current_user.subscription.send(@plan.stripe_id+"!", stripe)
    
    if current_user.subscription.errors.empty?
      flash[:event] = @plan.to_json
      redirect_to account_path(upgraded: true), notice: "Successfully subscribed to plan #{@plan.name} for #{(@plan.amount/100)} per month..."
    else
      render 'choose_plan'
    end
  rescue ActiveRecord::RecordNotFound => e
      redirect_to page_path("pricing"), alert: "Invalid plan #{params[:stripe_id]}"  
  end
  
end
