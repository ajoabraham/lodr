class Admin::ConsoleController < ApplicationController
  before_action :validate_admin_user
  layout 'admin'
  
  def index
    @users = User.all.order(last_sign_in_at: :desc)
      .paginate(:page => params[:page], :per_page => @per_page)
      .search(params[:search])
  end
  
  def load_jobs
    @load_jobs = LoadJob.all.order(updated_at: :desc)
      .paginate(:page => params[:page], :per_page => @per_page)
  end
  
  def queries
    @queries = Query.all.order(updated_at: :desc)
      .paginate(:page => params[:page], :per_page => @per_page)
  end
  
  def zips
    @zips = ZipFile.all.order(updated_at: :desc)
      .paginate(:page => params[:page], :per_page => @per_page)
  end
  
  def sources
    @sources = SourceFile.all.order(updated_at: :desc)
      .paginate(:page => params[:page], :per_page => @per_page)
      .search(params[:search])
  end
  
  def databases
    @databases = Database.all.order(updated_at: :desc)
      .paginate(:page => params[:page], :per_page => @per_page)
  end
    
  def show
    @user = User.find params[:id]
  end
  
  def cancel_long_running_jobs
    l = LoadJob.where('started_at <= ?', 4.hours.ago)
      .where(status: [:running, :queued])
      .update_all(status: :cancelled, 
      info: "job was cancelled by admins", finished_at: Time.now)
    
    redirect_to admin_console_path, notice: "#{l} long running jobs cancelled"
  end
  
  def cancel_all_jobs
    l = LoadJob.where(status: [:running, :queued]).each do |lj|
      lj.loader.cancel
    end
    
    redirect_to admin_console_path, notice: "#{l.size} jobs cancelled"
  end
  
  def cancel
    @loader = Loader.find params[:id]
    if @loader
      @loader.cancel
      redirect_to admin_console_path, notice: "#{@loader.name} job was cancelled"
    else
      redirect_to admin_console_path, alert: "#{@loader.name} was not found!"
    end
  end
  
  def cancel_unzip
    @zip = ZipFile.find params[:id]
    @zip.update_attributes(failed_reason: "Admin cancelled unzip, bad process", status: :failed)
    
    redirect_to admin_console_path, notice: "#{@zip.name} unzip was cancelled"
  end
  
  private
  
  def validate_admin_user
    unless current_user.admin?
      render :file => 'public/404.html', :status => :not_found, :layout => false
    end
  end
end
