class Api::V1::ApiController < ActionController::API
  include ActionController::HttpAuthentication::Basic::ControllerMethods
  
  before_action :authenticate, :set_pagination
  
  private
  
  def authenticate   
    authenticate_with_http_basic { |u, p| 
      @current_user = User.find_by_api_access_key(u)
      unless @current_user.nil?
        @current_project = @current_user.projects.find {|pj| pj.api_token == p }
      end
    }
    
    head :unauthorized unless @current_user && @current_project
  end
  
  def set_pagination   
    @offset = params[:offset] || 1 
    
    if params[:limit] && params[:limit].to_i < 101
      @limit = params[:limit]
    else
      @limit = 10
    end 
  end
  
end