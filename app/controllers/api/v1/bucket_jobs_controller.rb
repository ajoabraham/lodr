class Api::V1::BucketJobsController < Api::V1::ApiController
  
  def index
    if params[:status]
      @jobs = @current_project.bucket_load_histories
              .where(status: params[:status])
              .paginate(:page => @offset, :per_page => @limit)
    else
      @jobs = @current_project.bucket_load_histories
              .paginate(:page => @offset, :per_page => @limit)
    end
              
    render :json => @jobs
  end
  
  def run
    @bucket_loader = @current_project.bucket_loaders.find params[:bucket_loader_id]
        
    unless @bucket_loader.is_running?
      @bh = BucketLoadHistory.create(bucket_loader: @bucket_loader, status: :queued)    
      BucketLoadJob.perform_later @bh
      render json: @bh
    else
      render text: "Bucket Loader is already running", status: :bad_request
    end    
    
  rescue ActiveRecord::RecordNotFound  => e
      render text: "Could not find loader with id #{params[:bucket_loader_id]} for project #{@current_project.name}", status: :not_found 
  end
  
  def destroy    
    @bh = @current_project.bucket_load_histories.find params[:id]
    @bh.bucket_loader.cancel
    
    render json: @bh
  rescue ActiveRecord::RecordNotFound  => e
    render text: "Could not find a bucket load job with id #{params[:id]} for project #{@current_project.name}", status: :not_found
  end
  
  def show
    @bh = @current_project.bucket_load_histories.find params[:id]
    
    render json: @bh
  rescue ActiveRecord::RecordNotFound  => e
    render text: "Could not find bucket load job with id #{params[:id]} for project #{@current_project.name}", status: :not_found
  end
  
end
