class Api::V1::JobsController < Api::V1::ApiController
  
  def index
    if params[:status]
      @jobs = @current_project.load_jobs
              .where(status: params[:status])
              .order("greatest(started_at,updated_at) desc")
              .paginate(:page => @offset, :per_page => @limit)
    else
      @jobs = @current_project.load_jobs
              .order("greatest(started_at,updated_at) desc")
              .paginate(:page => @offset, :per_page => @limit)
    end
              
    render :json => @jobs
  end
  
  def run
    @loader = @current_project.loaders.find params[:loader_id]
    
    if @loader.is_running?
        render plain: "Loader is already running", status: :bad_request
    elsif @loader.loader_is_valid?        
        if @loader.will_exceed_quota?
          msg = %Q[This loader will process #{@loader.estimated_input_rows} rows, but the current plan does not have sufficient rows remaining.  Row allocation is based on the plan of the project owner. Please upgrade before proceeding.]
          render plain: msg, status: :upgrade_required
        else  
          @loader.update_attributes({active: true, last_ran_at: Time.now})
          @load_job = LoadJob.new(loader: @loader, user: @current_user, project: @current_project)
          @load_job.save
    
          RunLoadJob.perform_later @load_job
          render json: @load_job.as_json(except: [:job_id])
        end        
    else
        render json: @loader.errors.full_messages, status: :precondition_failed
    end
    
  rescue ActiveRecord::RecordNotFound  => e
      render plain: "Could not find loader with id #{params[:loader_id]} for project #{@current_project.name}", status: :not_found 
  end
  
  def destroy
    @lj = @current_project.load_jobs.find params[:id]
    @lj.loader.cancel unless @lj.loader.nil?
    
    render json: @lj.as_json(except: [:job_id])
  rescue ActiveRecord::RecordNotFound  => e
    render plain: "Could not find load job with id #{params[:id]} for project #{@current_project.name}", status: :not_found
  end
  
  def show
    @lj = @current_project.load_jobs.find params[:id]
    
    render json: @lj.as_json(except: [:job_id])
  rescue ActiveRecord::RecordNotFound  => e
    render plain: "Could not find load job with id #{params[:id]} for project #{@current_project.name}", status: :not_found
  end
  
end
