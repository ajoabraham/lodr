class Api::WebhooksController <  ActionController::API
  
  # Stripe subscription event life cycle
  # => customer.created
  # => customer.subsciption.created
  # => invoice.created
  # => charge.succeeded or failed
  # => invoice.payment_succeded or failed
  #
  # The process starts over once period elapses
  def stripe_event
    # retrieve event from stripe to confirm
    event = Stripe::Event.retrieve(params["id"])
    sub = Subscription.find_by_stripe_customer_id event.data.object.customer
    raise ActiveRecord::RecordNotFound, "Stripe customer #{event.data.object.customer} not found" if sub.nil?
    
    ssub = Stripe::Subscription.retrieve(sub.stripe_sub_id)
       
    sub.status = ssub.status
    sub.stripe_event = event.type
    sub.current_period_ends_at = Time.at(ssub.current_period_end)
    sub.current_period_starts_at = Time.at(ssub.current_period_start)
    
    # Reset usage when invoice_payment succeeds
    if event.type == 'invoice.payment_succeeded'
      sub.rows_processed = 0
      sub.queries_run = 0
    end
    
    # In our stripe setting we have told stripe
    # to mark sub as "unpaid" after several attempts.
    # We use the "cancel" status when the user manually
    # cancels their subscription.
    if sub.status_changed? && sub.status == "unpaid"
      sub.previous_plan_id = sub.plan.id
      sub.canceled_at = Time.now
      sub.plan = Plan.bootstrap
      
      sub.rows_processed = 0
      sub.queries_run = 0
    end
        
    sub.save
    head :no_content
  rescue ActiveRecord::RecordNotFound => e  
    logger.error("unauthorized stripe post: " + e.message)
    head :unauthorized
  rescue Stripe::InvalidRequestError => e
    logger.error("Bad Stripe Request: " + e.message)
    head :bad_request
  end
end
