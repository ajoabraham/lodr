class Api::Xo::DbRequestsController < ActionController::API
  
  def query
    @q = Query.find params[:id]
    @q.assign_attributes(query_params)
    @q.result_data = params[:execute_queries][:result_data]
    
    if @q.save
      head :ok
    else
      render json: @q.errors, status: :bad_request
    end
  end
  
  # Right now we are blindly destroying
  # existing tables and recreating them.
  # would probably be better to just update
  # the table
  def schema
    @db = Database.find params[:id]
    @db.tables.destroy_all
    
    if @db.update(schema_params)
      @db.tables.update_all(schema_status: :schema_ready)
      head :ok
    else
      render json: @db.errors, status: :bad_request
    end
  end
  
  def table
    @tb = Table.find params[:id]
    
    if params[:table].nil?
      @tb.destroy
      head :ok
    elsif @tb.update(table_params)
      head :ok
    else
      render json: @tb.errors, status: :bad_request
    end
  end
  
  
  private
  
  def schema_params
    sp = params.require(:database).permit(:schema_status, :schema_error, 
      tables_attributes: [:name, :schema, :table_type, :view_def, :table_size, 
      columns: [:name, :data_type, :compression, :precision, :scale, :length, 
        :is_distribution_key, :is_primary_key, :is_sort_key] ])
    
    sp[:schema_status].downcase! if sp[:schema_status]
    
    if sp[:tables_attributes]
      sp[:tables_attributes].each{ |t| t[:table_type].downcase! unless t[:table_type].nil? }
    end
    
    return sp
  end
  
  def table_params
    tp = params.require(:table).permit(:name, :schema, :table_type, :view_def, :table_size, :schema_status, 
                  columns: [:name, :data_type, :compression, :precision, :scale, :length, 
                  :is_distribution_key, :is_primary_key, :is_sort_key])
    
    tp[:schema_status].downcase! if tp[:schema_status]
    tp[:table_type].downcase! if tp[:table_type]
    
    return tp    
  end
  
  def query_params
    qp = params.require(:execute_queries).permit(:info,:total_rows,:fetched_rows,
      :finished_at,:last_executed_query, :status, :query_error)
      
    qp[:status].downcase! if qp[:status]
    
    return qp
  end
  
end