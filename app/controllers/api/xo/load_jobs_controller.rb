class Api::Xo::LoadJobsController < ActionController::API
    
    def update
      @load_job = LoadJob.find_by_job_id params[:id]
      was_running = @load_job.is_running?
      
      load_job_params[:status].downcase!
      
      if @load_job.cancelled?
        load_job_params[:status] = "cancelled"
      end
      
      if @load_job.update(load_job_params)
          if was_running && !@load_job.is_running? && !@load_job.cancelled? && @load_job.loader.notify
            NotificationsMailer.load_done(@load_job).deliver_later
          end
          
          if @load_job.succeeded?
            @load_job.loader.source_files.update_all(status: :loaded)
          end
          
          update_bucket_loader(@load_job) 
          head :ok
      else
        render status: :bad_request, json: @load_job.errors
      end
    end
    
    def canary
      head :no_content
    end
    
    private
    
    def load_job_params
       params.require(:load_job).permit(:info, :status, 
           :total_rows_loaded, :input_rows, :input_bytes,
           :inserted_rows, :updated_rows, :execution_errors,
           :total_failed_rows, { field_errors: [:columnName, :failedRows] }, 
           :finished_at, :lambda_done_at, :selected_file)
    end
    
    def update_bucket_loader(load_job)
      return unless @load_job.loader.bucket_loader
      
      @bh = @load_job.loader.bucket_loader.last_run
      
      return unless @bh.running?
          
      unless load_job.is_running?
        if load_job.succeeded?
          @bh.succeeded_files = @bh.succeeded_files + load_job.loader.source_files.count
        else
          @bh.failed_files = @bh.failed_files + load_job.loader.source_files.count
        end
        
        @bh.volume_processed = @bh.volume_processed + load_job.input_bytes
        @bh.rows_processed = @bh.rows_processed + load_job.input_rows
      end
      
      @bh.check_completed      
      @bh.save
      
      @bl = @bh.bucket_loader
      NotificationsMailer.bucket_load_finished(@bl).deliver_later if @bh.done? && (@bl.all_events? || @bl.load_finished?) 
      NotificationsMailer.bucket_load_failed(@bl).deliver_later if @bh.failed? && (@bl.all_events? || @bl.load_failed?)      
    end
end
