class Api::Xo::ZipFilesController < ActionController::API
    
    def update
      begin 
        @files = []
        @zip = ZipFile.find params[:id]
        
        loadable = @zip.loadable && file_extracted_to_uploads? ? nil : @zip.loadable
        
        if @zip.update(zip_params)
          files = params[:zip_file][:zip_files]        
          if @zip.succeeded? && files && files.size>0
            @zip.failed_reason = ""
          
            files.each do |f|
              name = loadable ? f[:object_key] : f[:name]
              
              @fl = SourceFile.find_or_initialize_by(name: name.strip, project: @zip.project, loadable: loadable)
              @fl.user = @zip.user
              @fl.size = f[:size]
              @fl.status = :pre_processing
              @fl.uploaded_at = Time.now
            
              @files << @fl.id
            
              if @fl.save     
                DataSampleProfileJob.perform_later @fl
              else
                begin 
                  @fl.delete_s3_object if @fl.name
                rescue => e
                  logger.error "Failed to delete bad user upload: #{@fl.name} for user id #{@zip.user.id}"
                end              
                @zip.status = :failed
                @zip.failed_reason += "failed: #{@fl.name} for #{@fl.errors.full_messages.join(', ')}"
              end            
            end # files each          
          
            @zip.save(validate: false)          
          elsif @zip.succeeded?
            # zip file was basically empty
            @zip.update(status: :failed, failed_reason: "Zip had 0 valid files")
          
          end
        
          head :no_content
        else
          head :bad_request
        end
      
      rescue => e
        @zip.update(status: :failed, failed_reason: e.message)
        render json: { error: e.message }, status: :bad_request
      ensure
        NotificationsMailer.unzip_done(@zip, @files).deliver_later
      end      
    end
    
    
    private
    
    def zip_params
      param = params.require(:zip_file).permit(:failed_reason,:status)
      param[:status].downcase!
      return param
    end
    
    def file_extracted_to_uploads?
      upload_bucket = Rails.application.secrets[:s3_upload_bucket]
      
      if params[:zip_file][:dest_bucket_name] && upload_bucket.strip == params[:zip_file][:dest_bucket_name].strip
        true
      else
        false
      end
    end
end
