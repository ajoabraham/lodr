class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!, :set_paginate_per_page
  
  # set users current_project
  # must occur after user is authenticated
  before_action :set_current_project, unless: :devise_controller?
  
  # add additional fields 
  # when users signup
  before_action :configure_permitted_parameters, if: :devise_controller?
  
  # route users to on boarding path on each 
  # index action if the user is new
  before_action :on_boarding_user_route, only: [:index], if: :on_boarding_user?
  # if the user goes to any
  # index page kill the currently 
  # editing loader
  before_action :clear_loader, only: [:index]
  
  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name,:last_name,:company])
    devise_parameter_sanitizer.permit(:account_update, keys: [:first_name,:last_name,:company])
  end
  
  def set_paginate_per_page    
    if params[:per_page] && [10,25,50,100].include?(params[:per_page].to_i)
      cookies[:per_page] = params[:per_page]
    end
     
    @per_page = cookies[:per_page] || 10
  end 
  
  def after_sign_in_path_for(resource)
    set_current_project
    
    if current_user.first_name.strip == "__missing__" || current_user.last_name.strip == "__missing__"
      current_user.first_name = ""
      current_user.last_name = ""      
      edit_user_registration_path
    elsif current_project.source_files.count == 0 && current_project.databases.count == 0
      session[:on_boarding] = true
      if request.referrer.include?("choose_plan")
        request.referrer
      else
        account_new_path
      end
    elsif current_project.source_files.count == 0
      sql_index_path 
    elsif request.referer && request.referer.include?("choose_plan")
      request.referer      
    else
      dashboard_path
    end 
  end
  
  # Add route intervention at each controllers
  # index page in order to re route user to the 
  # correct on boarding path
  def on_boarding_user_route
    if current_project.source_files.count == 0
      redirect_to account_new_path
    elsif current_project.loaders.count == 0
      redirect_to new_loader_path, notice: "Create a new loader to load files into your Redshift database.  One file corresponds to one loaded table."
    end 
  end
  
  def on_boarding_user?
    session[:on_boarding].nil? ? false : session[:on_boarding]
  end
  
  def user_boarded!
    session[:on_boarding] = false
  end 
  
  def clear_loader
    session[:loader_id] = nil
  end
  
  def set_current_project
    
    begin
      @current_project ||=
            if params[:project_id]
              current_user.projects.find params[:project_id]
            elsif cookies[:project_id]
              current_user.projects.find cookies[:project_id]
            else
              current_user.projects.first
            end
    rescue => e
      @current_project = current_user.projects.first
    end
        
    cookies[:project_id] = @current_project.id
    Time.zone = @current_project.time_zone
  end
  
  def current_project
    @current_project 
  end
  helper_method :current_project
  
end
