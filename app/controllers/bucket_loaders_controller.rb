class BucketLoadersController < ApplicationController

  def index
    if params[:loaders]
      lds = JSON.parse(params[:loaders])
      @bucket_loaders = current_project.bucket_loaders
            .where(id: lds)
            .order("greatest(created_at,updated_at) desc")
    else
      @bucket_loaders = current_project.bucket_loaders
            .order("greatest(created_at,updated_at) desc")
            .paginate(:page => params[:page], :per_page => @per_page)
            .search(params[:search])
    end
    
    respond_to do |format|
      format.js 
      format.html
      format.all {render json: @bucket_loaders}
    end
  end
  
  def new
    if params[:bucket_loader_id]
      @bucket_loader = current_project.bucket_loaders.find(params[:bucket_loader_id]).dup
    else
      shortcuts = params[:bucket_loader].nil? ? {} : bucket_loader_params
      @bucket_loader = BucketLoader.new(shortcuts)
    end    
  end
  
  def create
    @bucket_loader = BucketLoader.new(bucket_loader_params)
    @bucket_loader.user = current_user
    @bucket_loader.project = current_project
    if @bucket_loader.save
      schedule_load(@bucket_loader) 
      redirect_to bucket_loaders_path, notice: "Saved bucket loader #{@bucket_loader.name}"
    else
      render 'new'
    end
  end
  
  def update
    @bucket_loader = current_project.bucket_loaders.find params[:id]
    if @bucket_loader.update(bucket_loader_params)
      schedule_load(@bucket_loader)      
      redirect_to bucket_loaders_path, notice: "Saved bucket loader #{@bucket_loader.name}"
    else
      render 'edit'
    end
  end
  
  def show
     @bucket_loader = current_project.bucket_loaders.find params[:id]
     
     @histories = @bucket_loader.bucket_load_histories
           .order("greatest(created_at,updated_at) desc")
           .paginate(:page => params[:history_page], :per_page => @per_page)
    
     @loaders = @bucket_loader.loaders
           .order("greatest(created_at,updated_at) desc")
           .paginate(:page => params[:loader_page], :per_page => @per_page)
     
     @sources = @bucket_loader.source_files
           .order("greatest(created_at,updated_at) desc")
           .paginate(:page => params[:sources_page], :per_page => @per_page)
  end
  
  def edit
    @bucket_loader = current_project.bucket_loaders.find params[:id]
  end
  
  def preview
    if params[:bucket_loader]
      @bucket_loader = BucketLoader.new bucket_loader_params
    else
      @bucket_loader = current_project.bucket_loaders.find params[:id]
    end
   
    @preview = @bucket_loader.preview
  end
  
  def run
    @bucket_loader = current_project.bucket_loaders.find params[:id]
    
    unless @bucket_loader.is_running?
      @bh = BucketLoadHistory.create(bucket_loader: @bucket_loader, status: :queued)    
      BucketLoadJob.perform_later @bh
    end
    
    redirect_back fallback_location: bucket_loaders_path, notice: "Bucket loader #{@bucket_loader.name} is running..."
  end
  
  def cancel
    @bucket_loader = current_project.bucket_loaders.find params[:id]
    @bucket_loader.cancel
    redirect_back fallback_location: bucket_loaders_path, notice: "Bucket loader #{@bucket_loader.name} was cancelled..."
  end
  
  def destroy
    @bucket_loader = current_project.bucket_loaders.find params[:id]
    @bucket_loader.cancel if @bucket_loader.is_running?
    @bucket_loader.destroy
    redirect_to bucket_loaders_path, notice: "Bucket loader #{@bucket_loader.name} and related loaders and sources were deleted..."
  end
  
  def destroy_multiple
    current_project.bucket_loaders.where(id: params[:bucket_loader_ids]).destroy_all
    redirect_to bucket_loaders_path, notice: "Selected bucket loaders were deleted..."
  end
  
  private
  
  def bucket_loader_params
    params.require(:bucket_loader).permit(:name,:prefix,:file_filter,
      :includes_header,:delimiter,:quote_char, :file_mapping_type,
      :load_new_files_only, :load_type, :target_table_prefix, :target_table_suffix,
      :notify_type, :notification_emails, :schedule_type, :schedule_definition,
      :s3_bucket_id, :database_id, :pre_sql_id, :post_sql_id,
      :start_date, :start_time, :time_zone, :schedule_period, :schedule_type,
      { :months_of_year => [] }, { :weeks_of_month => [] },
      { :days_of_week => [] }, :day_of_month)
  end
  
  def schedule_load(bucket_loader)
    bucket_loader.bucket_load_histories.where(status: :scheduled).destroy_all
    if bucket_loader.scheduled?      
      @bh = BucketLoadHistory.create(bucket_loader: bucket_loader, status: :scheduled)
      BucketLoadJob.set(wait_until: bucket_loader.next_occurrence).perform_later @bh  
    end    
  end
end
