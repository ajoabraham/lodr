class DashboardController < ApplicationController
  before_action :set_dash_period  
  
  def index    
    @jobs = current_project.load_jobs
      .order("coalesce(started_at,created_at) desc")
      .paginate(:page => params[:page], :per_page => @per_page)
      
    @stats = LoadJob.stats(current_project, @dash_period)
    @perf = LoadJob.performance(current_project, @dash_period)
  end
  
  def stats
    if params[:job_ids]
      jids = JSON.parse(params[:job_ids])
      @jobs = LoadJob.where(project: current_project).where(id: jids)
    else
      @jobs = LoadJob.where(project: current_project).where(status: [:queued, :running])
    end
    
    @stats = LoadJob.stats(current_project, @dash_period)
        
    respond_to do |format|
      format.js
      format.html {redirect_to dashboard_path}
      format.all {render json: {stats: @stats, jobs: @jobs}}
    end
  end
  
  private 
  
  def set_dash_period
    if params[:dash_period] && LoadJob::PERIODS.values.include?(params[:dash_period])
      cookies[:dash_period] = params[:dash_period]
    end
         
    @dash_period = cookies[:dash_period] || "last_30"   
  end
  
end
