class DatabasesController < ApplicationController
  skip_before_action :on_boarding_user_route  
  before_action :set_loader, only: [:new, :create, :update]
  layout :set_loader_layout
  
  def index
      @databases = current_project.databases
  end
  
  def new      
      @database = Database.new
  end
  
  def refresh
    @database = current_project.databases.find params[:id]
    @database.schema_fetching!
    RemoteDbRequestJob.perform_later(@database, "schema")
    
    respond_to do |format|
        format.html { redirect_to sql_index_path, notice: "Refreshing database schema..."}
        format.js { }
    end
  end
  
  def show    
    respond_to do |format|
        format.html { redirect_to edit_database_path(params[:id]) }
        format.js { @database = current_project.databases.find params[:id] }
    end
  end
  
  def edit
      @database = current_project.databases.find params[:id]
  end
  
  def create
      @database = Database.new(database_params)
      @database.user = current_user
      @database.project = current_project
      
      @database.properties = get_props_json
      if @database.save
          RemoteDbRequestJob.perform_now(@database, "test")
          RemoteDbRequestJob.perform_later(@database, "schema")
          
          if @database.failed?            
            if @database.test_error.include?("Net::ReadTimeout")
              @database.update_attributes(test_connection_status: :testing, test_error: nil)
              RemoteDbRequestJob.perform_later(@database, "test", 180)
              if @loader
                redirect_to edit_loader_path(@loader)
              else
                redirect_to databases_path
              end
            else
              flash.now[:alert] = "<h4><strong class='text-danger'>Failed to Connect</strong></h4> <p>Please ensure the connection info is correct. Perhaps you need to whitelist our IP address or configure SSL properties.</p><br/> <p>#{@database.test_error[0..300]}</p>"
              render 'new'
            end            
          else
            flash[:event] = "target created"
            if @loader
              redirect_to edit_loader_path(@loader)
            else
              redirect_to databases_path
            end
          end
          
      else
          render 'new'
      end
  end
  
  def update
      @database = current_project.databases.find params[:id]
      @database.assign_attributes(database_params)
      @database.properties = get_props_json
      if @database.save
          RemoteDbRequestJob.perform_now(@database, "test") if @database.testing?
          RemoteDbRequestJob.perform_later(@database, "schema")
          
          if @database.failed?
            if @database.test_error.include?("Net::ReadTimeout")
              @database.update_attributes(test_connection_status: :testing, test_error: nil)
              RemoteDbRequestJob.perform_later(@database, "test", 180)
              redirect_to databases_path
            else                          
              flash.now[:alert] = "<h4><strong class='text-danger'>Failed to Connect</strong></h4> <p>Please ensure the connection info is correct. Perhaps you need to whitelist our IP address or configure SSL properties.</p><br/> <p>#{@database.test_error[0..300]}</p>"
              render 'new'
            end
          else
            redirect_to databases_path
          end                    
          
      else
          render 'new'
      end
  end
  
  def destroy
     @database = current_project.databases.find params[:id]
     @database.destroy 
      
      respond_to do |format|
        format.html {redirect_to databases_path}
        format.all {render text: "#{@database.name} was deleted"}
      end
  end
  
  def status
      return redirect_to databases_path, alert: "Missing `Targets` Parameter. i.e targets=[1,2,3]" if params[:targets].nil?
    
      fil = JSON.parse(params[:targets])
      @databases = Database.where(id: fil, project_id: current_project.id)
    
      respond_to do |format|
        format.js
        format.html {redirect_to databases_path}
        format.all {render json: @databases}
      end 
  end
  
  private
  
  def database_params
     tp = params.require(:database).permit(
         :name, :user_name, :password, :db_name,
         :jdbc_url, :port, :server
     )
     
     tp.delete(:password) if tp[:password].blank?
     return tp
  end
  
  def get_props_json
      return nil if params[:props].nil?
      props = {}
      params[:props].each_pair do |key,val|
          if key.starts_with? "key"
             parts = key.split("_")
             
             if parts.size == 2
                 actual_val = params[:props]["val_#{parts[1]}"]
                 props[val] = actual_val unless actual_val.nil? || actual_val.empty?
             end
          end
      end
      
      props.to_json || nil     
  end
  
  def set_loader
    if session[:loader_id]
      @loader = current_project.loaders.where(id: session[:loader_id]).first
    end
  end
  
  def set_loader_layout
    if @loader
      'loaders'
    else
      'application'
    end
  end
  
end
