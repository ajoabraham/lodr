class InvitesController < ApplicationController
  
  def new
    @invitee = User.new    
    render :layout => false
  end

  def create    
    @invitee = User.where(email: params[:user][:email]).first
    @project = current_user.projects.find params[:project_id]
    
    unless @invitee
      temp_pass = Devise.friendly_token.first(8)      
      @invitee = User.new email: params[:user][:email], password: temp_pass, password_confirmation: temp_pass,
                  first_name: "__missing__", last_name: "__missing__", company: current_project.owner.company
      @invitee.skip_confirmation!
      @invitee.save
    end
    
    if @invitee.valid?
      @project.users << @invitee
      UserMailer.invite_team_member(@invitee, current_user.id, current_project.id, temp_pass).deliver_later
    end
       
  end

  def destroy
    
    @user = User.find params[:id]
    @project = current_user.projects.find params[:project_id]
    @project.project_users.find_by_user_id(params[:id]).destroy
    
    @user.create_default_project
    
    if @user.id == current_user.id
      @current_project = current_user.projects.first
      redirect_back(fallback_location: root_path(project_id: @current_project.id), project_id: @current_project.id,
        notice: "You were removed from the project #{@project.name}")
    end
  rescue => e
    @user.errors.add(:base, e.message)    
  end
  
  
end
