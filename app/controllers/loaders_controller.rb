class LoadersController < ApplicationController
    layout 'application', only: [:index, :show]
    before_action :set_loader, only: [:sources, :settings, :transforms, :target_table, :summary, :wait, :exit, :back]
    
    def index      
      @loaders = current_project.loaders
              .where(bucket_loader_id: nil)
              .order("greatest(created_at,updated_at) desc")
              .paginate(:page => params[:page], :per_page => @per_page)
              .search(params[:search])
    end
    
    def status
      return redirect_to loaders_path, alert: "Missing `loaders` Parameter. i.e loaders=[1,2,3]" if params[:loaders].nil?
    
      fil = JSON.parse(params[:loaders])
      @loaders = Loader.where(id: fil, project_id: current_project.id)
    
      respond_to do |format|
        format.js
        format.html {redirect_to loaders_path}
        format.all {render json: @loaders}
      end
    end
    
    def new
        if params[:loader_id]
          copy_from = current_project.loaders.find params[:loader_id]
          @loader = copy_from.dup
          @loader.name = "Copy of #{copy_from.name}"
          @loader.step = :choose_inputs
          @loader.source_file_ids = copy_from.source_file_ids
          @loader.save
        else
          @loader = Loader.create name: "Untitled", 
              user: current_user, step: :choose_inputs, project: current_project
        
          if on_boarding_user? && current_project.source_files.count > 0 && current_project.loaders.count ==1
          
            @loader.source_files << current_project.source_files.last
            @loader.save
          
            user_boarded!
          end
        end
        
        session[:loader_id] = @loader.id
        next_route
    end
    
    def load_source
       @file = current_project.source_files.find params[:source_id] 
       @loader = @file.loaders.first
       
       unless @loader
           @loader = Loader.create name: "Untitled", 
               user: current_user, step: :choose_inputs, project: current_project
       else
           flash[:notice] = "This is an existing loader for this file. If you want a new one, go to the Loaders page and click new."
       end
       
       @loader.source_files << @file unless @loader.source_file_ids.include?(@file.id)
       
       @loader.save
       session[:loader_id] = @loader.id
       
       next_route
    end
    
    def edit
        @loader = current_project.loaders.find params[:id]
        @loader.update_attribute(:active,false)
        session[:loader_id] = @loader.id
        
        next_route
    end
    
    def show
       @loader = current_project.loaders.find params[:id]
       @loader.loader_is_valid?
       
       @jobs = @loader.load_jobs
         .order("coalesce(started_at,created_at) desc")
         .paginate(:page => params[:page], :per_page => @per_page)
       
       @sources = @loader.source_files
                  .order(updated_at: :desc)
                  .paginate(:page => params[:sources_page], :per_page => @per_page)
    end
    
    def destroy
       @loader = current_project.loaders.find params[:id]
       @loader.destroy 
        
        respond_to do |format|
          format.html {redirect_to loaders_path}
          format.all {render text: "#{@loader.name} was deleted"}
        end
    end
    
    def destroy_multiple
        current_project.loaders.where(id: params[:loader_ids]).destroy_all
     
        redirect_to loaders_path 
    end
    
    def run_multiple
        notice = "Execution summary: <br> <ul>"
        
        @loaders = current_project.loaders.find params[:loader_ids]
        rows = 0
        @loaders.each do |l|
          if l.estimated_input_rows > (current_project.owner.subscription.rows_remaining - rows) && !Rails.application.secrets[:on_prem]
            notice += "<li>loader #{l.name} was NOT started due to insufficient rows in current plan. Row allocation is based on the plan of the project owner. Please <a href='upgrade'>upgrade.</a></li>"
          elsif l.is_runnable?
            @load_job = LoadJob.new(loader: l, user: current_user, project: current_project)
            @load_job.save       
            RunLoadJob.perform_later @load_job
            
            rows += l.estimated_input_rows
            notice += "<li>successfully started loader #{@load_job.loader_name}. </li>"
          else
            notice += "<li>loader #{l.name} was NOT started. </li>"
          end
        end
        
        notice += "</ul>"
        flash[:warning]= notice
        redirect_to loaders_path
    end
    
    def run
       @loader = current_project.loaders.find params[:id]
       if @loader.is_running?
         if request.referrer && request.referrer.include?("loaders")
           redirect_back fallback_location: loaders_path, alert: "Loader is already running..."
         else
           redirect_to show_loader_path(@loader), alert: "Loader is already running..."
         end
       elsif @loader.loader_is_valid?
           
           if @loader.will_exceed_quota?
             redirect_to loaders_path, alert: %Q[This loader will process #{@loader.estimated_input_rows} rows, but the current plan does not have sufficient rows remaining.  Row allocation is based on the plan of the project owner. Please <a href="upgrade">upgrade before proceeding</a>] 
           else  
             @loader.update_attributes({active: true, last_ran_at: Time.now})
             @load_job = LoadJob.new(loader: @loader, user: current_user, project: current_project)
             @load_job.save
       
             RunLoadJob.perform_later @load_job
             session[:loader_was_run] = true
             redirect_to loaders_path 
           end
           
       else
           render 'summary'
       end       
    end
    
    def cancel
      @loader = current_project.loaders.find params[:id]
      @loader.cancel
      
      redirect_back(fallback_location: dashboard_path)
    end
    
    #### UPDATES TO LOADER ###
    
    def update_sources
        @loader = current_project.loaders.find params[:id]
        
        if @loader.update(loader_params)
            # only need this in multi-file load
            diff = loader_params[:source_file_ids] - @loader.source_file_ids | @loader.source_file_ids - loader_params[:source_file_ids]
            diff.reject!(&:blank?)

            unless diff.empty?
              headers = @loader.headers
              new_source_headers = SourceFile.find(loader_params[:source_file_ids].last).parsed_headers
              all_original_headers_are_present = headers.all? {|h|  new_source_headers.any?{ |ns| ns==h || ns["columnName"]==h } }
              
              @loader.transformations = nil unless all_original_headers_are_present
              @loader.column_settings = @loader.source_files.first.headers
            end

            @loader.save

            next_route
        else
          @files = current_project.source_files
                                .order(updated_at: :desc)
                                .paginate(:page => params[:page], :per_page => @per_page)
                                .search(params[:search])
          render 'sources'
        end  
    end
    
    def update_settings
        @loader = current_project.loaders.find params[:id]        
        @loader.update(loader_params)
        
        if params[:source_file]
            @loader.source_files.each do |sf|
                sf.assign_attributes(source_params)
                if sf.changed?
                   sf.status = :profiling
                   sf.save
                   DataSampleProfileJob.perform_later sf 
                end 
            end           
        end
        
        next_route
    end
    
    def update_transform
        @loader = current_project.loaders.find params[:id]
                
        if @loader.update(loader_params)
            next_route 
        else
            render 'transforms'
        end        
    end
    
    def update_target
        @loader = current_project.loaders.find params[:id]
        
        if @loader.update(target_params)
            next_route 
        else
            render 'target_table'
        end 
    end
    
    def update_schedule
        @loader = current_project.loaders.find params[:id]
        @tr = JSON.parse(@loader.transformations || "[]")
        @cols = JSON.parse(@loader.column_settings || "[]")
        
        @cols.each do |c|
            if c["dataType"] == "VARCHAR"
                c["dataType"] = "text"
            elsif c["dataType"] == "TIMESTAMP"
                c["dataType"] = "date time"
            end
        end
        
        @loader.assign_attributes(schedule_params)
        @loader.schedule_update
       
        if @loader.save && @loader.loader_is_valid? && @loader.schedule_valid?
          
          if @loader.scheduled?    
            @loader.update_attributes({active: true})          
            LoadJob.where(loader: @loader, status: :scheduled).destroy_all
          
            @load_job = LoadJob.new(loader: @loader, user: current_user, status: :scheduled, project: current_project)
            @load_job.save
    
            RunLoadJob.set(wait_until: @loader.next_occurrence).perform_later @load_job
            redirect_to loader_summary_path, notice: "Successfully scheduled #{@loader.name} to run on #{@loader.next_occurrence}"
          else
            LoadJob.where(loader: @loader, status: :scheduled).destroy_all
            redirect_to loader_summary_path, notice: "This loader is no longer scheduled"
          end          
            
        else
          Time.zone = schedule_params[:time_zone]
          start = Time.zone.parse("#{schedule_params[:start_date]} #{schedule_params[:start_time]}")
          
          @loader.start_time = start
          @loader.start_date = start
          
          if @loader.schedule_none?
            LoadJob.where(loader: @loader, status: :scheduled).destroy_all
            redirect_to loader_summary_path, notice: "This loader is no longer scheduled"
          else
            flash.now[:alert] = "Was not able to schedule job. Please edit your schedule settings."
            render 'summary'
          end
           
        end        
              
    end
    
    def exit
      @loader.update(loader_params)
      
      if @loader.loader_is_valid? && @loader.active == false
        @loader.update_attribute(:active,true)
      end
      
      session[:loader_id] = nil
      redirect_to loaders_path, notice: "Succesfully saved loader #{@loader.name} ..."
    end
    
    def exit_wizzard
      set_loader if session[:loader_id]
      if @loader && @loader.active == false && @loader.loader_is_valid?
        @loader.update_attribute(:active,true)
      end
      exit_name = @loader ? @loader.name : "" 
      
      session[:loader_id] = nil
      redirect_to loaders_path, notice: "Exited loader #{exit_name} ..."
    end
    
    # Redirect here when the files are not 
    # ready. Usually happens after user
    # updates files settings
    def wait
    end
    
    ### WIZARD ACTIONS ###
    
    def sources
        @loader.update_attribute(:step, :choose_inputs)
        @files = current_project.source_files
                  .order(updated_at: :desc)
                  .paginate(:page => params[:page], :per_page => @per_page)
                  .search(params[:search])
                  .to_a
        
        unless params[:search]
          @loader.source_files.each do |s|
            @files.unshift(s) unless @files.include?(s) 
          end 
        end
    end

    def settings
        @loader.update(step: :loader_settings)
        @files = @loader.source_files.select {|f| f.ready? || f.loaded? }
        
        @file = @files.first
          
        if @files.empty?
          waiting = @loader.source_files.select {|f| f.pre_processing? || f.profiling? }
          if waiting.size > 0
            redirect_to loader_wait_path
          else
            redirect_to loader_sources_path, alert: "Please choose a valid source file."
          end
        end        
            
    end

    def transforms
        @loader.update(step: :transformations)
        @file = @loader.source_files.where(status: [:ready,:loaded]).first
        unless @file
            redirect_to loader_wait_path
        end
    end
    
    def target_table
        @loader.update(step: :target_table)
    end
        
    def summary 
        @loader.update(step: :summary)
        @tr = JSON.parse(@loader.transformations || "[]")
        @cols = JSON.parse(@loader.column_settings || "[]")
        
        @cols.each do |c|
            if c["dataType"] == "VARCHAR"
                c["dataType"] = "text"
            elsif c["dataType"] == "TIMESTAMP"
                c["dataType"] = "date time"
            end
        end
    end
    
    def back
      if request.referrer && request.referrer.include?("loaders")
        if @loader.nil?
            redirect_to loaders_path
        elsif @loader.choose_inputs?
            redirect_to loader_sources_path
        elsif @loader.loader_settings?
            redirect_to loader_sources_path
        elsif @loader.transformations?
            redirect_to loader_settings_path            
        elsif @loader.target_table?
          redirect_to loader_transforms_path            
        else
          redirect_to loader_target_table_path
        end
      else
        next_route
      end
    end
    
    private
        
    def set_loader
        if session[:loader_id].nil? && params[:id].nil?
            redirect_to loaders_path, alert: "Create a new loader or edit an existing one."
        else
            @loader = current_project.loaders.find(params[:id] || session[:loader_id])
            session[:loader_id] = @loader.id
        end        
    end
    
    def loader_params
      # taking out multi source adder
      params.require(:loader).permit({ :source_file_ids => [] } , :name, :step,
           :column_settings, :transformations, :database_id,
            :target_table_name, :update_mode, { merge_keys: []},
            {:primary_keys => []}, {:sort_keys => []}, :distribution_style,
            :distribution_key, :pre_sql, :post_sql, :use_file_prefix, :file_prefix, :notify)
    end
    
    def target_params
        params.require(:loader).permit(:step, :database_id, 
            :target_table_name, :update_mode, { merge_keys: []},
            {:primary_keys => []}, {:sort_keys => []}, :distribution_style,
            :distribution_key, :pre_sql, :post_sql)
    end
    
    def source_params
       params.require(:source_file).permit(:sample_size, :encoding, 
           :delimiter, :quote_char, :includes_header) 
    end
    
    def schedule_params
      params.require(:loader).permit(:start_date, :start_time, :time_zone,
            :schedule_period, :schedule_type,
            { :months_of_year => [] }, { :weeks_of_month => [] },
            { :days_of_week => [] }, :day_of_month)
    end
        
    def next_route
       if @loader.nil?
           redirect_to loaders_path
       elsif @loader.choose_inputs?
           redirect_to loader_sources_path
       elsif @loader.loader_settings?
           redirect_to loader_settings_path
       elsif @loader.transformations?
           redirect_to loader_transforms_path
       elsif @loader.target_table?
           redirect_to loader_target_table_path
       else
           redirect_to loader_summary_path
       end 
    end

end
