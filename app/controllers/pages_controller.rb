class PagesController < ActionController::Base
  protect_from_forgery with: :exception
  include HighVoltage::StaticPage
  
  before_action :route_to_user_home
  layout :set_layout
  
  def route_to_user_home
    if current_user && request.path == "/"
      redirect_to dashboard_path
    end
  end
  
  def current_project
    if current_user.nil?
      nil
    else
      @current_project ||=
            if params[:project_id]
              current_user.projects.find params[:project_id]
            elsif cookies[:project_id]
              current_user.projects.find cookies[:project_id]
            else
              current_user.projects.first
            end
    end
  end
  helper_method :current_project
  
  private 
  
  def page_exists?
    HighVoltage::Constraints::RootRoute.matches?(request) || request.path == "/"
  end
  
  def set_layout
    if params[:id] && params[:id].include?("articles")
      "articles"
    else
      "pages"
    end    
  end
end
