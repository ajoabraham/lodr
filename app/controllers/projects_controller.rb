class ProjectsController < ApplicationController
  
  def show
    @current_project = current_user.projects.find params[:id]
    set_project_and_refresh
  end
  
  def create
    @current_project = Project.create(name: params[:project][:name], owner: current_user)
    
    Time.zone = @current_project.time_zone
    set_project_and_refresh
  end
  
  def edit
    @current_project = current_user.projects.find params[:id]
    render :layout => false
  end
  
  def update
    @current_project = current_user.projects.find params[:id]
    set_project_cookie
    
    if @current_project.update(project_params)
      Time.zone = @current_project.time_zone
      respond_to do |format|
        format.js {}
        format.html { redirect_back fallback_location: root_path, notice: "Updated project..."  }
        format.all {render json: @current_project.errors }
      end
    else
      respond_to do |format|
        format.js
        format.html { redirect_back fallback_location: root_path, alert: "Failed: #{@current_project.errors.full_messags.join('\n') } "  }
        format.all {render json: @current_project.errors }
      end
    end
    
  end
  
  def destroy
    @current_project = current_user.projects.find params[:id]
    pj_name = @current_project.name
    if @current_project.can_destroy_project?(current_user)
      @current_project.destroy
      @current_project = current_user.projects(order: :updated_at).first
      
      redirect_back(fallback_location: root_path, notice: "Successfully deleted project #{pj_name}")
    else
      redirect_back(fallback_location: root_path, alert: "You can't delete this project if this is your only project or if you don't own the project.")
    end    
  end
  
  private
  
  def set_project_and_refresh
    set_project_cookie
    msg = "Switched project to #{@current_project.name}..."
    
    if request.referrer.include?("/sql")
      redirect_to sql_index_path, notice: msg
    else 
      redirect_to loaders_path, notice: msg
    end
  end
  
  def set_project_cookie
    cookies[:project_id] = @current_project.id 
  end
  
  def project_params
    params.require(:project).permit(:owner_id, :name, :time_zone)
  end
  
end
