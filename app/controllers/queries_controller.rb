class QueriesController < ApplicationController
  
  def show
    @query = current_project.scripts.find(params[:script_id]).queries.find(params[:id])
    respond_to do |format|
      format.js {  }
      format.all{ render json: @query, status: :ok}
    end
  end
    
  def index 
    @script = current_project.scripts.find(params[:script_id])
    @queries = @script.queries.order(updated_at: :desc)
    
    respond_to do |format|
      format.js
      format.all{ render json: @queries, status: :ok}
    end
  end
  
  def new
    @can_query = true
    qr = current_project.owner.subscription.queries_remaining
    qr = 1 if Rails.application.secrets[:on_prem] 
    if qr == 0
      @can_query = false
      txt = "You have no queries remaining in your current plan. Please upgrade." 
      
      respond_to do |format|
        format.html { redirect_to sql_path(@script), alert: txt }
        format.js
        format.all { render json: txt, status: :unprocessable_entity }
      end
    else
      
      @script = current_project.scripts.find params[:script_id]
      @query = Query.new(script: @script, user: current_user)
    
      if params[:sql]
        @query.sql = params[:sql]
      else
        @query.sql = @script.content
      end
        
      respond_to do |format|
        if @query.save
          RemoteDbRequestJob.perform_later(@query, 'query')
        
          format.html { redirect_to sql_path(@script), notice: 'Your query is now executing' }
          format.js
          format.all { render json: @query, status: :created }
        else
          format.all { render json: @query.errors.full_messages, status: :unprocessable_entity }
        end
      end
      
    end     
  end
  
  def destroy
    q = current_project.queries.find(params[:id])
    if q.running?
      RemoteDbRequestJob.perform_later(q.id, 'cancel_query')
    end    
    q.delete
    
    respond_to do |format|
      format.js
      format.all{ render text: "successfully delete query", status: :ok}
    end
  end
  
  def delete_all
    qrs = current_project.scripts.find(params[:script_id]).queries
    qrs.each do |q|
      if q.running?
        RemoteDbRequestJob.perform_later(q.id, 'cancel_query')
      end
    end
    
    qrs.delete_all
    respond_to do |format|
      format.js
      format.all{ render text: "successfully deleted all queries", status: :ok}
    end
  end
  
end
