class RegistrationsController < Devise::RegistrationsController

  protected
    
    def after_sign_up_path_for(resource)
      session[:tmp_new_user] = true
      super
    end
        
    def after_update_path_for(resource)
      account_path
    end
    
    def update_resource(resource, params)
      resource.update_without_password(params)
    end
end