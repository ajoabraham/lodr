class S3BucketsController < ApplicationController
    # index page is okay even for on boarding users
    # here.  user will have to choose existing or add new
    skip_before_action :on_boarding_user_route
    
    def index
        @buckets = current_project.s3_buckets
        redirect_to new_s3_bucket_path if @buckets.empty?
    end
    
    def show
       @bucket = current_project.s3_buckets.find params[:id]
       
       if @bucket.valid?
         @bucket.list_objects(bucket_settings)
         @current_keys = @bucket.source_files.where(bucket_loader_id: nil).pluck(:name) 
       else
         render 'edit'
       end
    end
    
    def new
       @bucket = S3Bucket.new
    end
    
    def create
        @bucket = S3Bucket.find_or_initialize_by(project: current_project, bucket_name: bucket_params[:bucket_name])
        @bucket.user = current_user
        if @bucket.update(bucket_params)
           redirect_to s3_bucket_path(@bucket) 
        else
           render 'new'
        end
    end
    
    def update
        @bucket = current_project.s3_buckets.find params[:id]
        if @bucket.update(bucket_params)
           redirect_to s3_bucket_path(@bucket) 
        else
           render 'edit'
        end
    end
        
    def edit
       @bucket = current_project.s3_buckets.find params[:id]
    end
    
    def destroy
      @bucket = current_project.s3_buckets.find params[:id]
      @bucket.destroy
      
      redirect_to s3_buckets_path, notice: "Your S3 bucket #{@bucket.bucket_name} was deleted along with source file references."
    end
    
    def load
        if add_s3_file      
          # DataSampleProfileJob.perform_later @fl
                
          redirect_to loader_load_source_path(@fl), notice: "Configure loader for file #{@fl.clean_name}..."
        else
          @current_keys = @bucket.source_files.pluck(:name) 
          render 'show', alert: @fl.errors.full_messages
        end 
    end
    
    def add
        if add_s3_file
          if @is_zip
            flash[:notice] = "Zip file #{@z.name} was added..." if @z.unzipping?
            redirect_to sources_path
          else
            # DataSampleProfileJob.perform_later @fl
            redirect_to sources_path, notice: "File #{@fl.clean_name} from bucket #{@bucket.bucket_name} was added successfully..."
          end      
          
        else
          @bucket.list_objects(bucket_settings)
          @bucket.errors.add(:base, @fl.errors.full_messages.join(" "))
          @current_keys = @bucket.source_files.where(bucket_loader_id: nil).pluck(:name) 
          render 'show'
        end 
    end
    
    def add_multiple_files
      @bucket = current_project.s3_buckets.find params[:id]
      
      params[:s3_bucket_keys].each do |key|
        add_s3_file(key)
        unless ZipFile.is_zip?(key)
          # DataSampleProfileJob.perform_later @fl
        end
      end
      
      redirect_to sources_path
    end
    
    def search
      @bucket = current_project.s3_buckets.find params[:id]
      @bucket.list_objects(bucket_settings)
      @current_keys = @bucket.source_files.pluck(:name) 
      
      render 'show'
    end
    
    private
    
    def add_s3_file(key = nil)
      key = params[:key] if key.nil?
      @bucket = current_project.s3_buckets.find params[:id]
      
      if ZipFile.is_zip?(key)
        @z = ZipFile.new(name: key, user: current_user, 
                size: params[:size], loadable: @bucket, project: current_project)
            
        if @z.save
          @z.request_unzip
        else      
          flash[:alert] = @z.errors.full_messages
        end
        
        @is_zip = true
      else     
        @fl = SourceFile.find_or_initialize_by(name: key, project: current_project, loadable_id: @bucket.id)
        @fl.loadable = @bucket
        @fl.user = current_user
        
        @fl.status = :pre_processing
        @fl.uploaded_at = Time.now
        @fl.auto_set_header_inclusion if @fl.new_record?
    
        return @fl.save && DataSampleProfileJob.perform_later(@fl)
      end
    end
    
    def bucket_params
       tp = params.require(:s3_bucket).permit(:access_id, :access_key, :region, :bucket_name) 
       tp.delete(:access_key) if tp[:access_key].blank?
       
       return tp
    end
    
    def bucket_settings
      settings = {max_keys: 25}
      settings[:continuation_token] = params[:continuation_token] if params[:continuation_token]
      settings[:prefix] = params[:prefix] if params[:prefix]
      settings
    end
end
