class ScriptsController < ApplicationController
  
  def new
    @database = current_project.databases.order(updated_at: :desc).first
    
    if params[:duplicate_id]
      @script = current_project.scripts.find(params[:duplicate_id])
    end
      
    if @script
      @script = @script.dup
      @script.user = current_user
      @script.project = current_project
      @script.name = "Copy of #{@script.name}"
      @script.save
    else
      @script = Script.create(name: "Untitled Script", user: current_user, database: @database, project: current_project)
    end
    
    
    redirect_to sql_path(@script)
  end
  
  def update
    @script = current_project.scripts.find params[:id]
    
    if @script.update(script_params)
      if params[:script][:folder].nil?
        head :ok
      else 
        @scripts = current_project.scripts       
        respond_to do |format|
          format.js
          format.all { render json: @script, status: :ok}
        end
      end
    else
      render json: @script.errors.full_messages, status: :bad_request
    end
  end
  
  def destroy
    current_project.scripts.destroy(params[:id])
    
    redirect_to sql_index_path, notice: "successfully deleted script..."
  end
  
  private
  
  def script_params
    sp = params.require(:script).permit(:name,:content,:folder)
    sp[:folder] = nil if sp[:folder] && sp[:folder].downcase == "scripts"
    
    return sp
  end
end
