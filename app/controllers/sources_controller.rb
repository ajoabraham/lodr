class SourcesController < ApplicationController
  before_action :set_source, only: [:show, :edit, :update, :destroy]
  
  def index    
    @files = current_project.source_files
      .where(bucket_loader_id: nil)
      .order(uploaded_at: :desc, updated_at: :desc)
      .paginate(:page => params[:page], :per_page => @per_page)
      .search(params[:search])
      
      @zips = current_project.zip_files
  end
  
  def dismiss_zips
    current_project.zip_files.where(status: [:failed, :succeeded]).destroy_all
    redirect_to sources_path, notice: "Cleared out all unzip notices..."
  end
  
  def show
  end
  
  def edit
  end
  
  def update
      
      @file.assign_attributes(source_params)
      if @file.changed?
         @file.status = :profiling
         
         if @file.save
            DataSampleProfileJob.perform_later @file
            redirect_to sources_path, notice: "File settings updated successfully" 
         else
            render 'edit'
         end
      else
         redirect_to source_path(@file), notice: "No changes were detected and was not therefore applied" 
      end 
      
  end
  
  def file_status
    return redirect_to sources_path, alert: "Missing `Files` Parameter. i.e files=[1,2,3]" if params[:files].nil?
    
    fil = JSON.parse(params[:files])
    @files = SourceFile.where(id: fil, project_id: current_project.id)
    
    respond_to do |format|
      format.js
      format.html {redirect_to sources_path}
      format.all {render json: @files}
    end
  end
  
  def zips_status
    @zips = current_project.zip_files
    
    respond_to do |format|
      format.js
      format.html {redirect_to sources_path}
      format.all {render json: @zips}
    end
  end
  
  def upload
    set_s3_direct_post
  end
  
  def create
    if ZipFile.is_zip?(params[:name])
      @z = ZipFile.new(name: params[:name], user: current_user, size: params[:size], project: current_project)
            
      if @z.save
        @z.request_unzip
        render json: @z, status: :ok
      else
        begin 
          S3_USER_UPLOAD_BUCKET.delete_objects({delete: { objects: [{ key: @z.name }] }}) if @z.name
        rescue => e
          logger.warn "Failed to delete bad user upload: #{@z.name} for user id #{current_user.id}"
        end
      
        render json: @z.errors.full_messages, status: :bad_request
      end
    else
      @fl = SourceFile.find_or_initialize_by(name: params[:name], project: current_project)
      @fl.user = current_user
        
      @fl.file_type = params[:file_type]
      @fl.size = params[:size]
      @fl.status = :pre_processing
      @fl.uploaded_at = Time.now
      @fl.auto_set_header_inclusion if @fl.new_record?
    
      if @fl.save     
        DataSampleProfileJob.perform_later @fl
           
        render json: @fl, status: :ok
      else
        begin 
          S3_USER_UPLOAD_BUCKET.delete_objects({delete: { objects: [{ key: @fl.name }] }}) if @fl.name
        rescue => e
          logger.warn "Failed to delete bad user upload: #{@fl.name} for user id #{current_user.id}"
        end
      
        render json: @fl.errors.full_messages, status: :bad_request
      end
    end    
  end
  
  def destroy
    @file.destroy
    
    respond_to do |format|
      format.js
      format.html {redirect_to sources_path}
      format.all {render text: "#{@file.name} was deleted"}
    end
  end
  
  def destroy_multiple
     current_project.source_files.where(id: params[:source_file_ids]).destroy_all
     
     redirect_to sources_path 
  end
  
  private
    
    def set_source
       @file = current_project.source_files.find params[:id]
       @file.valid?
       if @file.failed?
          @file.errors.add(:base, @file.processing_errors) 
       end 
    end
    
    def set_s3_direct_post
      @s3_direct_post = S3_USER_UPLOAD_BUCKET.presigned_post(key: "#{current_user.id}/uploads/${filename}", 
        success_action_status: '201', acl: 'private')
    end
    
    def source_params
       params.require(:source_file).permit(:sample_size, :includes_header, 
           :delimiter, :quote_char, :encoding) 
    end
end
