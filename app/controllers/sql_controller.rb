class SqlController < ApplicationController
  skip_before_action :on_boarding_user_route  
  
  def index
    @scripts = current_project.scripts.order(updated_at: :desc)    
    @script = @scripts.first
    
    if @script.nil?      
      @script = Script.create(name: "Untitled Script", user: current_user, project: current_project)
    end
    
    if @script.database.nil?
      @database = current_project.databases.order(updated_at: :desc).first
      if @database.nil?
        redirect_to new_database_path, notice: "You have to create a database connection before using the SQL client"
      else
        @script.update_attribute(:database_id, @database.id)
      end
    else
      @database = @script.database
    end    
    
  end
  
  def show
    @scripts = current_project.scripts.order(updated_at: :desc) 
    @script = current_project.scripts.find(params[:id])
    @database = @script.database
    
    render 'index'
  end
  
  def update_db
    @database = current_project.databases.find params[:database_id]
    @script = current_project.scripts.find(params[:id])
    
    if @script.update_attribute(:database_id, @database.id)
      respond_to do |format|
          format.html { redirect_to sql_path(@script), notice: "Successfully switched database to #{@database.name}" }
          format.js { render 'databases/show' }
      end
    else
      redirect_to sql_path(@script), alert: "Failed to switch: #{@script.errors.full_messages.join(',') }"
    end
  end
  
end
