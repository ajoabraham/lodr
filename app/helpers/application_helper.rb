module ApplicationHelper
    
    NUM_UNITS = {thousand: "K", million: "M", billion: "B", trillion: "T"}
    
    def num_units
      NUM_UNITS
    end
    
    def will_paginate(collection,options ={})
      options.merge!({next_label: "&raquo;", previous_label:"&laquo;" })
      super(collection,options)
    end
    
    def type_to_html(type, fa_size=nil)
  		type_html = ""
        
  		if type=="INTEGER"
  			type_html='<i class="fa fa-hashtag #{fa_size}" aria-hidden="true"></i>'
  		elsif type=="DECIMAL"
  			type_html='<i class="fa text-icon" aria-hidden="true">#.#</i>'
  		elsif type=="DATE"
  			type_html = '<i class="fa fa-calendar" aria-hidden="true"></i>'
  		elsif type=="DATETIME" || type=="TIMESTAMP"
  			type_html = '<i class="fa fa-clock-o " aria-hidden="true"></i>'				
  		elsif type=="BOOLEAN"
  			type_html = '<i class="fa fa-toggle-on" aria-hidden="true"></i>'
      elsif type== "VARCHAR"
          type_html = '<i class="fa text-icon text-icon-string" aria-hidden="true">abc</i>'
      else
  	      type_html = '<i class="fa fa-question-circle-o" aria-hidden="true"></i>'
      end
		
  		type_html.html_safe 
    end
    
    def clean_user_name
      "#{current_user.first_name} #{current_user.last_name}"
    end
    
    def fa_icon(i='play',text = '', classes='')
      icon = "<i class='fa fa-#{i} #{classes}' aria-hidden='true'></i> #{text}"
      icon.html_safe
    end
    
    def fa_stack(top='square', bottom='circle-o', text= '', classes='', lg=false)
      size = lg ? 'fa-lg' : 'fa'
      icon = <<~html
                <span class="fa-stack #{size}">
                  <i class="fa fa-#{bottom} fa-stack-2x"></i>
                  <i class="fa fa-#{top} fa-stack-1x"></i>
                </span> #{text}
            html
            
      icon.html_safe
    end
    
    def en_icon(i='bucket',text = '', classes='')
      icon = "<span class='icon icon-#{i} #{classes}' aria-hidden='true'></span> #{text}"
      icon.html_safe
    end
    
    def glyph_icon(i='bucket',text = '', classes='')
      icon = "<span class='glyphicon glyphicon-#{i} #{classes}' aria-hidden='true'></span> #{text}"
      icon.html_safe
    end
    
    def formatted_time(time_val)
      if time_val && time_val.kind_of?(Time) 
        time_val.strftime("%b %d, %Y %I:%M:%S %p")
      else
        time_val
      end
    end
    
end
