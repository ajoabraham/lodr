module BucketLoadersHelper
  
  def humanize_seconds secs
    return "" if secs.nil? 
    [[60, :secs], [60, :mins], [24, :hrs], [1000, :days]].map{ |count, name|
      if secs > 0
        secs, n = secs.divmod(count)
        "#{n.to_i} #{name}"
      end
    }.compact.reverse.join(' ')
  end
  
end
