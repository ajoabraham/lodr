module LoadersHelper
  LodrObject = Struct.new(:text,:value)
  
  def array_to_collection(arry)
    col = []
    arry.each do |e|
      next if e.blank?
      
      l = LodrObject.new(e.to_s.split("_").last.titleize, e.downcase.to_sym)
      col << l      
    end
    
    col
  end

end
