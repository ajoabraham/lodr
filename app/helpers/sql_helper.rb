module SqlHelper
  
  def folderize(scripts)
    folders = {"root" => []}
    
    scripts.each do |s|
      if s.folder.blank?
        folders["root"].push s
      else
        folders[s.folder] = [] if folders[s.folder].nil?
        folders[s.folder].push s
      end
    end
    
    return folders
  end
  
end
