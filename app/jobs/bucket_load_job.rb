class BucketLoadJob < ApplicationJob
  queue_as :default
  
  after_perform do |job|
    @bucket_loader = job.arguments.first.bucket_loader
    if @bucket_loader.scheduled? && @bucket_loader.schedule_valid?
      @bucket_loader.bucket_load_histories.where(status: :scheduled).destroy_all
      @bh = BucketLoadHistory.create(bucket_loader: @bucket_loader, status: :scheduled)
      BucketLoadJob.set(wait_until: @bucket_loader.next_occurrence).perform_later @bh 
    end
  end
  
  def perform(bucket_load_job)
    @bl = bucket_load_job.bucket_loader
    
    if bucket_load_job.runnable?
      if bucket_load_job.scheduled?
        @bl.schedule_none! if @bl.schedule_once?
        bucket_load_job.update_attributes(status: :queued, created_at: Time.now)
      end
      
      @bl.run_load
    end
  rescue => e
    bucket_load_job.update_attributes(status: :failed, failure_message: e.message)
    NotificationsMailer.bucket_load_failed(@bl).deliver_later if @bl.all_events? || @bl.load_failed?
  end
end
