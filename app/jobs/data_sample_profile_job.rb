class DataSampleProfileJob < ApplicationJob
  queue_as :default

  def perform(input_data)   
    input_data.generate_settings_and_sample if input_data.pre_processing?
    
    input_data.profiling!
    input_data.profile
  end
end
