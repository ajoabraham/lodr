class RemoteDbRequestJob < ApplicationJob
    queue_as :default
    
    def test_connection(target, timeout)
        begin
           payload = target.to_ll.to_json
        
           resp = HTTParty.post(Wrangler::END_POINTS[:test_db], body: payload,
                   timeout: timeout,
                   :headers => {
                        'Content-Type' => 'application/json', 
                        'Accept' => 'application/json'
                      })
              
           body = JSON.parse resp.body
           
           if body["succeeded"]
              target.test_connection_status = :succeeded
              target.test_error = nil      
           else
               target.test_connection_status = :failed
               target.test_error = "Please whitelist our IP or fix your connecting settings like SSL. " + body["error"]
           end
         
           target.save
       rescue => e
           target.test_connection_status = :failed
           target.test_error = e.message
           target.save
       rescue Exception => e
           target.test_connection_status = :failed
           target.test_error = e.message
           target.save
       end 
    end  
      
    def perform(target, request, timeout = 20)
        if request == "test"
          test_connection(target, timeout)
        elsif request == "schema"
          schema(target, timeout)
        elsif request == "table"
          table(target, timeout) 
        elsif request == "query"
          query(target, timeout)
        elsif request == "cancel_query"
          cancel_query(target, timeout)  
        end
    end
    
    def schema(db, timeout)
        begin
           payload = {
             connInfo: db.to_ll,
             webhook: "#{Rails.application.secrets.internal_webhook_base}/api/xo/db_requests/schema/#{db.id}"  
           }
      
           resp = HTTParty.post(Wrangler::END_POINTS[:get_schema], body: payload.to_json,
                   timeout: timeout,
                   :headers => {
                        'Content-Type' => 'application/json', 
                        'Accept' => 'application/json'
                      })
            
           if resp.code == 200
              db.schema_status = :schema_fetching
              db.schema_error = nil      
           else
             db.schema_status = :schema_failed
             body = JSON.parse resp.body
             db.schema_error = body["error"] if body["error"] 
           end
       
           db.save
       rescue => e
         db.schema_status = :schema_failed
         db.schema_error = e.message
         db.save
       end
      
    end
    
    def table(tb, timeout)
        begin
           payload = {
             tableName: tb.name,
             schemaName: tb.schema,
             connInfo: tb.database.to_ll,
             webhook: "#{Rails.application.secrets.internal_webhook_base}/api/xo/db_requests/table/#{tb.id}"  
           }
      
           resp = HTTParty.post(Wrangler::END_POINTS[:get_table_schema], body: payload.to_json,
                   timeout: timeout,
                   :headers => {
                        'Content-Type' => 'application/json', 
                        'Accept' => 'application/json'
                      })
            
           if resp.code == 200
              tb.schema_status = :schema_fetching
              tb.schema_error = nil      
           else
             tb.schema_status = :schema_failed
             body = JSON.parse resp.body
             tb.schema_error = body["error"] if body["error"] 
           end
       
           tb.save
       rescue => e
         tb.schema_status = :schema_failed
         tb.schema_error = e.message
         tb.save
       end
      
    end
    
    def query(q, timeout)
        q.started_at = Time.now  
        
        begin
           payload = {
             queryId: q.id,
             sql: q.sql,
             rowLimit: 2000,
             connInfo: q.script.database.to_ll,
             webhook: "#{Rails.application.secrets.internal_webhook_base}/api/xo/db_requests/query/#{q.id}"  
           }
      
           resp = HTTParty.post(Wrangler::END_POINTS[:execute_query], body: payload.to_json,
                   timeout: timeout,
                   :headers => {
                        'Content-Type' => 'application/json', 
                        'Accept' => 'application/json'
                      })
           
           if resp.code == 200
              q.status = :running
              q.query_error = nil      
           else
             q.status = :failed
             body = JSON.parse resp.body
             q.query_error = body["error"] if body["error"] 
           end
       
           q.save
       rescue => e
         q.status = :failed
         q.query_error = e.message
         q.save
       end
      
    end
    
    def cancel_query(qid, timeout)
        begin                
          resp = HTTParty.get("#{Wrangler::END_POINTS[:cancel_query]}#{qid}", timeout: timeout )
       rescue => e
         logger.error e
       end      
    end
    
end
