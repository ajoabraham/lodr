class RunLoadJob < ApplicationJob
  queue_as :default
  
  after_perform do |job|
    lj = job.arguments.first
    @loader = lj.loader
    
    if @loader.loader_is_valid? && @loader.scheduled? && @loader.schedule_valid? && @loader.next_occurrence
                    
        @loader.update_attributes({active: true})          
        LoadJob.where(loader: @loader, status: :scheduled).destroy_all
    
        @load_job = LoadJob.new(loader: @loader, user: lj.user, status: :scheduled, project: lj.project)
        @load_job.save

        RunLoadJob.set(wait_until: @loader.next_occurrence).perform_later @load_job
    end
  end
    
  def perform(load_job)      
      if load_job.loader.will_exceed_quota?
        failed(load_job, "This load requires more processing rows than what is remaining on your current plan. Please upgrage.")
        NotificationsMailer.load_done(load_job).deliver_later
        
        return 
      end
      
      begin
        if load_job.loader.is_runnable?(load_job.id) && !load_job.cancelled?
        
           load_job.loader.update_attribute(:last_ran_at, Time.now)
         
           load_job.status = :queued
           load_job.started_at = Time.now        
           payload = load_job.to_ll.to_json
           load_job.save
         
           resp = HTTParty.post(Wrangler::END_POINTS[:run_job], body: payload,
           timeout: 25,  
           :headers => {
                'Content-Type' => 'application/json', 
                'Accept' => 'application/json'
              })
         
           if resp.code != 200
              failed(load_job, "Bad Lambda: #{resp.code} #{resp.headers.to_yaml}")
           end
          
        elsif load_job.loader.errors.any?
          msgs = load_job.loader.errors.full_messages.join("\n")
          failed(load_job, msgs)
        else
          failed(load_job, "Job was cancelled before it could be run.")
        end        
     rescue => e
         failed(load_job,e.message)
     rescue Exception => e
         failed(load_job,e.message)
     end
  end
  
  def failed(load_job,message)
      load_job.finished_at = Time.now
      load_job.execution_errors = message
      load_job.status = :failed
      load_job.save
      
      if load_job.loader.bucket_loader
        bh = load_job.loader.bucket_loader.last_run
        if bh.running?
          bh.check_completed
          bh.save
        end
      end      
  end
  
  def max_attempts
    1
  end
    
end
