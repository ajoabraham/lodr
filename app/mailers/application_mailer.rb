class ApplicationMailer < ActionMailer::Base
  default from: 'Lodr <support@lodr.io>'
  default :template_path => 'mailers'
  
  layout 'mailer'
end
