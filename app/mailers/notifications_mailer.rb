class NotificationsMailer < ApplicationMailer
  
  after_action :clean_up_zip
  
  rescue_from(ActiveRecord::RecordNotFound) do |e|
     logger.warn e.message
  end
  
  def unzip_done(zip,files)
    @zip = zip
    @files = SourceFile.where(id: files)
    @user = @zip.user
    
    subject = "Successfully unzipped #{@zip.name}"
    unless zip.succeeded?
      subject = "Problems unzipping #{@zip.name}"
    end
    
    email_with_name = %("#{@user.full_name}" <#{@user.email}>)
    mail(to: email_with_name, subject: subject)
  end
  
  def load_done(load_job)
    @load_job = load_job
    @user = load_job.user
    
    subject = "Load Job #{@load_job.status.humanize} for #{@load_job.loader_name}"
        
    email_with_name = %("#{@user.full_name}" <#{@user.email}>)
    mail(to: email_with_name, subject: subject)
  end
  
  def bucket_load_started(bucket_loader)
    @bl = bucket_loader
    subject = "Started loading #{@bl.name}..."
    
    mail(to: @bl.get_emails, subject: subject)
  end
  
  def bucket_load_finished(bucket_loader)
     @bl = bucket_loader
      
     subject = "Succeeded loading #{@bl.name}"
    
     mail(to: @bl.get_emails, subject: subject)
  end
  
  def bucket_load_failed(bucket_loader)
     @bl = bucket_loader
     subject = "FAILED loading #{@bl.name}"
    
     mail(to: @bl.get_emails, subject: subject)
  end
  
  private
  
  def clean_up_zip
    @zip.destroy if @zip && @zip.succeeded? && @zip.failed_reason.nil?
  end
end
