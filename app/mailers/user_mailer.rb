class UserMailer < ApplicationMailer
  
  def invite_team_member(user, inviter_id, project_id, temp_pass = nil)
    @user = user
    @inviter = User.find inviter_id
    @project = Project.find project_id
    @temp_pass = temp_pass
    
    subject = "You were invited to a Lodr Project #{@project.name}"
    
    mail(to: @user.email, subject: subject)
  end
  
  def reinvite_team_member(user, inviter_id, project_id, temp_pass = nil)
    @user = user
    @inviter = User.find inviter_id
    @project = Project.find project_id
    @temp_pass = temp_pass
    
    subject = "Reminder you were invited to a Lodr Project #{@project.name}"
    
    mail(to: @user.email, subject: subject)
  end
  
end
