class BucketLoadHistory < ApplicationRecord
  belongs_to :bucket_loader
  
  enum status: [:queued, :prepping_files, :running_loads, :done, :cancelled, :failed, :scheduled]
  
  def jobs_running?(excluding = nil)
    load_jobs.any?{|lj| lj.id != excluding && ( lj.queued? || lj.running? ) }
  end
  
  def running?
    ["queued", "prepping_files", "running_loads" ].include?(self.status)
  end
  
  def runnable?
    self.queued? || self.scheduled?
  end
  
  def failed_jobs
    load_jobs.select {|lj| lj.failed? }
  end
  
  def failed_percent
    return 0 if files_to_load == 0
    (failed_jobs.count * 1.00) / load_jobs.count
  end
  
  def finished_jobs
    load_jobs.select {|lj| !lj.is_running? }
  end
  
  def percent_done
    return 0 if files_to_load == 0
    (files_processed * 1.00) / files_to_load
  end
  
  def percent_succeeded
    return 0 if files_to_load == 0
    (succeeded_files * 1.00) / files_to_load
  end
  
  def files_processed
    succeeded_files + failed_files
  end
  
  def load_jobs
    @load_jobs ||= LoadJob.where(id: self.load_job_ids).to_a
  end
  
  def duration
    @duration ||= self.finished_at - self.created_at if self.finished_at
  end
  
  def prep_duration
    @prep_duration ||= (self.prep_finished_at - self.created_at) if self.prep_finished_at
  end
  
  def check_completed
    if !self.jobs_running? && self.running_loads?
      self.finished_at = Time.now
      if self.failed_percent >= 0.7
        self.status = :failed
        self.failure_message = "#{(self.failed_percent * 100).ceil}% or more load attempts failed"
      else
        self.status = :done unless self.failed? || self.cancelled?
      end
    end
  end
  
end
