class BucketLoader < ApplicationRecord
  # Schedule types
  include Scheduled
  include AuthorizeProject 
  
  belongs_to :project
  belongs_to :user
  belongs_to :database
  belongs_to :s3_bucket
  belongs_to :pre_sql, class_name: "Script"
  belongs_to :post_sql, class_name: "Script"
  has_many :loaders, dependent: :destroy
  has_many :source_files, dependent: :destroy
  has_many :load_jobs, through: :loaders 
  has_many :bucket_load_histories, dependent: :destroy
  
  enum file_mapping_type: [:whole_object, :root_level, :level_2, :level_3, :level_4, :level_5]
  FILE_MAPPING_TYPES = [
    ["Each file maps to a single table", :whole_object],
    ["Each root level maps to a single table", :root_level],
    ["Up to the second level of a path maps to a single table", :level_2],
    ["Up to the third level of a path maps to a single table", :level_3],
    ["Up to the fourth level of a path maps to a single table", :level_4],
    ["Up to the fifth level of a path maps to a single table", :level_5]
  ]
  
  enum load_type: [:append, :replace]
  enum notify_type: [:all_events, :load_failed, :load_finished, :no_notifications]
  
  validates :name, :user, :project, :database, :load_type, :s3_bucket, :file_mapping_type, presence: true
  validate :schedule_valid?, :authorized_project?
  
  before_save :schedule_update, :reset_sources_and_loaders
  
  def self.search(search)
    unless search.blank?
      where('lower(name) LIKE ?', "%#{search.downcase}%")
    else
      order(created_at: :desc)
    end
  end
  
  def cancel
    if self.is_running?
      self.loaders.each {|lo| lo.cancel }
    end
    self.bucket_load_histories.where(status: [:queued, :prepping_files, :running_loads]).update_all(status: :cancelled) 
  end
   
  def is_running?
    @running_count ||= self.load_jobs.where(status: [:running,:queued]).count
    return @running_count > 0
  end
  
  def running_count
    is_running? if @running_count.nil?
    @running_count    
  end
  
  def last_run
    @last_run ||= self.bucket_load_histories.order(created_at: :desc).where.not(status: :scheduled).limit(1).first
    @last_run
  end
    
  def runnable?
    self.valid?
    
    if self.last_run.nil? || self.last_run.cancelled? || 
           self.last_run.done? || self.last_run.failed?
      
        self.errors.add(:base, "Job was not initialized properly with a bucket loader history.")           
    end
    
    if self.errors.empty? && !self.s3_bucket.valid?
      self.errors.add(:s3_bucket, self.s3_bucket.errors.full_messages.join("\n"))
    end
        
    self.errors.empty?
  end  
    
  def mode_to_target_table(s3_key)
    skey_clean = s3_key.sub(/^\/*/,"")
    
    nm =      case file_mapping_type.to_sym
              when :whole_object
                clean_table_name(skey_clean)
              when :root_level
                clean_table_name(skey_clean.split("/").first)
              when :level_2
                s = skey_clean.split("/")
                pos = s.length >=2 ? 1 : s.length - 1
                clean_table_name(s[0..pos].join("/"))
              when :level_3
                s = skey_clean.split("/")
                pos = s.length >=3 ? 2 : s.length - 1
                clean_table_name(s[0..pos].join("/"))
              when :level_4
                s = skey_clean.split("/")
                pos = s.length >=4 ? 3 : s.length - 1
                clean_table_name(s[0..pos].join("/"))
              when :level_5
                s = skey_clean.split("/")
                pos = s.length >=5 ? 4 : s.length - 1
                clean_table_name(s[0..pos].join("/"))
              end
    
    "#{target_table_prefix}#{nm}#{target_table_suffix}"
  end
  
  def get_emails
    return notification_emails unless notification_emails.strip.blank?
    self.user.email
  end
  
  def run_load
    raise ArgumentError, "Bucket loader is not runnable: \n #{self.errors.full_messages.join('\n')}" unless self.runnable?
    
    self.last_run.prepping_files!
    loaders_to_run = []
    
    # first delete all source file
    # associattion from the loaders
    # This will allows us to just add
    # the new new ones without duplication
    self.loaders.each { |lo| lo.source_files.destroy_all }
    
    loop do
      break unless self.last_run.running?
      
      if self.s3_bucket.response && self.s3_bucket.response.is_truncated
        self.s3_bucket.list_objects(continuation_token: self.s3_bucket.response.next_continuation_token, prefix: self.prefix)
      elsif self.s3_bucket.response.nil?
        self.s3_bucket.list_objects(prefix: self.prefix)
      end    
      
      self.s3_bucket.contents.each do |s3file|
        break unless self.last_run.running?
        
        next if self.file_filter && !s3file["key"].include?(self.file_filter)
        s = self.source_files.where(name: s3file["key"]).first
        
        # The file exists but we are only loading new ones
        next if load_new_files_only && s && s.loaded?
        s = create_source_file(s3file) unless s
        
        next unless s      
      
        target_table = mode_to_target_table(s3file["key"])
        lo = self.loaders.where(name: target_table).first
        lo ||= create_loader(target_table)
      
        lo.source_files << s        
        self.last_run.files_to_load = self.last_run.files_to_load + 1
        
        lo.save(validate: false)
        loaders_to_run << lo unless loaders_to_run.include?(lo)
      end
      
      break unless self.s3_bucket.response.is_truncated
      
    end
    
    self.last_run.update_attributes(status: :running_loads, prep_finished_at: Time.now)
    
    # no find any loaders without column_settings
    # profile the data and set the column_settings
    time_increment = 5
    loaders_to_run.each_with_index do |lo, i|
      if lo.column_settings.nil? || lo.column_settings.empty?
        sf = lo.source_files.first
        sf.profile
        
        lo.update_attribute(:column_settings, sf.headers)
      end
      
      if lo.is_runnable?
                
        lo.update_attributes({active: true, last_ran_at: Time.now})
        lj = LoadJob.new(loader: lo, user: self.user, project: self.project)
        lj.save
        
        RunLoadJob.set(wait: time_increment.seconds, priority: 20).perform_later lj
        
        self.last_run.load_job_ids << lj.id
        time_increment += 15
      else
        # can't run it due to validation or
        # other issues. So we mark all files as failed
        bh = self.last_run
        bh.failed_files = bh.failed_files + lo.source_files.count
      end
    end
        
    if loaders_to_run.size == 0
      self.last_run.status = :done
      self.last_run.failure_message = "There were no files available to be loaded."
      self.last_run.finished_at = Time.now
      self.last_run.save
      NotificationsMailer.bucket_load_finished(self).deliver_later if self.all_events? 
    else
      self.last_run.save    
      NotificationsMailer.bucket_load_started(self).deliver_later if self.all_events?
    end    
    
  end
  
  def preview
    unless self.s3_bucket.valid?
      self.errors.add(:s3_bucket, self.s3_bucket.errors.full_messages.join("\n\t"))
      return false
    end
    
    preview_hash = {}
    self.s3_bucket.clear
    file_count = 0
    
    loop do            
      if self.s3_bucket.response && self.s3_bucket.response.is_truncated
        self.s3_bucket.list_objects(continuation_token: self.s3_bucket.response.next_continuation_token, prefix: self.prefix)
      elsif self.s3_bucket.response.nil?
        self.s3_bucket.list_objects(prefix: self.prefix)
      end    
      
      self.s3_bucket.contents.each do |s3file|
        
        next if self.file_filter && !s3file["key"].include?(self.file_filter)
        target_table = mode_to_target_table(s3file["key"])      
        preview_hash[target_table] ||= []
        preview_hash[target_table] << s3file["key"]
        file_count += 1
        
        break if preview_hash.count >= 10 || file_count >= 10
      end
      
      break if !self.s3_bucket.response.is_truncated || preview_hash.count > 10 || file_count >= 10  
    end
    
    preview_hash
  end
  
  private
  
  def clean_table_name(input)
    res = input.split(".").first.gsub(/[^[:alnum:]]+/,"_").gsub(/_$/,"").gsub(/^(\d+|_+)/,"l_")
    
    res[0..126]
  end
  
  def create_loader(target_table)
    
    lo = Loader.new step: :ready, name: target_table, user: self.user, project: self.project,
              target_table_name: target_table, update_mode: "mode_#{self.load_type}".to_sym,
              database: self.database, notify: false, active: true, bucket_loader: self 
    
    lo.save validate: false
    
    return lo  
  end
  
  def create_source_file(source)
   
    s = SourceFile.new status: :pre_processing, name: source["key"], 
      size: source["size"], uploaded_at: Time.now, loadable: self.s3_bucket,
      bucket_loader: self, user: self.user, project: self.project,
      includes_header: includes_header
    
    s.save
    s.generate_settings_and_sample    
    
    s.profiling!
    
    # Job manager only respects these settings
    # during a profile call
    s.quote_char = self.quote_char.strip == 'AUTO' ? nil : self.quote_char
    s.delimiter = self.delimiter.strip == 'AUTO' ? nil : self.delimiter
    s.save
    
    s.profile
    
    return s
  end
  
  def reset_sources_and_loaders
    if self.quote_char_changed? || self.delimiter_changed?    
      self.loaders.destroy_all
      self.source_files.destroy_all
    end
  end
    
end
