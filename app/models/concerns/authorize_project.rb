
module AuthorizeProject
  extend ActiveSupport::Concern
    
  def authorized_project?
    raise "Must belong to a project in order for project authorization" unless self.respond_to?(:project_id)
    p_id = self.project_id
    self.class.reflect_on_all_associations(:belongs_to).each do |bel_assoc|
      parent = self.send bel_assoc.name
      if parent.respond_to? :project_id
        unless parent.send(:project_id) == p_id
          errors.add(bel_assoc.name, "unauthorized, must be in the same project")
        end
      end
    end
    
    self.errors.any?
  end
  
  
end