# Adds field encryption based on attr_encrypted
#
#
module EncryptedFields
  extend ActiveSupport::Concern
  
  included do
    belongs_to :dek
  end
  
  def encryption_key
    self.dek ||= Dek.primary
    dek.key
  end
    
end