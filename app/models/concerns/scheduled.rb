# Adds scheduling defintion based on IceCub gem
# to the model.  The model should have schedule_type integer
# field and a schedule text field.
#
# ==== ATTRIBUTES
#
# +weeks_of_month+: is one of :first, :last, :second, :fourth
# +schedule_period+: is the frequency of the schedule.
# For example every 3 days schedule will have a schedule_period
# value of 3.
module Scheduled
  extend ActiveSupport::Concern
  include IceCube
  
  WEEKS_OF_MONTH = {
    first: 1, second: 2, third: 3, fourth: 4, last: -1
  }
  
  attr_accessor :start_date, :start_time, :time_zone, :schedule_period,
                :days_of_week, :months_of_year, :day_of_month, :weeks_of_month
  
  included do
    serialize :schedule, IceCube::Schedule
        
    enum schedule_type: [ :schedule_none, :schedule_once, :schedule_hourly, 
                          :schedule_daily, :schedule_weekly, :schedule_monthly, 
                          :schedule_yearly ]
    
    after_initialize :set_defaults
    after_find :schedule_to_model
  end
    
  def schedule_valid? 
    return if self.schedule_none?
       
    [:start_date, :start_time, :time_zone].each do |m|
      errors.add(m, " - is missing") if self.send(m) == nil
    end
    
    unless ActiveSupport::TimeZone::MAPPING.keys.include?(self.time_zone)
      errors.add(:time_zone, " - unknown time zone #{self.time_zone}")
    end
    
    n = Time.now.in_time_zone self.time_zone
    st = self.start_time.in_time_zone(self.time_zone)
    
    if n > st && self.schedule_once?
      self.errors.add(:start_time, " - is in the past")
      self.errors.add(:start_date, " - maybe in the past")
    end
    
    self.errors.empty?
  end
      
  def scheduled?
    !self.schedule_none? && !self.schedule.nil? && !self.next_occurrence.nil?
  end
  
  def schedule_type_name(t = nil)
    t = self.schedule_type unless t
    t.split("_").last.capitalize
  end
  
  def schedule_in_words
    if self.schedule && !self.schedule_none?
      t = self.schedule.start_time.in_time_zone(self.time_zone)
      self.schedule_once? ? "Once on #{t.strftime('%B %d, %Y %I:%M %p %Z')}" : self.schedule.to_s
    else
      "No schedule rule has been applied"
    end
  end
  
  def next_occurrence
    if self.schedule_once?
      n = Time.now.in_time_zone self.time_zone
      st = self.start_time.in_time_zone(self.time_zone)
      
      n > st ? nil : st
    else
      self.schedule.next_occurrence
    end
  end
      
  def schedule_update
    Time.zone = self.time_zone
    start = Time.zone.parse("#{self.start_date} #{self.start_time}")
    sched = Schedule.new(start)
    
    if self.days_of_week
      self.days_of_week = self.days_of_week.reject(&:blank?).collect {|w| w.downcase.to_sym }
    end
    
    self.schedule_period = 1 if self.schedule_period.nil? || self.schedule_period.to_i < 1
    
    if self.schedule_hourly?
      rule = Rule.hourly(self.schedule_period)
      rule.day(self.days_of_week) if self.days_of_week
      sched.add_recurrence_rule rule
    elsif self.schedule_daily?
      rule = Rule.daily(self.schedule_period)
      sched.add_recurrence_rule rule
    elsif self.schedule_weekly?
      rule = Rule.weekly(self.schedule_period)
      rule.day(self.days_of_week) if self.days_of_week
      sched.add_recurrence_rule rule
    elsif self.schedule_monthly?
      rule = Rule.monthly(self.schedule_period)
      
      if !self.days_of_week.empty? && self.weeks_of_month        
        wks = self.weeks_of_month.reject(&:blank?).collect {|w| WEEKS_OF_MONTH[w.to_sym]  }
        days_in_month = {}
        self.days_of_week.reject(&:blank?).each {|d| days_in_month[d.downcase.to_sym]  = wks }
        rule.day_of_week(days_in_month)
      elsif !self.days_of_week.empty?        
        rule.day(self.days_of_week)
      elsif self.day_of_month        
        rule.day_of_month(self.day_of_month.to_i)                 
      end
      
      sched.add_recurrence_rule rule
    elsif self.schedule_yearly?
      rule = Rule.yearly(self.schedule_period)
      
      rule.month_of_year(self.months_of_year.reject(&:blank?).map(&:to_sym)) if self.months_of_year
      
      if !self.days_of_week.empty? && self.weeks_of_month
        wks = self.weeks_of_month.reject(&:blank?).collect {|w| WEEKS_OF_MONTH[w.to_sym] }
        days_in_month = {}
        self.days_of_week.reject(&:blank?).each {|d| days_in_month[d.downcase.to_sym] = wks }
        rule.day_of_week(days_in_month)
      elsif !self.days_of_week.empty?
        rule.day(self.days_of_week.reject(&:blank?))
      elsif self.day_of_month
        rule.day_of_month(self.day_of_month.to_i)          
      end
      
      sched.add_recurrence_rule rule
    end
    
    # Only update schedule if the
    # actual schedule has changed
    if self.schedule.nil? || self.schedule.to_s != sched.to_s || self.schedule.start_time != sched.start_time
      self.schedule = sched 
    end
  end
    
  
  protected
    
    def schedule_to_model
      return if self.schedule.nil? || self.schedule_none?
      
      self.time_zone = self.schedule.start_time.time_zone.name
      self.start_date = self.schedule.start_time.to_date
      self.start_time = self.schedule.start_time
      
      return if self.schedule_once? 
      
      val = self.schedule.recurrence_rules.last.validations
              
      if val[:day]
        self.days_of_week = val[:day].collect { |v| Date::DAYNAMES[v.day].downcase.to_sym if v.day }
        self.days_of_week.uniq!
      elsif val[:day_of_week]
        self.days_of_week = [] 
        self.weeks_of_month = []
        val[:day_of_week].each { |v| 
          self.days_of_week << Date::DAYNAMES[v.day].downcase.to_sym
          self.weeks_of_month << WEEKS_OF_MONTH.key(v.occ).downcase.to_sym
        }
        self.weeks_of_month.uniq!
        self.days_of_week.uniq!
      end
      
      if val[:month_of_year]
        self.months_of_year = val[:month_of_year].collect {|m| Date::MONTHNAMES[m.month].downcase.to_sym }
        self.months_of_year.uniq!
      end
      
      if val[:day_of_month]
        self.day_of_month = val[:day_of_month].first.day
      end
      
      self.schedule_period = val[:interval].last.interval      
    end
 
  private 
  
    def set_defaults
      self.schedule_period ||= 1
      self.time_zone ||= "UTC"
      self.start_date ||= Time.now.to_date
      self.start_time ||= Time.now     
      self.days_of_week ||= []
    end
     
end