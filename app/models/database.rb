class Database < ApplicationRecord
    belongs_to :project
    belongs_to :user
    has_many :loaders, dependent: :nullify
    has_many :bucket_loaders, dependent: :nullify
    has_many :tables, dependent: :destroy
    accepts_nested_attributes_for :tables
    
    include EncryptedFields 
    attr_encrypted :password, key: :encryption_key
        
    enum test_connection_status: [:testing, :failed, :succeeded]
    enum schema_status: [:schema_fetching, :schema_failed, :schema_ready]
    
    validates :user, :name, :server, :port, :db_name, :user_name, :project, presence: true
    validates :port, numericality: { only_integer: true }
    
    before_save :update_connection_status
    
    def to_ll
      ll = {
             name: self.name,
             serverAddress: self.server,
             port: self.port,
             databaseName: self.db_name,
             username: self.user_name,
             password: self.password,
             jdbcURL: self.jdbc_url,
             userId: self.user_id,
             props: properties_object
          }
    end
    
    def reset_test_status
       self[:test_connection_status] = :testing
       self.test_error = nil
    end
    
    def reset_schema_status
      self[:schema_status] = :schema_fetching
      self.schema_error = nil
    end
    
    def properties_object
        if self.properties
           JSON.parse(self.properties)
        else
            {}
        end 
    end
    
    def reencrypt!(new_key)
      ts = self[:test_connection_status]
      te = self.test_error
      
      pw = self.password  
      self.update_columns(encrypted_password: nil, encrypted_password_iv: nil, dek_id: new_key.id)
      self.reload
       
      self.password = pw
      self.save(validate: false)
      
      self.update_columns(test_connection_status: ts, test_error: te)
    end
    
    private 
      
    def update_connection_status
      unless (self.changes.keys & ["server","port","password", "db_name", "user_name", "properties"]).empty?
        self.reset_test_status
      end
    end
end
