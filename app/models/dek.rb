class Dek < ApplicationRecord
  
  def self.primary
    find_by(primary: true)
  end
  
  def self.generate!(attrs={})
    create!(attrs.merge(key: AES.key))
  end
  
  def promote!
    self.transaction do
      Dek.primary.update_attributes!(primary: false) if Dek.primary
      update_attributes!(primary: true)
    end
  end
  
end
