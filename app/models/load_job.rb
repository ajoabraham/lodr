class LoadJob < ApplicationRecord
    # Authorize project associations
    include AuthorizeProject  
    
    PERIODS = { 
        "This Month"=>"this_month", "Today" => "today",
        "Last 90 Days" => "last_90", "Last 30 Days" => "last_30", 
        "Last 7 Days" => "last_7"}
    
    belongs_to :project    
    belongs_to :loader
    belongs_to :user
        
    enum status: [:queued, :running, :failed, :succeeded, :succeeded_with_errors, :cancelled, :scheduled]
    before_create :set_defaults
    before_save :ensure_status_and_info
    before_save :update_user_subscription
    
    validates :user, :loader, :project, presence: true
    validate :authorized_project?
    
    def self.period_to_filter(period)
      if period == "last_90"
        "created_at >= current_date - 90"
      elsif period == "last_30"
        "created_at >= current_date - 30"
      elsif period == "last_7"
        "created_at >= current_date - 7"
      elsif period == "today"
        "cast(created_at as date) = current_date"
      else
        "created_at between cast(date_trunc('month', current_date) as date) and current_date"
      end
    end
    
    def self.stats(project, period="last_30")
      sql = <<-SQL
        select 
          sum(case when status = 1 then 1 else 0 end) as "running_jobs",
          sum(case when status = 2 then 1 else 0 end) as "failed_jobs",
          sum(case when status = 3 then 1 else 0 end) as "succeeded_jobs",
          sum(case when status = 5 then 1 else 0 end) as "cancelled_jobs",
          sum(total_rows_loaded) as "total_rows_loaded",
          sum(input_rows) as "rows_processed",
          avg(extract(epoch from (finished_at - started_at))) as "avg_dur",
          sum(input_bytes) as "bytes_processed",
          avg(input_rows/extract(epoch from (finished_at - started_at))) as "avg_rows_per_sec",
          avg(input_bytes/extract(epoch from (finished_at - started_at))) as "avg_byte_throughput"
        from #{self.table_name}
        where project_id = #{project.id} and #{LoadJob.period_to_filter(period)}
      SQL
      
      ActiveRecord::Base.connection.execute(sql).to_a.first
    end
    
    def self.performance(project, period="last_30")
      sql = <<-SQL
        select 
          extract(epoch from date_trunc('day',started_at))*1000 as "started_at",
          sum(input_rows) as "rows_processed",
          avg(input_rows/extract(epoch from (finished_at - started_at))) as "avg_rows_per_sec",
          avg(extract(epoch from (finished_at - started_at))) as "avg_dur"
        from #{self.table_name}
        where project_id = #{project.id} and #{LoadJob.period_to_filter(period)}
        group by 1
        having sum(input_rows)>0 
        order by 1 asc        
      SQL
      
      ActiveRecord::Base.connection.execute(sql).to_a
    end
    
    def is_running?
      (queued? || running?)
    end
    
    def duration
      return @duration if @duration
      
      if started_at && finished_at
        @duration = finished_at - started_at
      elsif started_at
        @duration = Time.now - started_at
      else
        @duration = -1
      end
    end
    
    def rows_per_second
      if duration > -1 && input_rows
        (input_rows/duration)
      else
        -1
      end
    end
    
    def bytes_per_second
      if duration > -1 && input_bytes
        (input_bytes/duration)
      else
        -1
      end
    end
    
    def to_param
       job_id 
    end
    
    def to_ll
      file_infos = self.loader.source_files.collect { |f| f.to_ll }
      loader.to_ll
            .merge({
              fileInfos: file_infos,
               jobId: self.job_id,
               userId: self.user_id,
               webhook: "#{Rails.application.secrets.internal_webhook_base}/api/xo/load_jobs/#{self.job_id}"            
           })
    end
    
    def as_json(opts = {})
      opts.merge!(
        methods: [:duration, :bytes_per_second, :rows_per_second], 
        except: :execution_errors)
      super(opts)
    end
    
    private
    
    def set_defaults
        self.status = :queued if self.status.nil?
        self.job_id = SecureRandom.uuid
        self.loader_name = self.loader.name
    end 
    
    def ensure_status_and_info
      if self.status_was == "cancelled"
        self.status = :cancelled
        self.info = "This job was cancelled."
      end
    end
    
    def update_user_subscription
      if (self.succeeded? || self.succeeded_with_errors?) && !self.updated_sub
        self.updated_sub = true
        self.project.owner.subscription.rows_processed += ( self.input_rows || 0)
        self.project.owner.subscription.save
      end
    end    
end
