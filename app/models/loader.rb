require "#{Rails.application.config.root}/lib/wrangler"

class Loader < ApplicationRecord
  belongs_to :project
  belongs_to :user
  belongs_to :database
  belongs_to :bucket_loader
  
  # this has to come before load jobs
  # dependency related destroy callbacks
  # get fired
  before_destroy :cancel, :remove_scheduled_jobs
  
  has_many :load_jobs, dependent: :nullify
  has_many :loader_sources, dependent: :destroy
  has_many :source_files, through: :loader_sources
  
  accepts_nested_attributes_for :source_files, allow_destroy: true
  
  # Loader create stage
  enum step: [:choose_inputs,:loader_settings, :transformations, :target_table, :summary, :ready]
  
  # Redshift Distribution Style
  enum distribution_style: [:dist_key, :dist_even, :dist_all]
  
  # File update mode for ongoing runs
  #   of load job
  enum update_mode: [:mode_replace, :mode_append, :mode_merge]
  
  # Multi file uploads. This is where a loader
  #   ingests multiple files for a single target
  #   table
  enum multi_file_mode: [:multi_mode_union, :multi_mode_append, :multi_mode_merge, :multi_mode_join]
  
  # Schedule types
  include Scheduled
  # Authorize project associations
  include AuthorizeProject  
  
  before_save :set_name_and_target_table, :set_column_settings_when_blank
  before_validation :reject_blanks
  validates :name, :user, :project, presence: true
    
  # validate the wizard in phases
  # After sources are chosen
  validate :sources_are_valid?, if: :loader_settings?
  validate :transformations_are_valid?, if: :target_table?
  validate :target_table_is_valid?, if: :summary?
  validate :schedule_valid?, if: :summary?
  validate :loader_is_valid?, if: :ready?
  
  def self.search(search)
    unless search.blank?
      where('lower(name) LIKE ?', "%#{search.downcase}%")
    else
      order(created_at: :desc)
    end
  end
  
  def estimated_input_rows
    @estimated_rows ||= self.source_files.where.not(rows: nil).sum(:rows)
  end
  
  def will_exceed_quota?
    if Rails.application.secrets[:on_prem]
      false
    else
      self.project.owner.update_subscription_parameters
      estimated_input_rows > self.project.owner.subscription.rows_remaining
    end
  end
  
  def is_running?(excluding = nil)
    l = self.load_jobs.where(status: [:running,:queued]).where.not(id: excluding).count
    return l>0
  end
  
  # checks if loader is valid
  # active, and isn't already running
  def is_runnable?(excluding = nil)
    self.active && loader_is_valid? && !is_running?(excluding)
  end
  
  def cancel
    jobs = self.load_jobs.where(status: [:running,:queued])
    jobs.each do |l|
      begin
        logger.info "Cancelling job: #{l.job_id}"
        resp = HTTParty.get("#{Wrangler::END_POINTS[:cancel_job]}#{l.job_id}")
      rescue => e
        logger.error "Failed to cancel job #{l.job_id} \n #{e.message}"
      end
     end
     
     jobs.update_all(status: :cancelled, 
       finished_at: Time.now,
       info: "Job was cancelled")
  end
  
  def remove_scheduled_jobs
    LoadJob.where(loader_id: self.id, status: :scheduled).destroy_all unless self.id.nil?
  end
    
  def sources_are_valid?
      if self.source_files.size ==0
        errors.add(:source_files, " - choose at least 1 source file")
        return errors.empty?
      end 
      
      file_headers = []
      # limiting checked source files
      # 15 due to out of memory when using
      # large file sets from bucket loaders
      sfiles = self.source_files.limit(15)
      sfiles.each do |f| 
          if f.failed?
              self.errors.add(:invalid_files, " - #{f.name} failed pre processing")
          end
          file_headers.push( fetch_column_names(f) )
      end
            
      size = file_headers.first.size
      if file_headers.any?{ |f| f.size != size }
          self.errors.add(:columns, " - all source files should have the same number of columns")
      end
      
      file_headers = file_headers.flatten.uniq
      sfiles.each do |f|
          hd = fetch_column_names(f)
          diff = file_headers - hd
          if diff.size >0
              self.errors.add(:columns, " - all source files should have the same columns. #{f.name} is missing these columns #{diff}")
          end
      end
      
      if self.use_file_prefix 
        if self.source_files.count > 1
          self.errors.add(:source_files, " - only 1 source file can be selected when loading the latest matching file based on a prefix.")
        end
        
        if self.file_prefix.blank?
          self.errors.add(:source_files, " - please enter a file prefix")
        end
      end
      
      errors.empty?      
  end
  
  def has_source_errors?
     (errors.keys & [:source_files,:invalid_files,:columns]).any?
  end
  
  def transformations_are_valid?
     errors.add(:column_settings, " - invalid column settings") if column_settings.blank? 
     errors.empty?
  end
  
  def has_transform_errors?
     errors.keys.include? :column_settings
  end
  
  def has_schedule_errors?
     (errors.keys & [:start_time,:start_date, :time_zone]).any?
  end
  
  def target_table_is_valid?    
      [:target_table_name, :database_id, :update_mode, :distribution_style].each do |a|
         errors.add(a, " must be present ") if send(a).blank? 
      end
      
      authorized_project?
      
      errors.add(:target_table_name, " - must start with letter character") if target_table_name && target_table_name.first.match(/\d/)
      errors.add(:target_table_name, " - spaces, quotes, exclamations are not allowed ") if target_table_name && target_table_name.match(/[\'\"\s!]/)
      errors.add(:merge_keys, " - choose at least 1 merge key") if (merge_keys.nil? || merge_keys.size == 0) && mode_merge?
      errors.add(:distribution_key, " - choose a distribution key for a Key based distribution style") if dist_key? && distribution_key.nil?
      errors.empty?
  end
  
  def has_target_errors?
     (errors.keys & [:target_table_name, :database_id, 
         :update_mode, :distribution_style, :merge_key, :distribution_key]).any?
  end
  
  def loader_is_valid?
      sources_are_valid?
      transformations_are_valid?
      target_table_is_valid?
      errors.empty?
  end
  
  # Check box collection form is
  # sending a blank value in addition
  # to the actuals. Need to clear the
  # blanks before validation
  def reject_blanks
      self.merge_keys.reject! {|k| k.blank? || k=="0" || k==0 } if self.merge_keys
      self.sort_keys.reject! {|k| k.blank? || k=="0" || k==0 } if self.sort_keys
      self.primary_keys.reject! {|k| k.blank? || k=="0" || k==0 } if self.primary_keys
      
      self.distribution_key = nil if !dist_key?
  end
  
  def self.next_step(istep)
     if istep == :choose_inputs
         :loader_settings
     elsif istep == :loader_settings
         :transformations
     elsif istep == :transformations
         :target_table
     else
         :summary
     end 
  end
  
  def self.namify(name)
     s = name.split("_")
     s[s.size-1].capitalize
  end
  
  def last_run
     @last_run ||= load_jobs.where.not(status: :scheduled).order(updated_at: :asc).last 
  end
  
  def headers
    if self.column_settings && @headers.nil?
        @headers = self.parsed_column_settings.collect { |m| m["columnName"] }
    end
        
    return @headers || []
  end
  
  def parsed_column_settings     
     @parsed_column_settings ||= JSON.parse(self.column_settings || "[]")
  end  
  
  def to_ll
     ll = {
         useFilePrefix: self.use_file_prefix,
         filePrefix: self.file_prefix,
         originalColumnNames: fetch_column_names(self.source_files.first),
         tableName: self.target_table_name,
         loadMode: update_mode_to_ll,
         primaryKeys: self.primary_keys,
         mergeKeys: self.merge_keys,
         sortKeys: self.sort_keys,
         distKey: self.distribution_key,
         distStyle: self.distribution_style.split("_").last.upcase,
         preSql: self.pre_sql,
         postSql: self.post_sql
     }
     ll[:columnInfos] = JSON.parse(self.column_settings) if self.column_settings
     ll[:wrangleScript] =  JSON.parse(self.transformations) if self.transformations
     ll[:connInfo] = self.database.to_ll if self.database
     return ll
  end
  
  def update_mode_to_ll
      update_mode.split("_")[1].upcase
  end
  
  def contains(meth_sym, item)
    array_container = self.send(meth_sym)
    !array_container.blank? && array_container.include?(item)
  end
  
  def set_column_settings_when_blank
    if self.parsed_column_settings.size == 0 || self.parsed_column_settings[0]["columnName"].nil?
      sf = self.source_files.first
      self.column_settings = sf.headers if sf
    end
  end
  
  private
  
  def fetch_column_names(f)
     hd = f.parsed_headers.collect{ |hd| 
            if hd.class == String
                hd.strip
            else
               hd["columnName"].strip  
            end                 
     }
     return hd
  end
  
  def set_name_and_target_table
      sf = self.source_files.first
      if sf && self.name == "Untitled"           
          self.name = "Load " + sf.clean_name 
      end
      
      if sf && self.target_table_name.nil?           
          self.target_table_name = sf.name.split("/").last.split(".").first
          self.target_table_name.gsub!(/[^[:alnum:]_,$]/,"_")
          self.target_table_name.gsub!(/_+/, "_")
          if self.target_table_name.first.match(/\d/)
            self.target_table_name = "l_" + self.target_table_name
          end
      end
  end
  
end
