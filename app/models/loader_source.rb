class LoaderSource < ApplicationRecord
    belongs_to :source_file
    belongs_to :loader
end
