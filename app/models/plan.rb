class Plan < ApplicationRecord
  has_many :subscriptions
  has_many :users, through: :subscriptions
  
  enum period: [:monthly, :annual]
  
  validates :name, :desc, :stripe_id, :amount, :period, :rows_per_month, :queries_per_month,  presence: true
  validates :stripe_id, uniqueness: true
  
  ["bootstrap", "startup", "big_data", "elite", "business", "enterprise"].each do |p|
    define_singleton_method("#{p}") do
      self.find_by_stripe_id p
    end
  end  
  
  def is_stripe_paid_plan?
    ["startup", "big_data", "elite", "business"].include?(self.stripe_id)
  end
  
  
end
