class Project < ApplicationRecord
  
  # this must be ahead of the dependent
  # stuff so it will validate first
  before_destroy :check_destroyable
  
  has_many :loaders, dependent: :destroy
  has_many :bucket_loaders, dependent: :destroy
  has_many :load_jobs, dependent: :destroy
  
  
  has_many :scripts, dependent: :destroy
  has_many :databases, dependent: :destroy  
  has_many :source_files, dependent: :destroy
  has_many :zip_files, dependent: :destroy
  has_many :s3_buckets, dependent: :destroy
  
  has_many :project_users, dependent: :destroy
  has_many :users, -> { distinct }, through: :project_users
  has_many :queries, through: :scripts
  has_many :bucket_load_histories, through: :bucket_loaders
  
  belongs_to :owner, class_name: "User"
  
  # Field Encryption
  include EncryptedFields 
  attr_encrypted :api_token, key: :encryption_key
  
  validates :name, :owner, :time_zone, presence: true
  after_create :add_owner_project_user
  
  before_save :generate_token, unless: :api_token  
  
  def generate_token
    loop do
        self.api_token = SecureRandom.urlsafe_base64(nil, false)
        break unless self.class.exists?(encrypted_api_token: self.encrypted_api_token )
    end
  end
  
  def add_owner_project_user
    self.users << self.owner
  end
  
  def can_destroy_project?(user)
    if user.id == owner.id && owner.projects.count > 1
      true
    else
      false
    end
  end
  
  private
  
  def check_destroyable
    throw(:abort) unless self.owner_id ==-99 || can_destroy_project?(self.owner)
  end
  
end
