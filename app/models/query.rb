class Query < ApplicationRecord
  belongs_to :script
  belongs_to :user
  
  enum status: [:running, :completed, :failed]
  validates :sql, :script, :user, presence: true
  
  before_save :destroy_old_history
  before_save :update_user_subscription
  
  
  private
  
  # We limit the total history to 10.
  # need to delete those older than the last 10th item
  def destroy_old_history
    history_count = self.script.queries.count
    if history_count > 10
      dels = self.script.queries.where.not(status: :running).order(created_at: :asc)
      dels[0..(history_count-10)].each{ |q| q.destroy }
    end    
  end
  
  def update_user_subscription
    unless self.updated_sub
      self.updated_sub = true
      owner = self.script.project.owner
      owner.subscription.queries_run += 1
      owner.subscription.save
    end
  end 
end
