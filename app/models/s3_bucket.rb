class S3Bucket < ApplicationRecord
    belongs_to :project
    belongs_to :user
    has_many :source_files, as: :loadable, dependent: :destroy
    
    # Field Encryption
    include EncryptedFields 
    attr_encrypted :access_key, key: :encryption_key
    
    validates :access_id, :region, :bucket_name, :user, :project, presence: true
    validate :bucket_exists?
    
    
    REGIONS = { "US Standard" => "us-east-1", 
                "US East (N. Virginia)" => "us-east-1", "US East (Ohio)" => "us-east-2",
                "US West (N. California)" => "us-west-1", "US West (Oregon)" => "us-west-2",
                "Asia Pacific (Mumbai)" => "ap-south-1", "Asia Pacific (Seoul)" => "ap-northeast-2", 
                "Asia Pacific (Singapore)" => "ap-southeast-1", "Asia Pacific (Sydney)" => "ap-southeast-2", 
                "Asia Pacific (Tokyo)" => "ap-northeast-1", "EU (Frankfurt)" => "eu-central-1",
                "EU (Ireland)" => "eu-west-1", "South America (São Paulo)" => "sa-east-1" }
    
    attr_reader :contents
    attr_reader :response   
    def list_objects(options = {})
        @contents = []
        opts = {bucket: self.bucket_name }.merge(options)
        
        begin
            creds = Aws::Credentials.new(self.access_id, self.access_key)
            @s3 = Aws::S3::Client.new(region: self.region, credentials: creds, follow_redirects: false)
            
            @response = @s3.list_objects_v2(opts)
            @contents = @response.contents.select do |o|
                            ext = o.key.split(".")
                            o.key.last != "/" &&
                            ( ext.size == 1 || SourceFile::UPLOADABLE_FILE_TYPES.include?(ext.last.downcase) )
                        end    
        rescue => e
           self.errors.add(:base, "Aws Error: #{e.message}")
        end
        
        @contents
    end
    
    def bucket_exists?
      creds = Aws::Credentials.new(self.access_id, self.access_key)
      client = Aws::S3::Client.new(region: self.region, credentials: creds, follow_redirects: false)
      resp = client.head_bucket({ bucket: self.bucket_name })      
    rescue => e
       self.errors.add(:base, "Bad credentials or incorrect region and bucket configuration.")
    end
    
    def client
      unless @client
        creds = Aws::Credentials.new(self.access_id, self.access_key)
        @client = Aws::S3::Client.new(region: self.region, credentials: creds, follow_redirects: false)
      end
            
      @client
    end
    
    def reencrypt!(new_key)
      ky = self.access_key    
      self.update_columns(encrypted_access_key: nil, encrypted_access_key_iv: nil)
      self.reload
      
      self.dek = new_key
      self.access_key = ky
      self.save(validate: false)
    end
    
    def clear
      @contents = nil
      @response = nil
    end
    
end
