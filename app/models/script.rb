class Script < ApplicationRecord
  # Authorize project associations
  include AuthorizeProject  
  
  belongs_to :project
  has_many :queries, dependent: :destroy
  belongs_to :database
  belongs_to :user
  
  enum status: [:running, :completed, :failed]
  
  validates :name, :project, :user, presence: true
  validate :authorized_project?
  
end
