class SourceFile < ApplicationRecord
  belongs_to :project
  belongs_to :user
  belongs_to :loadable, polymorphic: true
  belongs_to :bucket_loader
  
  has_one :sample, dependent: :destroy
  has_many :loader_sources, dependent: :destroy
  has_many :loaders, through: :loader_sources
    
  # file processing stage
  #   checking_file_settings, 
  #   building_full_preview, 
  #   ready
  enum status: [:pre_processing, :profiling, :ready, :failed, :loaded]
  
  # Row count type.
  # - approximate is guess based on sample
  # - precise is calculated after we run
  #   at least one wrangle process
  enum row_count_type: [:approximate, :precise ]
  
  validates :name,:user, :project, presence: true
  validates :name, uniqueness: { scope: [:project,:loadable, :bucket_loader], message: " - file already exists"}, on: :create
  validate :file_format, on: :create
    
  before_save :fix_file_type
  after_destroy :delete_s3_object  
  
  ENCODINGS = Wrangler::ENCODINGS
  EXCEL_TYPES = ["xlsx", "xls"]
  CSV_TYPES = ["csv", "psv", "tsv"]
  UPLOADABLE_FILE_TYPES = ["json","jsonl", "jsonp","txt", "list", "log", "gzip", "gz", "zip", "bzip2", "bz2", CSV_TYPES].flatten
  
  def self.search(search)
    unless search.blank?
      where('lower(name) LIKE ?', "%#{search.downcase}%")
    else
      order(created_at: :desc)
    end
  end
    
  def file_format
      ext = File.extname(self.name).gsub!(".","")
      types = EXCEL_TYPES
      if !ext.blank? && types.include?(ext.downcase)
        errors.add(:extension, " #{ext} is not a valid format. Valid types: #{UPLOADABLE_FILE_TYPES.join(', ')}")
      end
  end
  
  def to_ll
     base = {  
          userId: user_id,
          sampleRowCount: sample_size,
          encoding: encoding,
          delimiter: delimiter,
          rowCount: self.rows,
          quote: quote_char,
          objectKey: object_key,
          includesHeader: includes_header,
          accessId: Rails.application.secrets.s3_key_id,
          accessKey: Rails.application.secrets.s3_access_key,
          region:  Rails.application.secrets.s3_bucket_region,
          bucketName: Rails.application.secrets.s3_upload_bucket }
          
      if self.loadable && self.loadable_type == "S3Bucket"
          base.merge!({
              accessId: self.loadable.access_id,
              accessKey: self.loadable.access_key,
              region: self.loadable.region,
              bucketName: self.loadable.bucket_name
          })
      end
      
      return base
  end
  
  def clean_name
      if self.loadable_type == "S3Bucket"
          self.name.split("/").last
      else
         self.name 
      end
  end
  
  def is_s3?
     self.loadable && self.loadable_type == "S3Bucket" 
  end
  
  def parsed_headers
     @parsed_headers ||= JSON.parse(self.headers || "[]")
  end      
      
  def object_key
      if self.loadable && self.loadable_type == "S3Bucket"
          self.name
      else
         "#{user_id}/uploads/#{name}" 
      end
  end
  
  def delete_s3_object
      if self.loadable.nil?
        S3_USER_UPLOAD_BUCKET
          .delete_objects({delete: { objects: [{ key: object_key }] }})        
      end
  end
  
  def generate_settings_and_sample
     begin
        payload = to_ll.to_json
        
        resp = HTTParty.post(Wrangler::END_POINTS[:sample], 
                  body: payload,
                  headers: {
                       'Content-Type' => 'application/json', 
                       'Accept' => 'application/json'
                     })
           
        body = JSON.parse resp.body
        
        if body["error"].nil? && resp.code == 200
            
          update_attributes(
            encoding: body["encoding"],
            delimiter: body["delimiter"],
            quote_char: body["quote"],
            headers: body["columnNames"].to_json,
            columns: body["columnNames"].size,
            size: body["fileSize"],
            status: :profiling        
          )      
      
          # save the incoming sample
          samp = Sample.find_or_initialize_by(source_file_id: self.id)
          samp.sample = body["previewData"].to_json
          samp.save
        elsif resp.code != 200
          update_attributes(processing_errors: body["message"], status: :failed)
        else
          update_attributes(processing_errors: body["error"], status: :failed)
        end
    rescue => e
        changes.each {|k,vs| self[k] = vs.first}
        update_attributes(processing_errors: e.message, status: :failed)
        
        errors.add(:base, :pre_processing_error,
          message: e.message)
        
        false
    rescue Exception => e
        changes.each {|k,vs| self[k] = vs.first}
        update_attributes(processing_errors: e.message, status: :failed)
        
        errors.add(:base, :pre_processing_error,
          message: e.message)
        
        false
    end          
  end
  
  def profile
    payload = to_ll.to_json
    
    resp = HTTParty.post(Wrangler::END_POINTS[:profile], 
              body: payload,
              headers: {
                   'Content-Type' => 'application/json', 
                   'Accept' => 'application/json'
                 })
                 
    body = JSON.parse resp.body
    
    if body["error"].nil? && resp.code == 200
      res = {
        headers: body["columnInfos"].to_json,
        columns: body["columnInfos"].size,
        rows: body["totalRowCount"],
        status: :ready,
        processing_errors: nil
      }
      
      if body["isEstimatedRowCount"]
        res[:row_count_type] = :approximate
      else
        res[:row_count_type] = :precise
      end      
      update_attributes(res)      
      
      # save the incoming sample
      samp = Sample.find_or_initialize_by(source_file_id: self.id)
      samp.sample = body["previewData"].to_json
      samp.save      
    elsif resp.code != 200
      update_attributes(processing_errors: body["message"], status: :failed)
    else
      update_attributes(processing_errors: body["error"], status: :failed)
    end
    
  rescue Exception => e 
    update_attributes(processing_errors: e.message, status: :failed)
  end
  
  def auto_set_header_inclusion
    return if name.nil?
    
    if name =~ /\.json([^\w]|$)|\.log|\.xml/
      self.includes_header = false
    end
  end
  
  private
  
  def fix_file_type
     return if name.nil?
     
     ext = name.split(".").last.downcase
     
     if EXCEL_TYPES.include? ext
         self.file_type = "excel"         
     elsif CSV_TYPES.include? ext
         self.file_type = "csv"
     elsif ext == "log"
         self.file_type = "log"
     end     
  end
  
end
