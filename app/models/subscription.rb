class Subscription < ApplicationRecord
  belongs_to :user
  belongs_to :plan
  belongs_to :previous_plan, class_name: "Plan", foreign_key: "previous_plan_id"
  
  before_destroy :cancel
  
  validates :user, :plan, presence: true
  validates :stripe_customer_id, :stripe_sub_id, presence: true, if: :is_stripe_paid_plan?
  
  
  def update_period_and_usage    
    if self.current_period_ends_at.nil? || self.current_period_ends_at.to_date < Time.now.to_date
      self.reset_at = Time.now
      self.rows_processed = 0
      self.queries_run = 0
      
      if self.is_custom?
        
        if self.current_period_starts_at.nil?
          self.current_period_starts_at = Time.now.beginning_of_month
          self.current_period_ends_at = Time.now.end_of_month
        else
          cpa = self.current_period_starts_at
          curr_time = Time.now
          months_passed = (curr_time.year*12 + curr_time.month) - (cpa.year*12 + cpa.month)
          
          next_period = self.current_period_starts_at + months_passed.month
          self.current_period_starts_at = next_period.beginning_of_month
          self.current_period_ends_at = next_period.end_of_month
        end 
         
      elsif self.bootstrap? && !self.use_previous_plan?
          
        self.current_period_starts_at = Time.now.beginning_of_month
        self.current_period_ends_at = Time.now.end_of_month
             
      end
    end
    
    self.save if self.changed?  
  end
  
  def custom
    self.is_custom = true
    self.reset_at = Time.now
    self.rows_processed = 0
    self.queries_run = 0
    self.status = "active"
    self.current_period_starts_at = Time.now.beginning_of_month
    self.current_period_ends_at = Time.now.end_of_month
  end
  
  def custom!
    self.custom
    self.save
  end
  
  # Cancels current plan and moves user back to
  # bootstrap tier and save the sub.
  def cancel
    unless self.stripe_sub_id.nil?
      subscription = Stripe::Subscription.retrieve(self.stripe_sub_id)
      subscription.delete(:at_period_end => true)
    end
    
    self.cancel_at_period_end = true
    self.canceled_at = Time.now
    self.status = "canceled"
    self.previous_plan_id = self.plan.id
    self.bootstrap!
  end
  
  def rows_remaining
    rows = self.rows_per_month - self.rows_processed 
    rows = 0 if rows < 0
    rows
  end
  
  def queries_remaining
    if self.queries_per_month == -1
      queries = -1
    else
      queries = self.queries_per_month - self.queries_run 
      queries = 0 if queries < 0
    end
        
    queries
  end
  
  # If the subscription was canceled and we haven't
  # reached the end of the paid period.  We will use the
  # previous plan rows
  def queries_per_month
    if use_previous_plan?
      self.previous_plan.queries_per_month      
    else
      self.plan.queries_per_month
    end
  end
  
  # If the subscription was canceled and we haven't
  # reached the end of the paid period.  We will use the
  # previous plan rows
  def rows_per_month
    if use_previous_plan?
      self.previous_plan.rows_per_month      
    else
      self.plan.rows_per_month
    end
  end
  
  # When the user cancels a plan but the previous plan is still active.
  #   For example user cancels plan on Jan 10th which means the paid plan
  #   is still valid till Jan 31st. If today is Jan 12th the previous plan
  #   is the correct one, but if today is Feb 1 the current plan which is 
  #   usually bootstrap is active.
  def use_previous_plan?
    cancel_at_period_end && Time.now <= current_period_ends_at
  end
    
  def is_stripe_paid_plan?
    self.plan.is_stripe_paid_plan? && !self.is_custom?
  end
  
  ["bootstrap", "startup", "big_data", "elite", "business", "enterprise"].each do |p|
    define_method("#{p}?") do
      self.plan && self.plan.stripe_id == p
    end
    
    define_method("#{p}!") do |stripe = {}|
      self.plan = Plan.find_by_stripe_id p
      
      # create stripe sub if its a stripe paid
      # plan. Don't call stripe for free 
      # or invoiced/enterprise plans
      create_stripe_subscription(stripe) if is_stripe_paid_plan?
      self.save unless errors.any?
    end
  end
    
  def create_stripe_subscription(stripe)
    if stripe_customer_id.nil? && !([:source, :email].all? {|s| stripe.key? s })
      errors.add(:stripe, " missing email or source token")
      return 
    end
    
    if stripe_customer_id.nil?
      customer = Stripe::Customer.create(stripe.merge({description: "#{user.full_name} <#{user.email}>"}))
      self.stripe_customer_id = customer.id
    elsif stripe.include?(:source)
      customer = Stripe::Customer.retrieve(stripe_customer_id)
      customer.source = stripe[:source]
      customer.email = stripe[:email] || user.email
      customer.save      
    end
    
    card = customer.sources.data.first
    self.last4 = card.last4
    self.card_brand = card.brand
    self.exp_month = card.exp_month
    self.exp_year = card.exp_year
    
    if stripe_sub_id.nil?
     sub = Stripe::Subscription.create(
        :customer => stripe_customer_id,
        :plan => plan.stripe_id
      )
      self.stripe_sub_id = sub.id
    else
      sub = Stripe::Subscription.retrieve(stripe_sub_id)
      sub.plan = plan.stripe_id
      sub.save
    end
    
    self.current_period_starts_at = Time.at(sub.current_period_start)
    self.current_period_ends_at = Time.at(sub.current_period_end)
    self.canceled_at = nil
    self.cancel_at_period_end = false
  rescue Stripe::CardError, Stripe::InvalidRequestError => e
    errors.add(:stripe, e.message)
  end

end
