class Table < ApplicationRecord
  belongs_to :database
  
  enum schema_status: [:schema_fetching, :schema_failed, :schema_ready]
  enum table_type: [:view, :table]
  
  validates :name, :schema, :table_type, :columns, presence: true
  
  def reset_schema_status
    self[:schema_status] = :schema_fetching
    self.schema_error = nil
  end
end
