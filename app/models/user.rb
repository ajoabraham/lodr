class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable
         
  has_many :source_files, dependent: :destroy
  has_many :loaders, dependent: :destroy
  has_many :databases, dependent: :destroy
  has_many :load_jobs, dependent: :destroy
  has_many :s3_buckets, dependent: :destroy
  has_many :zip_files, dependent: :destroy
  has_many :scripts, dependent: :destroy
  has_many :bucket_loaders, dependent: :destroy
  
  has_many :project_users, dependent: :destroy
  has_many :projects,  -> { distinct }, through: :project_users
  
  # Do not dependent destroy this.
  # It will be taken care of Via Scripts
  has_many :queries
  has_one :subscription, dependent: :destroy
  
  validates :first_name, :last_name, presence: true
  
  before_create :generate_token
  after_create :subscribe_bootstrap, :create_default_project
  before_save :update_subscription_parameters, unless: :new_record?
  
  before_destroy :destroy_owned_projects
    
  def generate_token
    loop do
        self.api_access_key = Devise.friendly_token
        break unless self.class.exists?(api_access_key: self.api_access_key  )
    end
  end
  
  # Configure devise to use delayed_job
  # as the backend for mail delivery
  # see more @ https://github.com/plataformatec/devise#activejob-integration
  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end
  
  def full_name
    "#{first_name} #{last_name}"
  end
    
  # Cancels any existing subscription and
  # moves user into the free plan. Whenever
  # users cancel plans or signs up we will
  # add them to the free tier
  def subscribe_bootstrap
    sub = self.subscription
    
    if sub && !sub.new_record?
      sub.cancel
    elsif sub.nil?
      sub = Subscription.new user: self, plan: Plan.bootstrap, 
                reset_at: Time.now, rows_processed: 0, queries_run: 0
      sub.save
    end
    
  end
  
  def self.search(search)
    unless search.blank?
      where('lower(email) LIKE ?', "%#{search.downcase}%")
    else
      order(created_at: :desc)
    end
  end
  
  def create_default_project    
    unless Project.exists?(owner_id: self.id)
      p = Project.create owner_id: self.id, name: "My Project"
    end
  end
  
  def destroy_owned_projects
    p = Project.where(owner_id: self.id)
    p.update_all(owner_id: -99)
    p.destroy_all
  end
  
  def update_subscription_parameters
    self.subscription.update_period_and_usage
  end  
end
