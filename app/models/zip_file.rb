class ZipFile < ApplicationRecord
  belongs_to :project
  belongs_to :user
  belongs_to :loadable, polymorphic: true # could be from s3
  
  enum status: [:unzipping, :succeeded, :failed]
  
  after_destroy :delete_s3_object
  
  validate :file_format, on: :create
  
  ZIP_TYPES = ["gzip", "gz", "zip", "bzip", "bz", "bz2"]
  
  def self.is_zip?(file_name)
    ext = File.extname(file_name).gsub!(".","")
    ZIP_TYPES.include?(ext)
  end
  
  def request_unzip
    update_attribute(:status, :unzipping)
     
    begin
         payload = to_ll.to_json
       
         resp = HTTParty.post(Wrangler::END_POINTS[:unzip], body: payload, headers: { 'Content-Type' => 'application/json'})
                
         unless resp.code == 200
           update_attributes(failed_reason: "Request failed", status: :failed)
         end
     rescue => e
         update_attributes(failed_reason: e.message, status: :failed)       
         errors.add(:base, :failed_reason, message: e.message)
         
         false
     rescue Exception => e
       update_attributes(failed_reason: e.message, status: :failed)     
       errors.add(:base, :failed_reason, message: e.message)
       
       false
     end 
  end
  
  def clean_name
      if self.loadable_type == "S3Bucket"
          self.name.split("/").last
      else
         self.name 
      end
  end
  
  def is_s3?
     self.loadable && self.loadable_type == "S3Bucket" 
  end
      
  def object_key
      if self.loadable && self.loadable_type == "S3Bucket"
          self.name
      else
         "#{user_id}/uploads/#{name}" 
      end
  end
  
  def delete_s3_object
      if self.loadable.nil?
        S3_USER_UPLOAD_BUCKET
          .delete_objects({delete: { objects: [{ key: object_key }] }})        
      end
  end
  
  def to_ll
     base = {  
          userId: user_id,
          objectKey: object_key,
          accessId: Rails.application.secrets.s3_key_id,
          accessKey: Rails.application.secrets.s3_access_key,
          region:  Rails.application.secrets.s3_bucket_region,
          bucketName: Rails.application.secrets.s3_upload_bucket,
          webhook: "#{Rails.application.secrets.internal_webhook_base}/api/xo/zip_files/#{self.id}"
         }
          
      if self.loadable && self.loadable_type == "S3Bucket"
          base.merge!({
              accessId: self.loadable.access_id,
              accessKey: self.loadable.access_key,
              region: self.loadable.region,
              bucketName: self.loadable.bucket_name
          })
      end
      
      return base
  end
  
  private
  
  def file_format
    unless ZipFile.is_zip?(self.name)
      ext = File.extname(self.name).gsub!(".","")
      errors.add(:file_format, " #{ext} is not a supported zip format")
    end
  end
end
