Aws.config.update({
  region: 'us-west-2',
  credentials: Aws::Credentials.new(Rails.application.secrets.s3_key_id, Rails.application.secrets.s3_access_key)
})

S3_USER_UPLOAD_BUCKET = Aws::S3::Resource.new.bucket(Rails.application.secrets.s3_upload_bucket)
 