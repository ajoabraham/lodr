# custom log file for delayed jobs
Delayed::Worker.logger = Logger.new(File.join(Rails.root, 'log', 'dj.log'))

Delayed::Worker.destroy_failed_jobs = true
Delayed::Worker.max_attempts = 3