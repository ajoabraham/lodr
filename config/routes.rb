Rails.application.routes.draw do
  devise_for :users, :controllers => { :registrations => :registrations }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'account' => 'account#show'
  get 'account/new' => 'account#new'
  get 'account/choose_plan/:id' => 'account#choose_plan', as: 'account_choose_plan'
  post 'account/subscribe' => 'account#subscribe', as: 'account_subscribe'
  delete 'account/cancel' => 'account#cancel', as: 'account_cancel'
  
  resources :projects do
    resources :invites, only: [:new, :create, :destroy]
  end
  
  # bucketloader for mass loading a bucket
  resources :bucket_loaders do
    delete 'destroy_multiple', on: :collection    
    get 'preview(/:id)', to: "bucket_loaders#preview", as: 'preview', on: :collection
    
    member do
      post 'run'
      delete 'cancel'
    end
    
    scope module: :bucket_loader do   
      resources :sources, :load_jobs, only: [:index, :show]
    end
  end
    
  # Source Files
  resources :sources do 
     collection do
         get 'dismiss' => 'sources#dismiss_zips', as: 'dismiss_zips'
         get 'upload' => 'sources#upload'
         get 'status/all' => 'sources#file_status', as: 'all_status'
         get 'status/zips' => 'sources#zips_status', as: 'zips_status' 
         delete 'delete' => 'sources#destroy_multiple'
         
         resources :s3_buckets do
            post 'add' => 's3_buckets#add', format: false, on: :member, as: 'add_file'            
            post 'add_multiple' => 's3_buckets#add_multiple_files', format: false, on: :member, as: 'add_multiple_files'            
            post 'load' => 's3_buckets#load', format: false, on: :member, as: 'load_file'
            get 'search' => 's3_buckets#search', format: false, on: :member 
         end
     end
  end
  
  # Target Databases
  resources :databases do
    patch 'refresh', on: :member
    get 'status', on: :collection
    
    resources :tables
  end  
  
  resources :sql, only: [:index,:show] do
    patch 'database/:database_id', to: 'sql#update_db', on: :member, as: 'db'
    collection do
      resources :scripts do
        resources :queries do
          delete '', to: 'queries#delete_all', on: :collection
        end
      end
    end
  end
  
  # Load Jobs
  get 'loaders/back/(:id)' => 'loaders#back', as: 'loader_back'
  get 'loaders' => 'loaders#index'
  get 'loaders/status/all' => 'loaders#status', as: 'all_status'
  get 'loaders/(:id)/sources' => 'loaders#sources', as: 'loader_sources'
  get 'loaders/(:id)/settings' => 'loaders#settings', as: 'loader_settings'
  get 'loaders/(:id)/transforms' => 'loaders#transforms', as: 'loader_transforms'
  get 'loaders/(:id)/target' => 'loaders#target_table', as: 'loader_target_table'
  get 'loaders/(:id)/summary' => 'loaders#summary', as: 'loader_summary'
  get 'loaders/new' => 'loaders#new', as: 'new_loader'
  get 'loaders/:source_id/load' => 'loaders#load_source', as: 'loader_load_source'
  get 'loaders/:id/edit' => 'loaders#edit', as: 'edit_loader'
  get 'loaders/:id/show' => 'loaders#show', as: 'show_loader'
  delete 'loaders/:id' => 'loaders#destroy', as: 'delete_loader'
  patch 'loaders/:id/sources' => 'loaders#update_sources', as: 'update_loader_sources'
  patch 'loaders/:id/settings' => 'loaders#update_settings', as: 'update_loader_settings'
  patch 'loaders/:id/transform' => 'loaders#update_transform', as: 'update_loader_transform'
  patch 'loaders/:id/target' => 'loaders#update_target', as: 'update_loader_target'
  patch 'loaders/:id/schedule' => 'loaders#update_schedule', as: 'update_loader_schedule'
  get 'loaders/wait' => 'loaders#wait', as: 'loader_wait'
  patch 'loaders/exit' => 'loaders#exit', as: 'exit_loader', format: false
  get 'loaders/exit' => 'loaders#exit_wizzard', as: 'exit_wizzard', format: false
  get 'loaders/:id/run' => 'loaders#run', as: 'loader_run'
  get 'loaders/:id/cancel' => 'loaders#cancel', as: 'loader_cancel'
  post 'loaders/delete' => 'loaders#destroy_multiple', as: 'destroy_loaders'
  post 'loaders/run' => 'loaders#run_multiple', as: 'run_loaders'
  
  get 'dashboard' => 'dashboard#index'
  get 'dashboard/stats' => 'dashboard#stats'
  
  namespace :api do
      scope :hooks, as: 'hooks' do
        post 'stripe', to: 'webhooks#stripe_event'
      end
      
      namespace :xo do
         resources :load_jobs, only: [:update]
         resources :zip_files, only: [:update]
         get 'canary', to: 'load_jobs#canary'
         
         put 'db_requests/query/:id' => 'db_requests#query', as: 'db_query'
         put 'db_requests/schema/:id'=> 'db_requests#schema', as: 'db_schema'
         put 'db_requests/table/:id' => 'db_requests#table', as: 'db_table'
      end
      
      namespace :v1 do
        resources :jobs, only: [:index, :destroy, :show] do
          post ':loader_id/run' => 'jobs#run', on: :collection
        end
        resources :bucket_jobs, only: [:index, :destroy, :show] do
          post ':bucket_loader_id/run' => 'bucket_jobs#run', on: :collection
        end
      end
  end
  
  # Admin routes
  namespace :admin do
    get 'console', to: 'console#index'
    get 'console/:id/show', to: 'console#show'
    get 'console/load_jobs'
    get 'console/queries'
    get 'console/zips'
    get 'console/sources'
    get 'console/databases'
    delete 'cancel_long', to: 'console#cancel_long_running_jobs'
    delete 'cancel_all', to: 'console#cancel_all_jobs'
    delete 'cancel/:id', to: 'console#cancel', as: 'cancel'
    delete 'cancel_unzip/:id', to: 'console#cancel_unzip', as: 'cancel_unzip'
  end
  
  # static high voltage pages
  # this has to be at the bottom of routes file
  # or else it will block all other routes
  get "/*id" => 'pages#show', as: :page, format: false
  root to: 'pages#show', id: 'home'
end
