class CreateSourceFiles < ActiveRecord::Migration[5.0]
  def change
    create_table :source_files do |t|
      t.string :name, null: false, index: true
      
      t.string :file_type
      t.integer :size , limit: 8 #bytes
      t.integer :rows, limit: 8
      # approximate, precise
      t.integer :row_count_type
      t.integer :columns
      
      t.string :encoding
      t.string :delimiter
      t.string :quote_char
      
      # pre_processing, profiling, ready, failed
      t.integer :status
      t.string :processing_errors
      
      # all about the sample
      t.json :headers
      t.integer :sample_size, default: 500
      t.boolean :includes_header, null: false, default: true
      
      t.timestamp :uploaded_at
      
      # polymorphic with parent container
      t.references :loadable, polymorphic: true, index: true
            
      t.references :user, index: true            
      t.timestamps
    end
    
  end
end
