class CreateSamples < ActiveRecord::Migration[5.0]
  def change
    create_table :samples do |t|
      t.json :sample, null: false
      
      t.references :source_file, index: true, null: false
      t.timestamps
    end
  end
end
