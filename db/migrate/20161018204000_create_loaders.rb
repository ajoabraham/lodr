class CreateLoaders < ActiveRecord::Migration[5.0]
  def change
    create_table :loaders do |t|
      # choose_input, transformations, table_settings, summary
      t.integer :step, null: false, default: 0          
      t.boolean :active, null: false, default: false
      t.boolean :notify, null: false, default: true
      
      t.string :name, null: false, index: true
      t.string :target_table_name
      t.references :target, index: true
      
      # [{name:"",type:"",...},...]
      t.json :column_settings
      # [ { command, col_name, arguments },... ]
      t.json :transformations
      
      t.string :primary_keys, array: true      
      # mpp table
      t.string :sort_keys, array: true
      # RedShift All, Even, Key
      t.integer :distribution_style, default: 1
      # RedShift single Key
      t.string :distribution_key
      
      # non redshift tables
      t.json :table_indexes
      
      # Replace, Append, Upsert
      t.integer :update_mode, null: false, default: 0
      t.string :merge_keys, array: true
      
      # Union, Append, Merge, Join
      t.integer :multi_file_mode
      t.string :multi_file_merge_keys, array: true
      
      t.text :pre_sql
      t.text :post_sql
      
      # none, once, hourly, daily, weekly, monthly, quarterly, yearly, file_change
      t.integer :schedule_type, null: false, default: 0, index: true
      t.string :schedule_definition
            
      t.references :user, index: true
      t.timestamps
    end
  end
end
