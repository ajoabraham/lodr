class CreateLoadJob < ActiveRecord::Migration[5.0]
  def change
    create_table :load_jobs do |t|
      t.references :loader, index: true
      t.references :user, index: true
      
      t.string :loader_name, null: false
      t.uuid :job_id, null: false, index: true
      # { file_name: [ { column_name: "name", error_rows: 2, errors: [ ‘ distinct issue list ‘ ] } ]      
      t.json :field_errors
      # when our lodr processing is done
      t.datetime :lambda_done_at
      
      # queued running, failed, succeded, succeded_with_errors, cancelled
      t.integer :status, null: false
      t.integer :input_bytes, limit: 8
      t.integer :input_rows, limit: 8
      t.integer :total_rows_loaded, limit: 8      
      t.integer :inserted_rows, limit: 8      
      t.integer :updated_rows, limit: 8
      t.integer :total_failed_rows, limit: 8
      
      t.string :info
      t.text :execution_errors
      t.datetime :started_at
      t.datetime :finished_at
      
      t.timestamps
    end
  end
end
