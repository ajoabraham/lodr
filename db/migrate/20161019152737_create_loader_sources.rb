class CreateLoaderSources < ActiveRecord::Migration[5.0]
  def change
    create_table :loader_sources do |t|
      t.references :loader, null: false, index: true
      t.references :source_file, null: false, index: true
      t.timestamps
      
      t.index [:loader_id,:source_file_id]
    end
  end
end
