class CreateTargets < ActiveRecord::Migration[5.0]
  def change
    create_table :targets do |t|
        t.string :name, null: false, index: true
        t.string :server, null: false
        t.integer :port, null: false, default: 5439
        t.string :db_name, null: false
        t.string :user_name, null: false
        t.string :password
        
        t.boolean :use_ssl, null: false, default: false
        t.string :jdbc_url
        t.json :properties
        
        # testing, failed, succeeded
        t.integer :test_connection_status, null: false, default: 2
        t.string :test_error
        
        t.references :user, index: true
        t.timestamps
    end
  end
end
