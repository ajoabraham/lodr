class CreateS3Buckets < ActiveRecord::Migration[5.0]
  def change
    create_table :s3_buckets do |t|
      t.string :bucket_name, null: false, index: true
      t.string :access_id, null: false
      t.string :access_key, null: false
      t.string :region, null: false
      
      t.references :user, index: true    
      t.timestamps
    end
  end
end
