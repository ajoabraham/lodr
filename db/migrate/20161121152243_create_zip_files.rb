class CreateZipFiles < ActiveRecord::Migration[5.0]
  def change
    create_table :zip_files do |t|
      t.string :name, null: false, index: true
      
      t.integer :size, limit: 8
      t.integer :status, null: false, default: 0
      t.text :failed_reason
      
      # polymorphic with parent container for s3
      t.references :loadable, polymorphic: true, index: true
      t.references :user, index: true
      t.timestamps
    end
  end
end
