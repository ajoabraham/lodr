class CreatePlans < ActiveRecord::Migration[5.0]
  def change
    create_table :plans do |t|
      t.string :stripe_id, null: false, index: true
      t.string :name, null: false
      t.string :desc, null: false
            
      # in cents
      t.integer :amount, null: false
      # enum :monthly, :annual
      t.integer :period, null: false      
      t.integer :rows_per_month, limit: 8
      
      t.boolean :active, default: true

      t.timestamps
    end
    
  
    Plan.create(stripe_id: "bootstrap", name: "Bootstrap", desc: "1 Million Rows per Month", amount: 0, period: 0, rows_per_month: 50000)
    Plan.create(stripe_id: "startup", name: "Startup", desc: "30 Million Rows per Month", amount: 10000, period: 0, rows_per_month: 30000000)
    Plan.create(stripe_id: "elite", name: "Elite", desc: "300 Million Rows per Month", amount: 40000, period: 0, rows_per_month: 300000000)
    Plan.create(stripe_id: "business", name: "Business", desc: "1 Billion Rows per Month", amount: 60000, period: 0, rows_per_month: 1000000000)
    Plan.create(stripe_id: "enterprise", name: "Enterprise", desc: "Custom Enterprise Plan", amount: 0, period: 0, rows_per_month: -1)
  end
  
end
