class CreateSubscriptions < ActiveRecord::Migration[5.0]
  def change
    create_table :subscriptions do |t|
      t.references :user, index: true, null: false
      t.references :plan, index: true, null: false
      t.integer :previous_plan_id
      t.integer :last4
      t.string :card_brand
      t.integer :exp_month
      t.integer :exp_year
      t.string :status
      t.string :stripe_event
      t.string :stripe_sub_id, index: true
      t.string :stripe_customer_id, index: true
      t.datetime :current_period_starts_at
      t.datetime :current_period_ends_at
      t.boolean :cancel_at_period_end
      t.datetime :canceled_at

      t.timestamps
    end
  end  
  
end
