class AddFreeSubscriptionToCurrentUsers < ActiveRecord::Migration[5.0]
  def up
    p = Plan.find_by_stripe_id "bootstrap"
    User.all.each do |u|
      s = Subscription.create(plan: p, user: u)
    end
  end
  
  def down
    Subscription.delete_all
  end
end
