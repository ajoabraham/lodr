class ModifyLoaderColumnsRanAtSched < ActiveRecord::Migration[5.0]
  def up
    add_column :loaders, :last_ran_at, :datetime 
    add_column :loaders, :schedule, :text    
    remove_column :loaders, :schedule_definition
  end
    
  def down
     remove_column :loaders, :last_ran_at
     remove_column :loaders, :schedule
     add_column :loaders, :schedule_definition, :string
  end     
end
