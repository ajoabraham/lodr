class AddFilePrefixToLoaders < ActiveRecord::Migration[5.0]
  def change
    add_column :loaders, :use_file_prefix, :boolean, default: false
    add_column :loaders, :file_prefix, :string    
  end
end
