class AddSelecteFileToLoadJobs < ActiveRecord::Migration[5.0]
  def change
    add_column :load_jobs, :selected_file, :string
  end
end
