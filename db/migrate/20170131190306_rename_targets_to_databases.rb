class RenameTargetsToDatabases < ActiveRecord::Migration[5.0]
  def change
    rename_table :targets, :databases
    rename_column :loaders, :target_id, :database_id
    
    add_column :databases, :schema_status, :integer, default: 0
    add_column :databases, :schema_error, :text
  end
end
