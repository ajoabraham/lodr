class CreateTables < ActiveRecord::Migration[5.0]
  def change
    create_table :tables do |t|
      t.string :name, null: false, index: true
      t.string :schema
      t.integer :table_type
      t.json :columns
      t.integer :table_size, limit: 8
      t.integer :row_count, limit: 8
      t.text :view_def
      t.integer :schema_status
      t.text :schema_error
      
      t.references :database, index: true      
      t.timestamps
    end
  end
end
