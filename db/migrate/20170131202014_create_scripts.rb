class CreateScripts < ActiveRecord::Migration[5.0]
  def change
    create_table :scripts do |t|
      t.string :name, index: true, null: false
      t.text :content
      t.string :folder
      
      t.references :database, index: true
      t.references :user, index: true
      
      t.timestamps
    end
  end
end
