class CreateQueries < ActiveRecord::Migration[5.0]
  def change
    create_table :queries do |t|
      t.integer :status
      t.text :sql
      t.text :last_executed_query
      t.string :info
      t.text :query_error
      t.json :result_data
            
      t.timestamp :started_at
      t.timestamp :finished_at
      t.integer :total_rows, limit: 8
      t.integer :fetched_rows
      
      t.references :script, index: true
      t.references :user, index: true
      
      t.timestamps
    end
  end
end
