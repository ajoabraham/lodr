class UpdateSubPlanPricing < ActiveRecord::Migration[5.0]
  def up
    add_column :load_jobs, :updated_sub, :boolean
    add_column :queries, :updated_sub, :boolean
    add_column :subscriptions, :reset_at, :timestamp
    add_column :subscriptions, :rows_processed, :integer, limit: 8, default: 0
    add_column :subscriptions, :queries_run, :integer, limit: 8, default: 0
    add_column :plans, :queries_per_month, :integer
    
    Plan.bootstrap.update({queries_per_month: 100, desc: "50 Thousand Rows Processed and 100 Queries Run per Month"})
    Plan.startup.update({ queries_per_month: 2000, rows_per_month: 10000000, desc: "10 Million Rows Processed and 2000 Queries per Month" })
    Plan.elite.update({ queries_per_month: -1, rows_per_month: 150000000, desc: "150 Million Rows and Unlimited Queries per Month" })
    Plan.business.update({ queries_per_month: -1, rows_per_month: 500000000, desc: "500 Million Rows and Unlimited Queries per Month" })
    
    Subscription.all.each do |sub|
      starts = sub.current_period_starts_at
      ends = sub.current_period_ends_at
        
      rows = sub.user.load_jobs
                              .where("created_at >= ?", starts)
                              .where("created_at <= ?", ends)
                              .where(status: [:succeeded, :succeeded_with_errors])
                              .sum(&:input_rows)
      sub.rows_processed = rows
      sub.save
    end
  end
  
  def down
    remove_column :load_jobs, :updated_sub
    remove_column :queries, :updated_sub
    remove_column :subscriptions, :reset_at
    remove_column :subscriptions, :rows_processed
    remove_column :subscriptions, :queries_run
    remove_column :plans, :queries_per_month
    
    Plan.bootstrap.update({queries_per_month: 100, desc: "50 Thousand Rows per Month"})
    Plan.startup.update({rows_per_month: 30000000, desc: "30 Million Rows per Month" })
    Plan.elite.update({ rows_per_month: 300000000, desc: "300 Million Rows per Month" })
    Plan.business.update({ rows_per_month: 1000000000, desc: "1 Billion Rows per Month" })
  end
end
