class UpdatePricingPlansAndCustomSubscription < ActiveRecord::Migration[5.0]
  def up
    add_column :subscriptions, :is_custom, :boolean, default: false
    
    Plan.bootstrap.update({queries_per_month: 100, desc: "10 Thousand Rows and 100 Queries per Month"})
    Plan.startup.update({ queries_per_month: 500, rows_per_month: 2000000, desc: "2 Million Rows and 500 Queries per Month", amount: 1900 })
    Plan.create({name: "Big Data", stripe_id: "big_data", queries_per_month: 1500, rows_per_month: 10000000, amount: 10000,
      desc: "10 Million Rows and 1500 Queries per Month", period: 0, active: true })
    Plan.elite.update({ queries_per_month: 3500, rows_per_month: 150000000, desc: "150 Million Rows and 3500 Queries per Month", amount: 39000 })
    Plan.business.update({ queries_per_month: 10000, rows_per_month: 500000000, desc: "500 Million Rows and 10000 Queries per Month", amount: 70000 })
  end
  
  def down
    remove_column :subscriptions, :is_custom
    
    Plan.bootstrap.update({queries_per_month: 100, desc: "50 Thousand Rows and 100 Queries per Month"})
    Plan.startup.update({ queries_per_month: 1000, rows_per_month: 10000000, desc: "10 Million Rows and 1000 Queries per Month", amount: 10000  })
    Plan.big_data.destroy
    Plan.elite.update({ queries_per_month: -1, rows_per_month: 150000000, desc: "150 Million Rows and Unlimited Queries per Month", amount: 40000  })
    Plan.business.update({ queries_per_month: -1, rows_per_month: 500000000, desc: "500 Million Rows and Unlimited Queries per Month", amount: 60000  })
  end
end
