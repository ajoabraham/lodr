class CreateBucketLoaders < ActiveRecord::Migration[5.0]
  def change
    create_table :bucket_loaders do |t|
      
      t.string :name, null: false
      t.string :prefix
      t.string :file_filter
      
      t.boolean :includes_header, default: true
      t.string :delimiter, default: "AUTO"
      t.string :quote_char, default: "AUTO"
      
      t.integer :file_mapping_type
      t.boolean :load_new_files_only, default: true
      t.integer :load_type, default: 0
      t.string :target_table_prefix
      t.string :target_table_suffix
      
      t.integer :notify_type, default: 0
      t.text :notification_emails
      
      # none, once, hourly, daily, weekly, monthly, quarterly, yearly, file_change
      t.integer :schedule_type, null: false, default: 0, index: true
      t.text :schedule
            
      t.references :user, index: true
      t.references :s3_bucket
      t.references :database
      
      t.references :pre_sql, references: :scripts
      t.references :post_sql, references: :scripts
          
      t.timestamps
    end
  end
end
