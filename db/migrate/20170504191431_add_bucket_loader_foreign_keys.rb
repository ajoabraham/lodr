class AddBucketLoaderForeignKeys < ActiveRecord::Migration[5.0]
  def change
    add_column :loaders, :bucket_loader_id, :integer, index: true
    add_column :source_files, :bucket_loader_id, :integer, index: true
  end
end
