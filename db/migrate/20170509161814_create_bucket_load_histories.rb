class CreateBucketLoadHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :bucket_load_histories do |t|
      t.references :bucket_loader, index: true
      
      t.integer :load_job_ids, array: true, default: []
      # queued, prepping_files, running_loads, done, cancelled
      t.integer :status, default: 0
      
      t.integer :files_to_load, default: 0
      t.integer :succeeded_files, default: 0
      t.integer :failed_files, default: 0
      t.integer :rows_processed, limit: 8, default: 0
      t.integer :volume_processed, limit: 8, default: 0
      
      t.timestamp :prep_finished_at
      t.timestamp :finished_at
      
      t.text :failure_message
      t.timestamps
    end
  end
end
