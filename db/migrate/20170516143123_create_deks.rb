class CreateDeks < ActiveRecord::Migration[5.0]
  def change
    create_table :deks do |t|
      t.string :key, null: false
      t.boolean :primary, default: false
      
      t.timestamps
    end
    
    add_column :s3_buckets, :encrypted_access_key, :string
    add_column :s3_buckets, :encrypted_access_key_iv, :string
    add_column :s3_buckets, :dek_id, :integer
    add_column :databases, :encrypted_password, :string
    add_column :databases, :encrypted_password_iv, :string
    add_column :databases, :dek_id, :integer
    
    rename_column :databases, :password, :orig_password
    rename_column :s3_buckets, :access_key, :orig_access_key

    dek = Dek.generate!
    dek.promote!

    Database.find_each do |db|
      ts = db.test_connection_status
      er = db.test_error

      db.dek = dek
      db.password = db.orig_password
      db.save(validate: false)

      db.update_columns(test_connection_status: ts, test_error: er)
    end

    S3Bucket.find_each do |s3|
      s3.dek = dek
      s3.access_key = s3.orig_access_key
      s3.save(validate: false)
    end
    
    remove_column :s3_buckets, :orig_access_key, :string
    remove_column :databases, :orig_password, :string
  end
end
