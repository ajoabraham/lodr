class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name, null: false, default: "My Project"
      t.references :owner, references: :users, index: true, null: false
      
      t.string :time_zone, default: 'UTC'
      t.string :encrypted_api_token, index: true
      t.string :encrypted_api_token_iv, index: true
      t.integer :dek_id
            
      t.timestamps
    end
    
    add_reference :zip_files, :project, index: true
    add_reference :source_files, :project, index: true
    add_reference :s3_buckets, :project, index: true
    add_reference :loaders, :project, index: true
    add_reference :bucket_loaders, :project, index: true
    add_reference :load_jobs, :project, index: true
    add_reference :databases, :project, index: true
    add_reference :scripts, :project, index: true    
    
  end
end
