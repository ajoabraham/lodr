class CreateProjectUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :project_users do |t|
      t.references :project, index: true
      t.references :user, index: true
            
      t.timestamps
      
      t.index [:project_id,:user_id]
    end
    
    User.find_each do |u|
      p = Project.create(owner: u)
      p.users << u
      p.save
      
      u.databases.update_all(project_id: p.id)
      u.loaders.update_all(project_id: p.id)
      u.bucket_loaders.update_all(project_id: p.id)
      u.source_files.update_all(project_id: p.id)
      u.zip_files.update_all(project_id: p.id)
      u.scripts.update_all(project_id: p.id)
      u.load_jobs.update_all(project_id: p.id)
      u.s3_buckets.update_all(project_id: p.id)
    end
  end
end
