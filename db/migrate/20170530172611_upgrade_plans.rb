class UpgradePlans < ActiveRecord::Migration[5.0]
  def up
    Plan.bootstrap.update({rows_per_month: 50000, queries_per_month: 25, desc: "50 Thousand Rows Processed and 25 Queries Run per Month"})
    Plan.big_data.update({rows_per_month: 100000000, queries_per_month: 3500, desc: "100 Million Rows Processed and 3500 Queries per Month", amount: 90000 })
    
    Plan.where.not(stripe_id: ["bootstrap", "big_data"]).update_all(active: false)
  end
  
  def down
    Plan.bootstrap.update({rows_per_month: 10000, queries_per_month: 100, desc: "10 Thousand Rows Processed and 100 Queries Run per Month"})
    Plan.big_data.update({rows_per_month: 150000000, queries_per_month: 3500, desc: "150 Million Rows Processed and 3500 Queries per Month", amount: 10000 })
    Plan.update_all(active: true)
  end
end
