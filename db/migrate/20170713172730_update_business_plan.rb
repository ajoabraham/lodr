class UpdateBusinessPlan < ActiveRecord::Migration[5.0]
  def up
    p = Plan.business
    p.desc = "10 Million Rows per Month and 1000 Queries per Month"
    p.rows_per_month = 10000000
    p.queries_per_month = 1000
    p.amount = 29900
    p.active = true
    
    p.save
  end
  
  def down
    p = Plan.business
    p.active = false
    p.save
  end
end
