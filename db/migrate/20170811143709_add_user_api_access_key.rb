class AddUserApiAccessKey < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :api_access_key, :string, index: true
    User.all.each do |u|
      u.generate_token
      u.save
    end
  end
end
