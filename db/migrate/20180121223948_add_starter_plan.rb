class AddStarterPlan < ActiveRecord::Migration[5.0]
  def up
    p = Plan.startup
    p.desc = "1 Million Rows per Month and 150 Queries per Month"
    p.rows_per_month = 1000000
    p.queries_per_month = 150
    p.amount = 7900
    p.active = true
    
    p.save
  end
  
  def down
    p = Plan.startup
    p.active = false
    p.save
  end
end
