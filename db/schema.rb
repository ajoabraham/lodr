# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180121223948) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bucket_load_histories", force: :cascade do |t|
    t.integer  "bucket_loader_id"
    t.integer  "load_job_ids",     default: [],              array: true
    t.integer  "status",           default: 0
    t.integer  "files_to_load",    default: 0
    t.integer  "succeeded_files",  default: 0
    t.integer  "failed_files",     default: 0
    t.bigint   "rows_processed",   default: 0
    t.bigint   "volume_processed", default: 0
    t.datetime "prep_finished_at"
    t.datetime "finished_at"
    t.text     "failure_message"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["bucket_loader_id"], name: "index_bucket_load_histories_on_bucket_loader_id", using: :btree
  end

  create_table "bucket_loaders", force: :cascade do |t|
    t.string   "name",                                 null: false
    t.string   "prefix"
    t.string   "file_filter"
    t.boolean  "includes_header",     default: true
    t.string   "delimiter",           default: "AUTO"
    t.string   "quote_char",          default: "AUTO"
    t.integer  "file_mapping_type"
    t.boolean  "load_new_files_only", default: true
    t.integer  "load_type",           default: 0
    t.string   "target_table_prefix"
    t.string   "target_table_suffix"
    t.integer  "notify_type",         default: 0
    t.text     "notification_emails"
    t.integer  "schedule_type",       default: 0,      null: false
    t.text     "schedule"
    t.integer  "user_id"
    t.integer  "s3_bucket_id"
    t.integer  "database_id"
    t.integer  "pre_sql_id"
    t.integer  "post_sql_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "project_id"
    t.index ["database_id"], name: "index_bucket_loaders_on_database_id", using: :btree
    t.index ["post_sql_id"], name: "index_bucket_loaders_on_post_sql_id", using: :btree
    t.index ["pre_sql_id"], name: "index_bucket_loaders_on_pre_sql_id", using: :btree
    t.index ["project_id"], name: "index_bucket_loaders_on_project_id", using: :btree
    t.index ["s3_bucket_id"], name: "index_bucket_loaders_on_s3_bucket_id", using: :btree
    t.index ["schedule_type"], name: "index_bucket_loaders_on_schedule_type", using: :btree
    t.index ["user_id"], name: "index_bucket_loaders_on_user_id", using: :btree
  end

  create_table "databases", force: :cascade do |t|
    t.string   "name",                                   null: false
    t.string   "server",                                 null: false
    t.integer  "port",                   default: 5439,  null: false
    t.string   "db_name",                                null: false
    t.string   "user_name",                              null: false
    t.boolean  "use_ssl",                default: false, null: false
    t.string   "jdbc_url"
    t.json     "properties"
    t.integer  "test_connection_status", default: 2,     null: false
    t.string   "test_error"
    t.integer  "user_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "schema_status",          default: 0
    t.text     "schema_error"
    t.string   "encrypted_password"
    t.string   "encrypted_password_iv"
    t.integer  "dek_id"
    t.integer  "project_id"
    t.index ["name"], name: "index_databases_on_name", using: :btree
    t.index ["project_id"], name: "index_databases_on_project_id", using: :btree
    t.index ["user_id"], name: "index_databases_on_user_id", using: :btree
  end

  create_table "deks", force: :cascade do |t|
    t.string   "key",                        null: false
    t.boolean  "primary",    default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "lj_temp", id: false, force: :cascade do |t|
    t.integer  "id"
    t.integer  "loader_id"
    t.integer  "user_id"
    t.uuid     "job_id"
    t.json     "field_errors"
    t.integer  "status"
    t.bigint   "input_bytes"
    t.bigint   "input_rows"
    t.bigint   "total_rows_loaded"
    t.bigint   "inserted_rows"
    t.bigint   "updated_rows"
    t.bigint   "total_failed_rows"
    t.string   "info"
    t.text     "execution_errors"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "load_jobs", force: :cascade do |t|
    t.integer  "loader_id"
    t.integer  "user_id"
    t.string   "loader_name",       null: false
    t.uuid     "job_id",            null: false
    t.json     "field_errors"
    t.integer  "status",            null: false
    t.bigint   "input_bytes"
    t.bigint   "input_rows"
    t.bigint   "total_rows_loaded"
    t.bigint   "inserted_rows"
    t.bigint   "updated_rows"
    t.bigint   "total_failed_rows"
    t.string   "info"
    t.text     "execution_errors"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.datetime "lambda_done_at"
    t.string   "selected_file"
    t.boolean  "updated_sub"
    t.integer  "project_id"
    t.index ["job_id"], name: "index_load_jobs_on_job_id", using: :btree
    t.index ["loader_id"], name: "index_load_jobs_on_loader_id", using: :btree
    t.index ["project_id"], name: "index_load_jobs_on_project_id", using: :btree
    t.index ["user_id"], name: "index_load_jobs_on_user_id", using: :btree
  end

  create_table "loader_sources", force: :cascade do |t|
    t.integer  "loader_id",      null: false
    t.integer  "source_file_id", null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["loader_id", "source_file_id"], name: "index_loader_sources_on_loader_id_and_source_file_id", using: :btree
    t.index ["loader_id"], name: "index_loader_sources_on_loader_id", using: :btree
    t.index ["source_file_id"], name: "index_loader_sources_on_source_file_id", using: :btree
  end

  create_table "loaders", force: :cascade do |t|
    t.integer  "step",                  default: 0,     null: false
    t.boolean  "active",                default: false, null: false
    t.string   "name",                                  null: false
    t.string   "target_table_name"
    t.integer  "database_id"
    t.json     "column_settings"
    t.json     "transformations"
    t.string   "primary_keys",                                       array: true
    t.string   "sort_keys",                                          array: true
    t.integer  "distribution_style",    default: 1
    t.string   "distribution_key"
    t.json     "table_indexes"
    t.integer  "update_mode",           default: 0,     null: false
    t.string   "merge_keys",                                         array: true
    t.integer  "multi_file_mode"
    t.string   "multi_file_merge_keys",                              array: true
    t.text     "pre_sql"
    t.text     "post_sql"
    t.integer  "schedule_type",         default: 0,     null: false
    t.integer  "user_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.boolean  "notify",                default: true
    t.datetime "last_ran_at"
    t.text     "schedule"
    t.boolean  "use_file_prefix",       default: false
    t.string   "file_prefix"
    t.integer  "bucket_loader_id"
    t.integer  "project_id"
    t.index ["database_id"], name: "index_loaders_on_database_id", using: :btree
    t.index ["name"], name: "index_loaders_on_name", using: :btree
    t.index ["project_id"], name: "index_loaders_on_project_id", using: :btree
    t.index ["schedule_type"], name: "index_loaders_on_schedule_type", using: :btree
    t.index ["user_id"], name: "index_loaders_on_user_id", using: :btree
  end

  create_table "plans", force: :cascade do |t|
    t.string   "stripe_id",                        null: false
    t.string   "name",                             null: false
    t.string   "desc",                             null: false
    t.integer  "amount",                           null: false
    t.integer  "period",                           null: false
    t.bigint   "rows_per_month"
    t.boolean  "active",            default: true
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "queries_per_month"
    t.index ["stripe_id"], name: "index_plans_on_stripe_id", using: :btree
  end

  create_table "project_users", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id", "user_id"], name: "index_project_users_on_project_id_and_user_id", using: :btree
    t.index ["project_id"], name: "index_project_users_on_project_id", using: :btree
    t.index ["user_id"], name: "index_project_users_on_user_id", using: :btree
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name",                   default: "My Data Prep Project", null: false
    t.integer  "owner_id",                                                null: false
    t.string   "time_zone",              default: "UTC"
    t.string   "encrypted_api_token"
    t.string   "encrypted_api_token_iv"
    t.integer  "dek_id"
    t.datetime "created_at",                                              null: false
    t.datetime "updated_at",                                              null: false
    t.index ["encrypted_api_token"], name: "index_projects_on_encrypted_api_token", using: :btree
    t.index ["encrypted_api_token_iv"], name: "index_projects_on_encrypted_api_token_iv", using: :btree
    t.index ["owner_id"], name: "index_projects_on_owner_id", using: :btree
  end

  create_table "queries", force: :cascade do |t|
    t.integer  "status"
    t.text     "sql"
    t.text     "last_executed_query"
    t.string   "info"
    t.text     "query_error"
    t.json     "result_data"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.bigint   "total_rows"
    t.integer  "fetched_rows"
    t.integer  "script_id"
    t.integer  "user_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.boolean  "updated_sub"
    t.index ["script_id"], name: "index_queries_on_script_id", using: :btree
    t.index ["user_id"], name: "index_queries_on_user_id", using: :btree
  end

  create_table "s3_buckets", force: :cascade do |t|
    t.string   "bucket_name",             null: false
    t.string   "access_id",               null: false
    t.string   "region",                  null: false
    t.integer  "user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "encrypted_access_key"
    t.string   "encrypted_access_key_iv"
    t.integer  "dek_id"
    t.integer  "project_id"
    t.index ["bucket_name"], name: "index_s3_buckets_on_bucket_name", using: :btree
    t.index ["project_id"], name: "index_s3_buckets_on_project_id", using: :btree
    t.index ["user_id"], name: "index_s3_buckets_on_user_id", using: :btree
  end

  create_table "samples", force: :cascade do |t|
    t.json     "sample",         null: false
    t.integer  "source_file_id", null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["source_file_id"], name: "index_samples_on_source_file_id", using: :btree
  end

  create_table "scripts", force: :cascade do |t|
    t.string   "name",        null: false
    t.text     "content"
    t.string   "folder"
    t.integer  "database_id"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "project_id"
    t.index ["database_id"], name: "index_scripts_on_database_id", using: :btree
    t.index ["name"], name: "index_scripts_on_name", using: :btree
    t.index ["project_id"], name: "index_scripts_on_project_id", using: :btree
    t.index ["user_id"], name: "index_scripts_on_user_id", using: :btree
  end

  create_table "source_files", force: :cascade do |t|
    t.string   "name",                             null: false
    t.string   "file_type"
    t.bigint   "size"
    t.bigint   "rows"
    t.integer  "row_count_type"
    t.integer  "columns"
    t.string   "encoding"
    t.string   "delimiter"
    t.string   "quote_char"
    t.integer  "status"
    t.string   "processing_errors"
    t.json     "headers"
    t.integer  "sample_size",       default: 500
    t.boolean  "includes_header",   default: true, null: false
    t.datetime "uploaded_at"
    t.string   "loadable_type"
    t.integer  "loadable_id"
    t.integer  "user_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "bucket_loader_id"
    t.integer  "project_id"
    t.index ["loadable_type", "loadable_id"], name: "index_source_files_on_loadable_type_and_loadable_id", using: :btree
    t.index ["name"], name: "index_source_files_on_name", using: :btree
    t.index ["project_id"], name: "index_source_files_on_project_id", using: :btree
    t.index ["user_id"], name: "index_source_files_on_user_id", using: :btree
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "user_id",                                  null: false
    t.integer  "plan_id",                                  null: false
    t.integer  "previous_plan_id"
    t.integer  "last4"
    t.string   "card_brand"
    t.integer  "exp_month"
    t.integer  "exp_year"
    t.string   "status"
    t.string   "stripe_event"
    t.string   "stripe_sub_id"
    t.string   "stripe_customer_id"
    t.datetime "current_period_starts_at"
    t.datetime "current_period_ends_at"
    t.boolean  "cancel_at_period_end"
    t.datetime "canceled_at"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.datetime "reset_at"
    t.bigint   "rows_processed",           default: 0
    t.bigint   "queries_run",              default: 0
    t.boolean  "is_custom",                default: false
    t.index ["plan_id"], name: "index_subscriptions_on_plan_id", using: :btree
    t.index ["stripe_customer_id"], name: "index_subscriptions_on_stripe_customer_id", using: :btree
    t.index ["stripe_sub_id"], name: "index_subscriptions_on_stripe_sub_id", using: :btree
    t.index ["user_id"], name: "index_subscriptions_on_user_id", using: :btree
  end

  create_table "tables", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "schema"
    t.integer  "table_type"
    t.json     "columns"
    t.bigint   "table_size"
    t.bigint   "row_count"
    t.text     "view_def"
    t.integer  "schema_status"
    t.text     "schema_error"
    t.integer  "database_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["database_id"], name: "index_tables_on_database_id", using: :btree
    t.index ["name"], name: "index_tables_on_name", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "first_name",                             null: false
    t.string   "last_name",                              null: false
    t.string   "company"
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.string   "api_access_key"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "zip_files", force: :cascade do |t|
    t.string   "name",                      null: false
    t.bigint   "size"
    t.integer  "status",        default: 0, null: false
    t.text     "failed_reason"
    t.string   "loadable_type"
    t.integer  "loadable_id"
    t.integer  "user_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "project_id"
    t.index ["loadable_type", "loadable_id"], name: "index_zip_files_on_loadable_type_and_loadable_id", using: :btree
    t.index ["name"], name: "index_zip_files_on_name", using: :btree
    t.index ["project_id"], name: "index_zip_files_on_project_id", using: :btree
    t.index ["user_id"], name: "index_zip_files_on_user_id", using: :btree
  end

end
