namespace :lodr do
  desc "check if we can connect"
  task :check do  
    on roles(:all) do
      execute :hostname
      execute(:ls, '-ltra')
      execute :pwd
    end
  end
  
  desc "list environment variables"
  task :vars do
    on roles(:all) do
      execute :hostname
      execute(:rbenv, 'vars')
    end
  end
  
  desc "test internal api/xo access is working"
  task :api_canary do
    on roles(:all) do
      puts "good if there is no other output"
      execute :curl, "http://10.8.2.94/api/xo/canary"
      puts "request from outside should be blocked"
      execute :curl, "https://www.lodr.io/api/xo/canary"
    end
  end
  
  desc "tail log"
  task :log do
    on roles(:all) do
      within "/home/deploy/apps/lodr/current/log" do
        execute :tail, "-500 production.log"
      end
    end
  end
  
  desc 'Restart puma, nginx, delayed job'
  task :restart_all do
    on roles(:app,:background, :web), in: :sequence, wait: 5 do
      invoke 'puma:restart'
      invoke 'delayed_job:restart'
      invoke 'nginx:restart'
    end
  end
end

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end

namespace :deploy do
  
  desc "Setup and deploy nginx"
  task :nginx do
    on roles(:web), in: :sequence, wait: 5 do 
      execute "sudo mkdir -p /etc/nginx/sites-available"
      execute "sudo mkdir -p /etc/nginx/sites-enabled"
      execute "sudo chmod -R g+xwr /var/lib/nginx"
      invoke "nginx:stop"
      invoke "nginx:site:remove"
      invoke "nginx:site:add"
      invoke "nginx:site:enable"
      invoke "nginx:start"
    end
  end
  
  
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/#{fetch(:branch)}`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  before :starting,     :check_revision
  after  :finishing,    :compile_assets  
  after  :finishing,    :cleanup
  after  :finishing,    :restart # puma
  after  :finishing,    :nginx
end

namespace :delayed_job do

  desc "Install Deployed Job executable if needed"
  task :install do
    on roles(delayed_job_roles) do |host|
      within release_path do
        # Only install if not already present
        unless test("[ -f #{release_path}/#{delayed_job_bin} ]")
          with rails_env: fetch(:rails_env) do
            execute :bundle, :exec, :rails, :generate, :delayed_job
          end
        end
      end
    end
  end

  before :start, :install
  before :restart, :install

end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma
