namespace :encryption do
  desc "Rotate current data encryption key with new key and reencrypt data. Affects s3 buckets and databases"
  task :rotate_dek => :environment do
    puts "\ncreating new dek"
    dek = Dek.generate!
    dek.promote!
    
    puts "reencrypting databases"
    Database.find_each do |db|
      db.reencrypt!(dek)
      puts "#{db.name} done.."
    end

    puts "reencrypting s3 buckets"
    S3Bucket.find_each do |s3|
      s3.reencrypt!(dek)
      puts "#{s3.bucket_name} done.."
    end
  end
  
  desc "Delete unused data encryption keys (dek)"
  task :destroy_unused => :environment do
    used = Database.pluck(:dek_id) + S3Bucket.pluck(:dek_id)
    Dek.where.not( id: used.uniq).delete_all
  end
end