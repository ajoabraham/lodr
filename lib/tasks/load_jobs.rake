namespace :load_jobs do
  desc "Cancels all load jobs that have exceeded the max runtime of 8 hours. will not call loader.cancel"
  task cancel_long_running_jobs: :environment do
    LoadJob.where('started_at <= ?', 4.hours.ago)
      .where(status: [:running, :queued])
      .update_all(status: :cancelled, 
      info: "job was cancelled", finished_at: Time.now)
  end
  
  desc "Cancels all load jobs by calling cancel on each active one."
  task cancel_all: :environment do
    LoadJob.where(status: [:running, :queued]).each do |lj|
      lj.loader.cancel
    end
  end
  
  desc "Reque all scheduled jobs. Important when schedule jobs may have been wiped out."
  task reque: :environment do 
    LoadJob.where(status: :scheduled).destroy_all
    
    Loader.where.not(schedule_type: :schedule_none).each do |ld|
      if ld.loader_is_valid? && ld.scheduled? && ld.schedule_valid? && ld.next_occurrence
                    
          ld.update_attributes({active: true}) unless ld.active
    
          @load_job = LoadJob.new(loader: ld, user: ld.user, status: :scheduled, project: ld.project)
          @load_job.save

          RunLoadJob.set(wait_until: ld.next_occurrence).perform_later @load_job
      end
    end
    
  end
end
