namespace :lodr do
  desc "Resend project invites to non activated"
  task reinvite: :environment do
    users = User.where(first_name: "__missing__")
    
    users.each do |u|
      temp_pass = Devise.friendly_token.first(8)      
      u.password = temp_pass
      u.password_confirmation = temp_pass
      u.save
      p = u.projects.first
      UserMailer.reinvite_team_member(u, p.owner_id, p.id, temp_pass).deliver_later  
      puts "inviting user #{u.id} for project owner #{p.owner.full_name} again.."
    end
     
  end # end reinvite task

end