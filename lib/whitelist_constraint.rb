# Not used since we can deny
# access from nginx more efficiently

class WhitelistConstraint
  def initialize
    @ips = []
    @ips << NetAddr::CIDR.create('10.8.3.0/24')
    @ips << NetAddr::CIDR.create('10.8.2.0/24')
  end

  def matches?(request)
    Rails.logger.debug "api remote ip: #{request.remote_ip}"
    Rails.logger.debug "forwarded for: #{request.headers['HTTP_X_FORWARDED_FOR']}"
    
    req_ip = request.headers['HTTP_X_FORWARDED_FOR'] || request.remote_ip
    
    if Rails.env.production?
      valid = @ips.select {|cidr| cidr.contains?(req_ip) }
      Rails.logger.info "api request is valid? #{!valid.empty?}"
      
      !valid.empty?
    else
      true
    end
  end
   
 end