require 'test_helper'

class Api::DbRequestsControllerTest < ActionDispatch::IntegrationTest

    
  test "can update database schema" do
    db = databases(:one)
    assert_equal 2, db.tables.count, "should have two tables"
    
    r = { database: { schema_status: "SCHEMA_READY", 
          tables_attributes: [{name: "somebody_table", table_type: "TABLE", table_size: 1113, schema: "public", 
          columns: [ {name: "a_col", data_type: "varchar"},
                     {name: "a_col2", data_type: "varchar"} ] }] } 
         }
    put api_xo_db_schema_path(databases(:one)), params: r
    assert_response :ok
    
    db.reload
    assert_equal 1, db.tables.count, "should have one table"
    
    assert_equal true, db.tables.all? {|t| t.schema_ready? }, "all tables should be marked as schema_ready"
  end
  
  test "there were no tables" do
    db = databases(:one)
    assert_equal 2, db.tables.count, "should have two tables"
    
    r = { database: { schema_status: "SCHEMA_READY"} }
    put api_xo_db_schema_path(databases(:one)), params: r
    
    assert_response :ok
    
    db.reload
    assert_equal 0, db.tables.count, "should have one table"
  end
  
  test "update existing table" do
    t = tables(:one)
    assert_equal 1, t.columns.size, "only one column in table now"
    
    r = {      
      table: {
        schema_status: "SCHEMA_READY",
        name: "somebody_table", table_type: "TABLE", table_size: 1113, schema: "public", 
        columns: [ {name: "a_col", data_type: "varchar"},
                   {name: "a_col2", data_type: "varchar"} ] 
         }
    }
    put api_xo_db_table_path(t), params: r
    
    assert_response :ok
    
    t.reload
    assert_equal 2, t.columns.size, "table now has two columns"
    assert t.schema_ready?
  end 
  
  test "table should be deleted when node is nil" do
    t = tables(:one)
    
    r = {
      schema_status: "SCHEMA_READY",
      table: nil
    }
    put api_xo_db_table_path(t), params: r
    
    assert_response :ok
    
    assert_raise ActiveRecord::RecordNotFound do 
      tt = Table.find t.id
    end
  end 
  
  test "execute query with results" do
    s = Script.create(name: "my new script", user: users(:one), project: projects(:one))
    q = Query.create(script: s, sql: "select * from table", user: users(:one))
    
    assert q.valid?
    assert q.result_data.nil?, "there should be no result data"
    
    r = { execute_queries: {
          result_data: {columns: [ {name: "a_col", data_type: "int"},
                                   {name: "a_col2", data_type: "varchar"},
                                   {name: "a_col3", data_type: "decimal", precision: 10} ],
                        data: [ [1,"asf",55.2], [77,"asdf asdf",105.22] ] },
          status: "COMPLETED", total_rows: 5242342, fetched_rows: 2000,
          finshed_at: Time.now
          } }
    put api_xo_db_query_path(q), params: r
    assert_response :ok
    
    q = Query.find q.id
    assert_not q.result_data.nil?, "there should be data now"
  end
  
end
