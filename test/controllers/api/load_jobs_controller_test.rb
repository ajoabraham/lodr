require 'test_helper'

class Api::LoadJobsControllerTest < ActionDispatch::IntegrationTest
  
  test "field errors are allowed to post" do
    lj = load_jobs(:running)
    p = {"load_job"=>{"job_id"=>"f0fe8cce-81de-40ec-95fa-1c94529ea06e", 
      "info"=>"Import job finished successfully.", "status"=>"SUCCEEDED", 
      "total_rows_loaded"=>42, "input_rows"=>44, "updated_rows"=>0, "inserted_rows"=>42, 
      "execution_errors"=>nil, "total_failed_rows"=>2, "finished_at"=>"2016-12-21T22:30:36.795Z", 
      "lambda_done_at"=>"2016-12-21T22:30:33.414Z", "input_bytes"=>4702, 
      "field_errors"=>[{"columnName"=>"Date of Service", "failedRows"=>42}]}, "id"=>"f0fe8cce-81de-40ec-95fa-1c94529ea06e"}
    
    put api_xo_load_job_path(lj), params: p
    assert_response :success
    
    lj = LoadJob.find lj.id
    assert_equal "succeeded", lj.status, "should have succeeded"
    assert_not lj.field_errors.nil?, "field errors should be present"
    
    assert lj.field_errors.first.has_key? "columnName"
  end
  
  test "bucket loader updates" do
    
    lj = load_jobs(:running_bucket_loader)
    lj2 = load_jobs(:running_bucket_loader2)
    bh = BucketLoadHistory.create(bucket_loader: lj.loader.bucket_loader, 
                            load_job_ids:[lj.id,lj2.id], files_to_load: 2)
    
    p = {"load_job"=>{"job_id"=>"f0fe8cce-81de-40ec-95fa-1c94529ea06e", 
      "info"=>"Import job finished successfully.", "status"=>"FAILED", 
      "total_rows_loaded"=>42, "input_rows"=>44, "updated_rows"=>0, "inserted_rows"=>42, 
      "execution_errors"=>nil, "total_failed_rows"=>2, "finished_at"=>"2016-12-21T22:30:36.795Z", 
      "lambda_done_at"=>"2016-12-21T22:30:33.414Z", "input_bytes"=>4702, 
      "field_errors"=>[{"columnName"=>"Date of Service", "failedRows"=>42}]}, "id"=>"f0fe8cce-81de-40ec-95fa-1c94529ea06e"}
    
    put api_xo_load_job_path(lj), params: p
    assert_response :success
    
    lj = LoadJob.find lj.id    
    bh = lj.loader.bucket_loader.last_run
    assert bh.running?
    assert_equal "queued", bh.status, "should be running loads still"
    
    bh.running_loads! # since we aren't calling run_load have to manually set
    put api_xo_load_job_path(lj2), params: p
    assert_response :success
    
    lj = LoadJob.find lj2.id
    
    bh = lj.loader.bucket_loader.last_run
    assert_not bh.jobs_running?
    
    assert bh.failed?, "should have failed"
  end
  
  test "bucket loader ran separte from bucketloader" do
    lj = load_jobs(:running_bucket_loader)
    lj2 = load_jobs(:running_bucket_loader2)
    bh = BucketLoadHistory.create(bucket_loader: lj.loader.bucket_loader, 
                            load_job_ids:[lj.id,lj2.id], files_to_load: 2, status: :done)
    
    p = {"load_job"=>{"job_id"=>"f0fe8cce-81de-40ec-95fa-1c94529ea06e", 
      "info"=>"Import job finished successfully.", "status"=>"FAILED", 
      "total_rows_loaded"=>42, "input_rows"=>44, "updated_rows"=>0, "inserted_rows"=>42, 
      "execution_errors"=>nil, "total_failed_rows"=>2, "finished_at"=>"2016-12-21T22:30:36.795Z", 
      "lambda_done_at"=>"2016-12-21T22:30:33.414Z", "input_bytes"=>4702, 
      "field_errors"=>[{"columnName"=>"Date of Service", "failedRows"=>42}]}, "id"=>"f0fe8cce-81de-40ec-95fa-1c94529ea06e"}
    
    
    job = mock()
    job.stubs(:deliver_later).returns(true)
    NotificationsMailer.expects(:bucket_load_finished).returns(job).never
    
    put api_xo_load_job_path(lj), params: p
    assert_response :success
    
    bh = lj.loader.bucket_loader.last_run
    
    assert bh.done?, "Should still be done"
  end
end