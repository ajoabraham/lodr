require 'test_helper'

class Api::V1::JobsControllerTest < ActionDispatch::IntegrationTest
  def get_hdrs(u = users(:one), p = nil)
    u.generate_token
    u.save
    
    p = p || u.projects.first
    p.save
    
    { 'Authorization' => ActionController::HttpAuthentication::Basic.encode_credentials(u.api_access_key, p.api_token) }
  end
  
  test "basic job queries" do
    get api_v1_jobs_path, headers: get_hdrs
    assert_response :success
    
    res = JSON.parse response.body
    u = users(:one)
    assert u.projects.first.load_jobs.count == res.count
    
    get api_v1_jobs_path
    assert_response :unauthorized
    
    get api_v1_job_path(res[0]["id"] + 2340902393), headers: get_hdrs
    assert_response :not_found
    
  end
  
  test "start and stop job" do
    u = users(:one)
    
    lj = load_jobs(:running)
    get api_v1_job_path(lj.id), headers: get_hdrs(users(:custom))
    assert_response :not_found        
    
    delete api_v1_job_path(lj.id), headers: get_hdrs
    assert_response :success
    
    lj.reload
    assert_equal lj.status, "cancelled"
    
    post api_v1_job_path(u.loaders.first)+"/run", headers: get_hdrs
    assert_response :precondition_failed
    
    s = SourceFile.last
    s.headers = [{"columnName" => "yoyuo yo", "dataType" => "TEXT"}].to_json
    s.save
    l = Loader.new(user: u, project: u.projects.first)
    l.source_files << s
    l.database = Database.first
    l.transformations = nil 
    l.column_settings = l.source_files.first.headers
    l.step = :summary
    l.target_table_name = "test"
    l.name = "name"
    assert l.save
    
    post api_v1_job_path(l)+"/run", headers: get_hdrs(l.user)
    assert_response :success
  end
end
