require 'test_helper'

class Api::WebhooksControllerTest < ActionDispatch::IntegrationTest
    
  test "post something to stripe" do
    # TODO need to mock these things before testing
   
    # Stripe::Event.stubs(:retrieve).returns(true)
 #    Stripe::Event::Data::Object.any_instance
 #
 #    Subscription.any_instance.stubs(:find_by_stripe_customer_id).returns(subscriptions(:two))
 #    Stripe::Subscription.stubs(:retrieve).returns({status: "ok"})
 #    post api_hooks_stripe_path, params: {id: "sfasdfa"}
 #    assert_response :success
  end
  
  test "reset usage number after payment succeeded" do
    # event.data.object.customer
    
    em = mock()
    em.stubs(:type).returns("invoice.payment_succeeded")
    em.stubs(:data).returns(stub(:object => stub( :customer => "stripe_1_cust_id" ) ))
    
    Stripe::Event.stubs(:retrieve).returns(em)
    sub = Subscription.find_by_stripe_customer_id "stripe_1_cust_id"
    Subscription.stubs(:find_by_stripe_customer_id).returns(sub)
    
    sb = mock()
    sb.stubs(:status).returns("Active")
    sb.stubs(:current_period_start).returns(10.days.ago.to_i)
    sb.stubs(:current_period_end).returns(19.days.from_now.to_i)
    
    Stripe::Subscription.expects(:retrieve).with(sub.stripe_sub_id).returns(sb)
      
    assert_equal 0, sub.rows_remaining, "no more rows remaining"
    assert_equal 0, sub.queries_remaining, "no more rows remaining"
    
    p = {id: "abracadabra"}
    post api_hooks_stripe_path, params: p
    assert_response :success
    
    assert_equal sub.plan.rows_per_month, sub.rows_remaining, "should have all rows"
    assert_equal sub.plan.queries_per_month, sub.queries_remaining, "should have all quries"
  end
  
  test "status unpaid" do
    # event.data.object.customer
    
    em = mock()
    em.stubs(:type).returns("invoice.payment_failed")
    em.stubs(:data).returns(stub(:object => stub( :customer => "stripe_1_cust_id" ) ))
    
    Stripe::Event.stubs(:retrieve).returns(em)
    sub = Subscription.find_by_stripe_customer_id "stripe_1_cust_id"
    Subscription.stubs(:find_by_stripe_customer_id).returns(sub)
    
    sb = mock()
    sb.stubs(:status).returns("unpaid")
    sb.stubs(:current_period_start).returns(10.days.ago.to_i)
    sb.stubs(:current_period_end).returns(19.days.from_now.to_i)
    
    Stripe::Subscription.expects(:retrieve).with(sub.stripe_sub_id).returns(sb)
      
    assert_equal 0, sub.rows_remaining, "no more rows remaining"
    assert_equal 0, sub.queries_remaining, "no more rows remaining"
    
    p = {id: "abracadabra"}
    post api_hooks_stripe_path, params: p
    assert_response :success
    
    assert_equal sub.plan.id, Plan.bootstrap.id
    assert_equal sub.plan.queries_per_month, sub.queries_remaining, "should have all quries"
  end
end