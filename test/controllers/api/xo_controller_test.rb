require 'test_helper'

class Api::XoControllerTest < ActionDispatch::IntegrationTest
  setup do
      NotificationsMailer.expects(:unzip_done).returns(stub(deliver_later: true)).at_least_once
  end
    
  test "can update zip file with failure notice" do

    z = zip_files(:unzipping)
    p = {zip_file: {status: "failed",
          failed_reason: "bad zip",
          zip_files: [{name: "my unzipped file.csv", size: 23123}]
        }}

    assert_no_difference 'SourceFile.count', 'no new files should be added' do
      patch api_xo_zip_file_path(z), params: p
    end
    assert_response :no_content, "we should send a no content success back"
    zz = ZipFile.find z.id

    assert zz.failed?, "should be marked as failed: #{zz.status}"
    assert_equal "bad zip", zz.failed_reason, "failed reason should be recorded"
  end

  test "mark as failure when there is no content" do
    z = zip_files(:unzipping2)
    p = {zip_file: {status: "succeeded",
          zip_files: []
        }}

    assert_no_difference 'SourceFile.count', 'no new files should be added' do
      patch api_xo_zip_file_path(z), params: p
    end
    assert_response :no_content, "we should send a no content success back"
    z.reload

    assert z.failed?, "should be failed with no content"
    assert_equal "Zip had 0 valid files", z.failed_reason
  end

  test "success with new files added" do
    z = zip_files(:unzipping2)
    p = {zip_file: {status: "succeeded",
          zip_files: [{name: "mysomefile90.csv", size: 123123},
            {name: "mysomefile29234.csv", size: 53123}
          ]
        }}


    SourceFile.any_instance.stubs(:profile).returns(true)
    SourceFile.any_instance.stubs(:generate_settings_and_sample).returns(true)
    assert_difference 'SourceFile.count', +2, 'no new files should be added' do
      patch api_xo_zip_file_path(z), params: p
    end
    assert_response :no_content, "we should send a no content success back"
    z.reload

    assert z.succeeded?, "should have succeeded"
    assert_equal "", z.failed_reason
  end

  test "success with uppercase" do
    z = zip_files(:unzipping2)
    p = {zip_file: {status: "SUCCEEDED",
          zip_files: [{name: "mysomefile90.csv", size: 123123},
            {name: "mysomefile29234.csv", size: 53123}
          ]
        }}

    SourceFile.any_instance.stubs(:profile).returns(true)
    SourceFile.any_instance.stubs(:generate_settings_and_sample).returns(true)
    assert_difference 'SourceFile.count', +2, 'no new files should be added' do
      patch api_xo_zip_file_path(z), params: p
    end
    assert_response :no_content, "we should send a no content success back"
    z.reload

    assert z.succeeded?, "should have succeeded"
    assert_equal "", z.failed_reason
  end

  test "success with file from user s3 bucket" do
    z = zip_files(:user_s3)

    assert z.loadable, "Should have a loadable"
    p = {zip_file: {status: "SUCCEEDED",
          zip_files: [{name: "mysomefile90.csv", size: 123123, object_key: "my_path/x/mysomefile90_1x39.csv"},
            {name: "mysomefile29234.csv", size: 53123, object_key: "my_path/x/mysomefile29234_2x39.csv"}
          ]
        }}

    SourceFile.any_instance.stubs(:profile).returns(true)
    SourceFile.any_instance.stubs(:generate_settings_and_sample).returns(true)
    assert_difference 'SourceFile.count', +2, '2 new files should be added' do
      patch api_xo_zip_file_path(z), params: p
    end
    assert_response :no_content, "we should send a no content success back"
    z.reload

    assert z.succeeded?, "should have succeeded"
    assert_equal "", z.failed_reason

    s = SourceFile.find_by_name "my_path/x/mysomefile29234_2x39.csv"
    assert s.loadable_id == z.loadable_id, "Should have same loadable as zip"
  end
  
  test "unzip no write access bucket" do
    z = zip_files(:user_s3)
    assert z.loadable, "Should have a loadable"
    p = {zip_file: {status: "SUCCEEDED",
          zip_files: [{name: "mysomefile90_nowrite.csv", size: 123123, object_key: "my_path/x/mysomefile90_nowrite.csv"},
            {name: "mysomefile29234_nowrite.csv", size: 53123, object_key: "my_path/x/mysomefile29234_nowrite.csv"}
            ], dest_bucket_name: Rails.application.secrets[:s3_upload_bucket]
        }}
    
    SourceFile.any_instance.stubs(:profile).returns(true)
    SourceFile.any_instance.stubs(:generate_settings_and_sample).returns(true)
    assert_difference 'SourceFile.count', +2, '2 new files should be added' do
      patch api_xo_zip_file_path(z), params: p
    end
    assert_response :no_content, "we should send a no content success back"
    z.reload
    
    assert z.succeeded?, "should have succeeded"
    assert_equal "", z.failed_reason
    
    s = SourceFile.find_by_name "mysomefile29234_nowrite.csv"
    assert s, "should have found sourcefile with clean name"
    assert s.loadable.nil? , "The file should not be linked to a loadable since its in the user uploads bucket"
  end
  
end
