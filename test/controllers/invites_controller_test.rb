require 'test_helper'

class InvitesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers  
  
  test "should get new" do
    sign_in(users(:one))
    
    get new_project_invite_url(projects(:one))
    assert_response :success
  end


end
