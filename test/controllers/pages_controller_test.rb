require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers  
  
  test "request to non-existent page should throw error" do
    assert_raise ActionController::RoutingError do 
      get '/simon-says'
    end
  end
  
  test "redirect to app main path" do
    stubs(:current_project).returns("yo")
    sign_in(users(:one))
    
    get '/'
    assert_redirected_to dashboard_path, "route root to user dash"
    
    get '/privacy'
    assert_response :ok, "should still be able to get static content"
  end
end
