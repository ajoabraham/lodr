require 'test_helper'

class BucketLoaderTest < ActiveSupport::TestCase
  test "whole object key to table nane" do
    b = BucketLoader.new
    b.file_mapping_type = :whole_object
    
    res = b.mode_to_target_table("1hello_+][=world-/files/2016/07$.csv.gz")
    assert "l_hello_world_files_2016_07" == res
    
    res = b.mode_to_target_table("$1121_yodelle-asdf--asdf//a924723/)(*&^%)_hello.csv-2015512.gz")
    assert "l_1121_yodelle_asdf_asdf_a924723_hello" == res
    
    res = b.mode_to_target_table("$1121_yodelle-asdf--asdf//a924723/)(*&^%)_hello-923939393")
    assert "l_1121_yodelle_asdf_asdf_a924723_hello_923939393" == res
    
    res = b.mode_to_target_table("$1121_yodelle-asdf--asdf//a924723/)(*&^%)_hello-923939393_wrasfasdfasdfasdfajl;jaoewerweiruwoe wweroweruoweurwer weroweurweiowrwerw werwerwierwe weruowieruwoeu werouweoiruwioeruwe weoruweoiurwueo weruoweiuroweiuoiweuorweioursjflasdjfjalsj")
    
    assert res.length <= 127
    
  end
  
  test "file mode translations" do
    b = BucketLoader.new
    b.file_mapping_type = :root_level    
    res = b.mode_to_target_table("1hello_+][=world-/files/2016/07$.csv.gz")
    assert "l_hello_world" == res
    
    res = b.mode_to_target_table("//1hello_+][=world-/files/2016/07$.csv.gz")
    assert "l_hello_world" == res
    
    b.file_mapping_type = :level_2   
    res = b.mode_to_target_table("1hello_+][=world-/files/2016/07$.csv.gz")
    assert "l_hello_world_files" == res
    
    b.file_mapping_type = :level_3
    res = b.mode_to_target_table("1hello_+][=world-/files/2016/07$.csv.gz")
    assert "l_hello_world_files_2016" == res
    
    b.file_mapping_type = :level_4
    res = b.mode_to_target_table("1hello_+][=world-/files/2016/07$.csv.gz")
    assert "l_hello_world_files_2016_07" == res
    
    b.file_mapping_type = :level_5
    res = b.mode_to_target_table("1hello_+][=world-/files/2016/07$/9000-log.csv.gz")
    assert "l_hello_world_files_2016_07_9000_log" == res
    
    res = b.mode_to_target_table("1hello_+][=world-/files/2016/07.csv.gz")
    assert "l_hello_world_files_2016_07" == res, "too short situation"
    
    res = b.mode_to_target_table("1hello_+][=world-youyo.csv.gz")
    assert "l_hello_world_youyo" == res, "too short situation"
  end
  
  test "run load adds new s3 files" do
    DataSampleProfileJob.stubs(:perform_later).returns(true)
    RunLoadJob.stubs(:perform_later).returns(true)
        
    response = mock()
    response.stubs(:is_truncated, false)
    contents = [
      {"key" => "some_file_key/here/something.log", "size" => 23412},
      {"key" => "some_file_key/here/something2.log", "size" => 23412}
    ]
    S3Bucket.any_instance.stubs(:response).returns(response)
    S3Bucket.any_instance.stubs(:contents).returns(contents)
    S3Bucket.any_instance.stubs(:valid?).returns(true)
    SourceFile.any_instance.stubs(:profile).returns(true)
    SourceFile.any_instance.stubs(:generate_settings_and_sample).returns(true)
    
    columns = [
      {"columnName" => "yoyo", "dataType" => "boolean"},
      {"columnName" => "yoyo2", "dataType" => "boolean"},
      {"columnName" => "boo", "dataType" => "boolean"}
    ]
    SourceFile.any_instance.stubs(:headers).returns(columns.to_json)
    
    b = BucketLoader.new
    b.file_mapping_type = :whole_object
        
    assert_raises(ArgumentError){ b.run_load }
  
    b.s3_bucket = s3_buckets(:one)
    b.database = databases(:one)
    b.user = users(:one)
    b.project = projects(:one)
    b.name = "my test bucket loader"
    assert b.save, "loader should be saved now"
    
    bh = BucketLoadHistory.create(bucket_loader: b)
    
    sf_count = SourceFile.count
    ld_count = Loader.count
    lj_count = LoadJob.count
    
    assert b.run_load, "should run load on valid bucket"
    
    assert sf_count+2 == SourceFile.count, "two new source fiels should be added"  
    assert ld_count+2 == Loader.count, "two new loaders should be added"  
    assert lj_count+2 == LoadJob.count, "two new load jobs should be added"
    assert b.last_run.files_to_load == 2, "should have 2 files to load"    
    
    sf_count = SourceFile.count
    ld_count = Loader.count
    lj_count = LoadJob.count
    
    # there should not be any new additional objects
    assert b.run_load, "should run load on valid bucket"
    assert b.source_files.first.includes_header
    
    assert sf_count == SourceFile.count, "0 new source fiels should be added"  
    assert ld_count == Loader.count, "0 new loaders should be added"  
    assert lj_count == LoadJob.count, "0 new load jobs should be added"
    
    b.load_new_files_only = false
    b.save
    
    bh = BucketLoadHistory.create(bucket_loader: b)
    b = BucketLoader.find b.id
    b.load_jobs.where(status: [:queued, :running]).update_all(status: :cancelled)
    assert b.run_load, "should run load on valid bucket"
    
    assert sf_count == SourceFile.count, "0 new source fiels should be added"  
    assert ld_count == Loader.count, "0 new loaders should be added"  
    assert lj_count+2 == LoadJob.count, "2 new load jobs should be added"
    
    assert b.last_run.running_loads?, "should be in running load phase"
    assert_equal b.last_run.files_to_load, 2, "should have 2 files to load"    
  end
  
  test "lodr is configured properly" do
    DataSampleProfileJob.stubs(:perform_later).returns(true)
    RunLoadJob.stubs(:perform_later).returns(true)
        
    response = mock()
    response.stubs(:is_truncated, false)
    contents = [
      {"key" => "some_file_key/here/something.log", "size" => 23412},
      {"key" => "some_file_key/here/something2.log", "size" => 23412},
      {"key" => "something3.csv", "size" => 53412}
    ]
    S3Bucket.any_instance.stubs(:response).returns(response)
    S3Bucket.any_instance.stubs(:contents).returns(contents)
    S3Bucket.any_instance.stubs(:valid?).returns(true)
    SourceFile.any_instance.stubs(:profile).returns(true)
    SourceFile.any_instance.stubs(:generate_settings_and_sample).returns(true)
    
    columns = [
      {"columnName" => "yoyo", "dataType" => "boolean"},
      {"columnName" => "yoyo2", "dataType" => "boolean"},
      {"columnName" => "boo", "dataType" => "boolean"}
    ]
    SourceFile.any_instance.stubs(:headers).returns(columns.to_json)
    
    b = bucket_loaders(:config_test)
    b.target_table_prefix = nil
    assert_equal "something3_suffix", b.mode_to_target_table("something3.csv"), "no prefix test"
    b.reload
    
    bh = BucketLoadHistory.create(bucket_loader: b)    
    assert b.run_load, "should run load on valid bucket"
    
    bh.reload
    b.reload
    ld = b.loaders.last  
    assert_equal "prefix_something3_suffix", ld.target_table_name, "should add table prefix and suffix"
        
    # assert_equal scripts(:pre).content, b.loaders.first.pre_sql, "pre sql content should be present"
    # assert b.loaders.first.post_sql.blank?
    #
    # mid = LoadJob.find(bh.reload.load_job_ids[1]).loader
    # assert mid.pre_sql.blank?
    # assert mid.post_sql.blank?
    #
    # assert ld.pre_sql.blank?
    # assert_equal scripts(:post).content, ld.post_sql, "post sql content should be present"
    #
    # b.update_attributes(pre_sql: nil, post_sql: nil)
    # bh = BucketLoadHistory.create(bucket_loader: b)
    # assert b.run_load, "should run load on valid bucket"
    #
    # b.reload
    # b.loaders.any?{ |l| puts "#{l.name}= #{l.pre_sql} : #{l.post_sql}" }
    # assert_not b.loaders.any?{ |l| l.pre_sql || l.post_sql }, "no pre or post sql should be present"
  end
end
