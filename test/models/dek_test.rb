require 'test_helper'

class DekTest < ActiveSupport::TestCase
  test "dek promotion" do
    dek = Dek.generate!
    dek.promote!
    
    assert Dek.primary.key == dek.key
  end
end
