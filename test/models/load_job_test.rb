require 'test_helper'

class LoadJobTest < ActiveSupport::TestCase
  
  test "on save updates rows processed" do
    u = users(:one)
    l = u.load_jobs.where(status: :succeeded).first
    
    assert l.input_rows > 0, "load jobs has rows"
    l.loader = loaders(:one)
    assert l.save
    
    assert u.subscription.rows_processed > 0, "load job save should have updated rows processed"
  end 
  
end
