require 'test_helper'

class LoaderTest < ActiveSupport::TestCase
  test "validates summary" do
      u = User.new
      l = Loader.new(user: u, target_table_name: "bla", name: "blkdsf", update_mode: :mode_replace,database: databases(:one), project: projects(:one))
      l.step = :summary
      assert l.valid?, "should be valid since we only check basic things on the default valid call"
  end
  
  test "validates summary stage loader is not valid" do
      u = User.new
      l = Loader.new(user: u, target_table_name: "bla", name: "blkdsf", update_mode: :mode_merge, project: projects(:one))
      l.step = :summary
      l.loader_is_valid?
      assert_not l.loader_is_valid?, "should not be valid"
  end
end
