require 'test_helper'

class PlanTest < ActiveSupport::TestCase
  test "should not create plan with same stripe_id" do
    p = Plan.create stripe_id: "bootstrap", amount: 0, rows_per_month: 5000000, period: 0
    assert_not p.save, "stripe_id must be unique"
    
    assert p.errors.full_messages.include?("Stripe has already been taken"), "stripe error message should be there"
  end
  
  test "find by stripe id shortcut" do
    p = Plan.bootstrap
    assert_equal p.stripe_id, "bootstrap"
    
    p = Plan.startup
    assert_equal p.stripe_id, "startup"
    
    p = Plan.elite
    assert_equal p.stripe_id, "elite"
    
    p = Plan.business
    assert_equal p.stripe_id, "business"
    
    p = Plan.enterprise
    assert_equal p.stripe_id, "enterprise"
    
    p = Plan.big_data
    assert_equal p.stripe_id, "big_data"
  end
end
