require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  test "before destroy validate user and project count" do
    project = projects(:one)
    assert_equal 1, project.owner.projects.count, "Should only have one project to make in order to test this case."
  end
  
  test "new user should create project and add membership" do
    u = User.create first_name: "yo", last_name: "yoyo", email: "blue@blue.com",
                    password: "yoyoyoyo", password_confirmation: "yoyoyoyo"
    assert u.valid?
    assert_equal 1, u.projects.count, "There should be one default project"
    
    p = Project.find u.projects.first.id
    assert p.users.include?(u)
  end
  
  test "destroy user kilss all projects" do
    
    SourceFile.any_instance.stubs(:delete_s3_object).returns(true)
    ZipFile.any_instance.stubs(:delete_s3_object).returns(true)
    Subscription.any_instance.stubs(:cancel).returns(true)
    project = projects(:one)
    user = project.owner
    
    user.destroy
        
    assert Project.where(owner_id: user.id).count == 0, "The users projects that he owned should be deleted"
  end
  
  test "unauthorized project association" do
    s = loaders(:one)
    project = projects(:two)    
    db = s.project.databases.first
    
    assert_not db.project == project, "db is in a diff project"
    s.project = project
    s.database = db
    assert_not s.loader_is_valid?
    
    assert s.errors.full_messages.include?("Database unauthorized, must be in the same project")
    
    bl = bucket_loaders(:config_test)
    assert bl.valid?
    
    bl.s3_bucket.project = project
    assert_not bl.valid?, "Should not authorize due to project mix match"
  end
end
