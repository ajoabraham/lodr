require 'test_helper'

class QueryTest < ActiveSupport::TestCase
  test "more than 10 query histories" do
    s = Script.create(name: "my new script", user: users(:one), project: projects(:one),database: databases(:one))
    
    (0..13).each do |l|
      q = Query.create(script: s, sql: "select * from table where #{l}", status: :completed, user: users(:one))
    end
    
    assert_equal 10, s.queries.count, "no matter what we should only maintain last 10 execs"
  end
  
  test "user subscription is being updated" do
    u = users(:one)
    (1..25).each do |l|
      s = Script.create(name: "my new script", user: u, project: projects(:one), database: databases(:one))
      q = Query.create(script: s, sql: "select * from table where #{l}", status: :completed, user: u)
    end
    
    assert_equal (100-25), u.subscription.queries_remaining, "Should correctly update user sub"
    (1..75).each do |l|
      s = Script.create(name: "my new script", user: u, project: projects(:one), database: databases(:one))
      q = Query.create(script: s, sql: "select * from table where #{l}", status: :completed, user: u)
    end
    u.subscription.reload
    assert_equal 0, u.subscription.queries_remaining, "no more queries left"
    
    Subscription.any_instance.stubs(:create_stripe_subscription).returns(true)
    u.subscription.elite!
    assert_equal -1, u.subscription.queries_remaining, "should have unlimited queries"
  end
end
