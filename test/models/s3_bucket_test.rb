require 'test_helper'

class S3BucketTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  test "ensure bucket is in correct region" do
    s = S3Bucket.new(
      bucket_name: "lhw-data-dump", 
      access_id: "AKIAJ6AUWRHEZWKBDESA", 
      access_key: "4W3llDsBQqtGJnCDqAl48KuwoXiOYvX7yTqP8RJ7", region: "us-east-1",
      user: users(:one),
      project: projects(:one)
    )
    
    assert s.save, "should save bucket"
    s.region = "us-west-2"
    
    assert_not s.save, "should not save bucket with bad region"
  end
end
