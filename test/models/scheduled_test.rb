require 'test_helper'

class ScheduledTest < ActiveSupport::TestCase
  setup do
    @ld = loaders(:one)
    @ld.source_files << source_files(:reg_file2)
  end
  
  test "test hourly schedule" do
    @ld.schedule_type = :schedule_hourly
    @ld.schedule_update
    assert_equal "Hourly", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Hourly", l.schedule_in_words
    
    @ld.days_of_week = [:monday, :tuesday]
    @ld.schedule_update
    assert_equal "Hourly on Mondays and Tuesdays", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Hourly on Mondays and Tuesdays", l.schedule_in_words
    
    @ld.weeks_of_month = [:first,:last]
    @ld.schedule_update
    assert_equal "Hourly on Mondays and Tuesdays", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Hourly on Mondays and Tuesdays", l.schedule_in_words
    
    @ld.months_of_year = [:january, :october]
    @ld.schedule_update
    assert_equal "Hourly on Mondays and Tuesdays", @ld.schedule_in_words, "should ignore month settings"
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Hourly on Mondays and Tuesdays", l.schedule_in_words
    
    @ld.schedule_period = 2
    @ld.schedule_update
    assert_equal "Every 2 hours on Mondays and Tuesdays", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Every 2 hours on Mondays and Tuesdays", l.schedule_in_words
  end
  
  test "test daily schedule" do
    @ld.schedule_type = :schedule_daily
    @ld.schedule_update
    assert_equal "Daily", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Daily", l.schedule_in_words
    
    @ld.days_of_week = [:monday, :tuesday]
    @ld.schedule_update
    assert_equal "Daily", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Daily", l.schedule_in_words
    
    @ld.weeks_of_month = [:first,:last]
    @ld.schedule_update
    assert_equal "Daily", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Daily", l.schedule_in_words
    
    @ld.months_of_year = [:january, :october]
    @ld.schedule_update
    assert_equal "Daily", @ld.schedule_in_words, "should ignore month settings"
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Daily", l.schedule_in_words
    
    @ld.schedule_period = 2
    @ld.schedule_update
    assert_equal "Every 2 days", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Every 2 days", l.schedule_in_words
  end
  
  test "test monthly schedule" do
    @ld.schedule_type = :schedule_monthly
    @ld.schedule_update
    assert_equal "Monthly", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Monthly", l.schedule_in_words
    
    @ld.days_of_week = ["monday", "tuesday"]
    @ld.schedule_update
    assert_equal "Monthly on Mondays and Tuesdays", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Monthly on Mondays and Tuesdays", l.schedule_in_words
    
    @ld.weeks_of_month = [:first,:last]
    @ld.schedule_update
    assert_equal "Monthly on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Monthly on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", l.schedule_in_words
    
    @ld.months_of_year = [:january, :october]
    @ld.schedule_update
    assert_equal "Monthly on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", @ld.schedule_in_words, "should ignore month settings"
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Monthly on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", l.schedule_in_words
    
    @ld.schedule_period = 2
    @ld.schedule_update
    assert_equal "Every 2 months on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Every 2 months on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", l.schedule_in_words
  end
  
  test "monthly shedule on days of week" do
    @ld.schedule_type = :schedule_monthly    
    @ld.days_of_week = ["monday", "tuesday"]
    @ld.schedule_update
    assert_equal "Monthly on Mondays and Tuesdays", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Monthly on Mondays and Tuesdays", l.schedule_in_words
  end
  
  test "test yearly schedule" do
    @ld.schedule_type = :schedule_yearly
    @ld.schedule_update
    assert_equal "Yearly", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Yearly", l.schedule_in_words
    
    @ld.days_of_week = [:monday, :tuesday]
    @ld.schedule_update
    assert_equal "Yearly on Mondays and Tuesdays", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Yearly on Mondays and Tuesdays", l.schedule_in_words
    assert_equal [:monday, :tuesday], l.days_of_week
    
    @ld.weeks_of_month = [:first,:last]
    @ld.schedule_update
    assert_equal "Yearly on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Yearly on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", l.schedule_in_words
    assert_equal [:first,:last], l.weeks_of_month
    
    @ld.months_of_year = [:january, :october]
    @ld.schedule_update
    assert_equal "Yearly in January and October on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Yearly in January and October on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", l.schedule_in_words
    assert_equal [:january, :october], l.months_of_year
    
    @ld.schedule_period = 2
    @ld.schedule_update
    assert_equal "Every 2 years in January and October on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Every 2 years in January and October on the 1st Monday and last Monday and 1st Tuesday and last Tuesday", l.schedule_in_words
    assert_equal 2, l.schedule_period
  end
  
  test "schedule run once" do
    @ld.schedule_type = :schedule_once
    
    tt = 1.hour.from_now.in_time_zone("EST")
    @ld.time_zone = tt.time_zone
    @ld.start_date = tt.to_date
    @ld.start_time = tt
    
    @ld.schedule_update
    assert_equal "Once on #{tt.strftime('%B %d, %Y %I:%M %p %Z')}", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal "Once on #{tt.strftime('%B %d, %Y %I:%M %p %Z')}", l.schedule_in_words
    
    tt = 1.hour.ago.in_time_zone("EST")
    @ld.time_zone = tt.time_zone
    @ld.start_date = tt.to_date
    @ld.start_time = tt
    
    @ld.schedule_update
    assert_equal @ld.next_occurrence, nil, "Should be no additional occurrences"
    assert_equal "Once on #{tt.strftime('%B %d, %Y %I:%M %p %Z')}", @ld.schedule_in_words
    @ld.save
    l = Loader.find @ld.id
    assert_equal l.scheduled?, false
    assert_equal l.next_occurrence, nil, "Should be no additional occurrences"
    assert_equal "Once on #{tt.strftime('%B %d, %Y %I:%M %p %Z')}", l.schedule_in_words
  end
end
