require 'test_helper'

class SourceFileTest < ActiveSupport::TestCase
  test "should not save unsupported type" do
      u = users(:one)
      s = SourceFile.new name: "somefile.xlsx", file_type: "balksdsf", user_id: u.id, project: projects(:one)
      assert_not s.save, "Should not save due to unsuported type" 
      
      s.name = "haszip_ext.xls"
      assert_not s.save, "Should not save due to unsuported type"      
  end
  
  test "should set correct type" do
      u = users(:one)
      s = SourceFile.new name: "somefile.csv", file_type: "balksdsf", user_id: u.id, project: projects(:one)
      
      assert s.save, "Should save file"   
      assert_equal "csv", s.file_type
  end
  
  test "libs are reachable" do
    assert_equal SourceFile::ENCODINGS.size, 171, "ensure there is 171 encodings in source files"
    assert Wrangler::END_POINTS[:profile], "profile url should be there"
    assert Wrangler::END_POINTS[:sample], "sample url should be there"  
  end
  
  test "names should be unique on create" do
    f = source_files(:reg_file)
    f2 = SourceFile.new(name: f.name, user: f.user, project: f.project)
    
    assert_not f2.save, "Save should fail since a file with the name already exists"
    f2.user = users(:two)
    assert_not f2.save, "Save should fail since a file with the name already exists in project"
    
    f2.project = projects(:two)
    assert f2.save, "File should be saved since user is different"    
  end
  
  test "names should be unique in loadable" do
    f = source_files(:s3_file)
    f2 = SourceFile.new(name: f.name, user: f.user, loadable: f.loadable, project: f.project)
    
    assert_not f2.save, "Save should fail since a file with the name already exists in the bucket"
    f2.loadable = s3_buckets(:two)
    
    assert f2.save, "File should be saved since loadable is different"    
  end
  
  test "includes header is false if json, xml, log" do
    f = source_files(:reg_file)
    f2 = SourceFile.new(name: "somefile.json.gz", user: f.user, project: f.project)
    f2.auto_set_header_inclusion
    
    assert f2.save, "should save"
    assert_not f2.includes_header, "should not include header for json file"
    
    f = source_files(:reg_file)
    assert f.includes_header, "should include header in orig file"
    f.name = "someblah.json"
    f.auto_set_header_inclusion
    assert f.save
    assert_not f.includes_header, "should change include headers option on existing record"
    
    f3 = SourceFile.new(name: "somefile.jsonl.gz", user: f.user, project: f.project)
    f3.auto_set_header_inclusion
    assert f3.save, "should save"
    assert f3.includes_header, "should include header for jsonl file"
  end
end
