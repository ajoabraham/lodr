require 'test_helper'

class SubscriptionTest < ActiveSupport::TestCase
  setup do
    Subscription.any_instance.stubs(:create_stripe_subscription).returns(true)
  end
  
  test "check type of plan" do
    s = Subscription.new
    ["bootstrap", "startup", "elite", "business", "enterprise"].each do |t|
      s.send("#{t}?")
      s.send("#{t}!")      
    end
  end
  
  test "should error on paid plans" do    
    u = users(:one)
    s = Subscription.new user: u, plan: Plan.startup
    
    assert_not s.save, "Should not save when the plan is paid"
    s.bootstrap!
    
    assert s.save, "Should save when plan is free"
  end
  
  test "user upgrades subscription" do
    u = users(:one)
    
    assert u.subscription.bootstrap?, "should be bootstrap plan"
    assert_equal 5000000, u.subscription.rows_remaining, "full plan rows of 5mil left"
    
    ld = loaders(:one)
    u.load_jobs.each do |lj|      
      lj.loader = ld
      lj.save 
    end
    
    u.subscription.reload    
    assert_equal 7000000, u.subscription.rows_processed     
    assert_equal 0, u.subscription.rows_remaining, "No more rows left for this user"
    
    u.subscription.elite!
    assert u.subscription.save, "should save upgrade to elite"
    
    u = User.find u.id    
    assert u.subscription.elite?, "should now be elite"
    assert_equal 293000000, u.subscription.rows_remaining, "Should have more rows"
  end
    
  test "should add new user into bootstrap sub on create" do
    u = User.create first_name: "new", last_name: "user", email: "new@user.com", 
          password: "blah689", password_confirmation: "blah689"
        
    assert u.valid?, "should be a valid new user"
    
    assert u.subscription.bootstrap?, "should be added into a bootstrap plan"
  end
    
  test "cancel subscription" do
    u = User.create first_name: "new", last_name: "user", email: "new@user.com", 
          password: "blah689", password_confirmation: "blah689"
    
    u.subscription.stripe_customer_id= "asdfasdf"
    u.subscription.stripe_sub_id = "sdfaasf"
    u.subscription.current_period_starts_at = Time.now
    u.subscription.current_period_ends_at = 30.days.from_now
    u.subscription.elite!
    assert u.subscription.save, "should save upgrade to elite"
    
    sub = mock()
    sub.expects(:delete).with(:at_period_end => true).at_least_once
    Stripe::Subscription.stubs(:retrieve).returns(sub)
    u.subscription.cancel
    u = User.find u.id
    assert u.subscription.bootstrap?, "should be bootstrap after user cancels"
    
    assert_equal "canceled", u.subscription.status, "should be canceled sub"
    assert u.subscription.use_previous_plan?, "should be using previous plan"
  end
  
  test "custom subscription period processing" do
    u = users(:custom)
    u.subscription.update(rows_processed: 1000, queries_run: 200)
    
    assert_not u.subscription.nil?, "should have a sub"
    assert u.subscription.is_custom?, "the sub should be custom"
    
    assert u.subscription.reset_at.nil? && u.subscription.current_period_starts_at.nil?, "should not be reset at"
    u.last_sign_in_at = Time.now
    u.save
    
    assert_not u.subscription.reset_at.nil?, "reset should no longer be nil"
    assert u.subscription.current_period_starts_at = Time.now.beginning_of_month, "current period should be begginning of this month"
    assert u.subscription.current_period_ends_at = Time.now.end_of_month, "current period should be end of this month"
    
    
    assert_equal 0, u.subscription.rows_processed, "should rows proceessed should be 0"
    assert_equal 0, u.subscription.queries_run, "should rows proceessed should be 0"
    
    u.subscription.current_period_starts_at = 2.months.ago
    u.subscription.save
    assert u.subscription.current_period_starts_at < Time.now.beginning_of_month
    u.last_sign_in_at = Time.now + 1.hour
    u.save
    
    assert u.subscription.current_period_starts_at = Time.now.beginning_of_month, "current period should be begginning of this month"
    assert u.subscription.current_period_ends_at = Time.now.end_of_month, "current period should be end of this month"
    assert_equal 0, u.subscription.rows_processed, "should rows proceessed should be 0"
    assert_equal 0, u.subscription.queries_run, "should rows proceessed should be 0"
  end
end
