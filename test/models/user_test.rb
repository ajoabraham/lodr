require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "can find api access key" do
    u = User.create(first_name: "api", last_name: "tester", password: "pwdpwd", password_confirmation: "pwdpwd", email: "yoyoyoy@yo1l.com")
    assert u.valid?
    
    u2 = User.find_by_api_access_key u.api_access_key
    assert_not u2.nil?, "should not be nil"
  end
end
